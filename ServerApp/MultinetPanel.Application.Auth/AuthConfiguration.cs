﻿

using System.Collections.Generic;
using IdentityServer4.Models;
using IdentityServer4.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using MultinetPanel.Application.Auth.Helpers;
using MultinetPanel.Application.Auth.Services;
using MultinetPanel.Application.Boundary.Auth.Helpers;
using MultinetPanel.Application.Boundary.Auth.Services;
using MultinetPanel.Core.Boundary.Configurations;
using MultinetPanel.Repository;
using MultinetPanel.Repository.Users.Entities;

namespace MultinetPanel.Application.Auth
{
    public static class AuthConfiguration
    {
        public static void AddAppAuth(this IServiceCollection services, AppConfig config)
        {
            services.AddTransient<IAccountService, AccountService>();
            services.AddTransient<IProfileService, ProfileService>();

            services.AddIdentity<UserEntity, IdentityRole>(options =>
            {
                options.Password.RequireDigit = false;
                options.Password.RequiredLength = 3;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequireLowercase = false;
            })
                .AddEntityFrameworkStores<AppDbContext>()
                .AddDefaultTokenProviders();

            services.AddIdentityServer(options => { options.Endpoints.EnableDiscoveryEndpoint = true; })
                .AddDeveloperSigningCredential()
                .AddAspNetIdentity<UserEntity>()
                .AddInMemoryPersistedGrants()
                .AddInMemoryIdentityResources(GetIdentityResources())
                .AddInMemoryApiResources(GetApiResources())
                .AddInMemoryClients(GetClient(config))
                .AddProfileService<ProfileService>();
        }

        private static IEnumerable<IdentityResource> GetIdentityResources()
        {
            return new IdentityResource[]
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
                new IdentityResources.Email()
            };
        }

        public static IEnumerable<ApiResource> GetApiResources()
        {
            return new List<ApiResource>
            {
                new ApiResource("client-app","My API", new []
                {
                    Claims.AccountActivatedClaim.Type
                })
            };
        }

        private static IEnumerable<Client> GetClient(AppConfig config)
        {
            return new[]
            {
                new Client
                {
                    ClientId = "client-app",
                    AllowedGrantTypes = GrantTypes.Implicit,
                    AllowAccessTokensViaBrowser = true,
                    RedirectUris =
                    {
                        config.AppUrl,
                        config.AppUrl + "/account/profile"
                    },
                    PostLogoutRedirectUris =
                    {
                        config.AppUrl
                    },
                    AllowedScopes = { "openid", "profile", "email", "client-app" },
                    AllowedCorsOrigins =
                    {
                        config.AppUrl
                    },
                    AllowOfflineAccess = true,
                    RequireConsent = false,
                    AlwaysIncludeUserClaimsInIdToken = true
                }
            };
        }
    }

}
