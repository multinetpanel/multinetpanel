﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using IdentityServer4.Events;
using IdentityServer4.Services;
using Microsoft.AspNetCore.Identity;
using MultinetPanel.Application.Boundary.Auth.Exceptions;
using MultinetPanel.Application.Boundary.Auth.Helpers;
using MultinetPanel.Application.Boundary.Auth.Requests;
using MultinetPanel.Application.Boundary.Auth.Responses;
using MultinetPanel.Application.Boundary.Auth.Services;
using MultinetPanel.Core.Boundary.Configurations;
using MultinetPanel.Core.Boundary.Handlers;
using MultinetPanel.Repository.Boundary;
using MultinetPanel.Repository.Users.Entities;

namespace MultinetPanel.Application.Auth.Services
{
    public class AccountService : IAccountService
    {
        private readonly SignInManager<UserEntity> _signInManager;
        private readonly UserManager<UserEntity> _userManager;
        private readonly AppConfig _appConfig;
        private readonly ICryptoHandler _cryptoHandler;
        private readonly IEventService _events;

        public AccountService(
            SignInManager<UserEntity> signInManager,
            UserManager<UserEntity> userManager,
            AppConfig appConfig,
            ICryptoHandler cryptoHandler,
            IEventService events)
        {
            _signInManager = signInManager;
            _userManager = userManager;
            _appConfig = appConfig;
            _cryptoHandler = cryptoHandler;
            _events = events;
        }

        public SignInResponse SignIn(SignInRequest request)
        {
            var user = _userManager.FindByNameAsync(request.Username).Result ?? _userManager.FindByEmailAsync(request.Username).Result;

            if (user == null)
            {
                throw new SignInFailedException("Nie znaleziono użytkownika o takim loginie");
            }

            var oldPassword = _cryptoHandler.Sha256Hash(request.Password + _appConfig.OldAuthHash);

            if (user.OldPassword == oldPassword)
            {
                return SignInResponse.ResetPassword;
            }

            var result = _signInManager.PasswordSignInAsync(user, request.Password, false, false).Result;

            if (!result.Succeeded)
            {
                throw new SignInFailedException("Błędne hasło");
            }

            _events.RaiseAsync(new UserLoginSuccessEvent(user.UserName, user.Id, user.UserName)).Wait();

            return SignInResponse.Ok;
        }

        public void SignUp(SignUpRequest request)
        {
            var result = _userManager.CreateAsync(new UserEntity()
            {
                UserName = request.Username,
                Email = request.Email
            }, request.Password).Result;
        }
    }

}
