﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using IdentityServer4.Models;
using IdentityServer4.Services;
using Microsoft.AspNetCore.Identity;
using MultinetPanel.Application.Boundary.Auth.Helpers;
using MultinetPanel.Repository.Users.Entities;

namespace MultinetPanel.Application.Auth.Helpers
{
    public class ProfileService : IProfileService
    {
        protected UserManager<UserEntity> UserManager;

        public ProfileService(UserManager<UserEntity> userManager)
        {
            UserManager = userManager;
        }

        public Task GetProfileDataAsync(ProfileDataRequestContext context)
        {
            var user = UserManager.GetUserAsync(context.Subject).Result;
            var userClaims = UserManager.GetClaimsAsync(user).Result;

            context.IssuedClaims.AddRange(userClaims);
            context.IssuedClaims.Add(new Claim(Claims.Email.Type, user.Email));

            return Task.FromResult(0);
        }

        public Task IsActiveAsync(IsActiveContext context)
        {
            var user = UserManager.GetUserAsync(context.Subject).Result;

            context.IsActive = (user != null);

            return Task.FromResult(0);
        }
    }
}
