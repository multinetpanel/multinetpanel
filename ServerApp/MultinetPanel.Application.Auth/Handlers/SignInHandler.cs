﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using MultinetPanel.Application.Boundary.Auth.Requests;
using MultinetPanel.Application.Boundary.Auth.Responses;
using MultinetPanel.Application.Boundary.Auth.Services;

namespace MultinetPanel.Application.Auth.Handlers
{
    public class SignInHandler : IRequestHandler<SignInRequest, SignInResponse>
    {
        private readonly IAccountService _accountService;

        public SignInHandler(IAccountService accountService)
        {
            _accountService = accountService;
        }


        public Task<SignInResponse> Handle(SignInRequest request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_accountService.SignIn(request));
        }
    }
}
