﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Razor.Compilation;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MultinetPanel.Application.Auth;
using MultinetPanel.Core;
using MultinetPanel.Core.Boundary.Configurations;
using MultinetPanel.DomainModules.FinancialModule;
using MultinetPanel.Repository;
using Newtonsoft.Json;

namespace MultinetPanel
{
    public class Startup
    {
        public IConfiguration Configuration { get; set; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            var config = GetAppConfig();
            services.AddSingleton(config);

            var appAssemblies = GetAppAssemblies().ToArray();

            services.AddMediatR(appAssemblies);
            services.AddRepository(Configuration);
            services.AddCore(appAssemblies);
            services.AddFinancialModule(Configuration);

            services.AddMvc()
                .AddJsonOptions(options =>
                {
                    options.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Local;
                });


            services.AddAppAuth(config);
        }

        private IEnumerable<Assembly> GetAppAssemblies()
        {
            return Assembly
                .GetAssembly(typeof(Startup))
                .GetReferencedAssemblies()
                .Select(Assembly.Load)
                .Where(n => n.FullName.StartsWith("MultinetPanel"));
        }

        private AppConfig GetAppConfig()
        {
            var config = new AppConfig();

            Configuration.Bind(config);
            config.ConnectionString = Configuration.GetConnectionString("DefaultConnection");

            return config;
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseExceptionHandler("/error");

            app.UseStaticFiles();
            app.UseIdentityServer();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");

                routes.MapSpaFallbackRoute(
                    name: "spa-fallback",
                    defaults: new { controller = "Home", action = "Index" });
            });

        }
    }
}
