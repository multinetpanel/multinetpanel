﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MultinetPanel.Core.Boundary.AppFlow;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Models;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Requests;

namespace MultinetPanel.Controllers
{
    public class TransfersController : CoreController
    {
        private readonly IFlowExecutor _flowExecutor;

        public TransfersController(IFlowExecutor flowExecutor)
        {
            _flowExecutor = flowExecutor;
        }

        [HttpGet]
        public Task<IEnumerable<Transfer>> GetTransfers([FromQuery] GetTransfersRequest request)
        {
            return _flowExecutor.Send(request);
        }

        [HttpGet("count")]
        public Task<int> GetTransfersCount([FromQuery] GetTransfersCountRequest request)
        {
            return _flowExecutor.Send(request);
        }

        [HttpGet("{id}/detailed")]
        public Task<DetailedTransfer> GetTransfer([FromRoute] GetTransferRequest request)
        {
            return _flowExecutor.Send(request);
        }

        [HttpGet("detailed/{id}")]
        public Task<DetailedTransfer> GetDetailedTransfer([FromRoute] GetDetailedTransferRequest request)
        {
            return _flowExecutor.Send(request);
        }

        [HttpPost("import")]
        public Task Import(ImportTransfersRequest request)
        {
            return _flowExecutor.Send(request);
        }

        [HttpPost("index")]
        public Task Index(IndexTransfersRequest request)
        {
            return _flowExecutor.Send(request);
        }

        [HttpGet("index/invoice")]
        public Task Index(IndexTransferInvoiceRequest request)
        {
            return _flowExecutor.Send(request);
        }
    }
}
