﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MultinetPanel.Core.Boundary.AppFlow;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Clients.Models;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Clients.Requests;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Invoices.Models;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Invoices.Requests;

namespace MultinetPanel.Controllers
{
    public class ClientsController : CoreController
    {
        private readonly IFlowExecutor _flowExecutor;

        public ClientsController(IFlowExecutor flowExecutor)
        {
            _flowExecutor = flowExecutor;
        }

        [HttpGet("{id:int}")]
        public Task<Client> GetClient([FromRoute] GetClientRequest request)
        {
            return _flowExecutor.Send(request);
        }

        [HttpGet]
        public Task<IEnumerable<Client>> GetClients([FromQuery] GetClientsRequest request)
        {
            return _flowExecutor.Send(request);
        }

        [HttpGet("count")]
        public Task<int> GetClientsCount([FromQuery] GetClientsCountRequest request)
        {
            return _flowExecutor.Send(request);
        }

        [HttpGet("search")]
        public Task<IEnumerable<Client>> SearchClient([FromQuery] SearchClientRequest request)
        {
            return _flowExecutor.Send(request);
        }
    }
}
