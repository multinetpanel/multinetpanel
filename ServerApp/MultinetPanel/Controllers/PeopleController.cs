﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MultinetPanel.Core.Boundary.AppFlow;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Clients.Requests;
using MultinetPanel.DomainModules.FinancialModule.Boundary.People.Models;
using MultinetPanel.DomainModules.FinancialModule.Boundary.People.Requests;

namespace MultinetPanel.Controllers
{
    public class PeopleController : CoreController
    {
        private readonly IFlowExecutor _flowExecutor;

        public PeopleController(IFlowExecutor flowExecutor)
        {
            _flowExecutor = flowExecutor;
        }

        [HttpGet("search")]
        public Task<IEnumerable<Person>> SearchPerson([FromQuery] SearchPersonRequest request)
        {
            return _flowExecutor.Send(request);
        }
    }
}