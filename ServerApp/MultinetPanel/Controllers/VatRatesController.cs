﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MultinetPanel.Core.Boundary.AppFlow;
using MultinetPanel.DomainModules.FinancialModule.Boundary.VatRates.Models;
using MultinetPanel.DomainModules.FinancialModule.Boundary.VatRates.Requests;

namespace MultinetPanel.Controllers
{
    public class VatRatesController : CoreController
    {
        private readonly IFlowExecutor _flowExecutor;

        public VatRatesController(IFlowExecutor flowExecutor)
        {
            _flowExecutor = flowExecutor;
        }

        [HttpGet]
        public Task<IEnumerable<VatRate>> GetVatRates(GetVatRatesRequest request)
        {
            return _flowExecutor.Send(request);
        }
    }
}
