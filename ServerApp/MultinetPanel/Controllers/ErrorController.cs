﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;

namespace MultinetPanel.Controllers
{
    [Route("[controller]")]
    public class ErrorController : Controller
    {
        public string Index(string errorId)
        {
            var exception = HttpContext.Features.Get<IExceptionHandlerFeature>()?.Error;

            return exception?.Message;
        }
    }
}
