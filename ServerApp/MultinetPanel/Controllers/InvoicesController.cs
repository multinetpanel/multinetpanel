﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MultinetPanel.Core.Boundary.AppFlow;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Invoices.Models;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Invoices.Requests;

namespace MultinetPanel.Controllers
{
    public class InvoicesController : CoreController
    {
        private readonly IFlowExecutor _flowExecutor;

        public InvoicesController(IFlowExecutor flowExecutor)
        {
            _flowExecutor = flowExecutor;
        }

        [HttpPost]
        public Task AddInvoice([FromBody] AddInvoiceRequest request)
        {
            return _flowExecutor.Send(request);
        }

        [HttpGet("{id:int}")]
        public Task<Invoice> GetInvoice([FromRoute] GetInvoiceRequest request)
        {
            return _flowExecutor.Send(request);
        }

        [HttpGet]
        public Task<IEnumerable<Invoice>> GetInvoices([FromQuery] GetInvoicesRequest request)
        {
            return _flowExecutor.Send(request);
        }

        [HttpGet("count")]
        public Task<int> GetInvoicesCount([FromQuery] GetInvoicesCountRequest request)
        {
            return _flowExecutor.Send(request);
        }

        [HttpGet("sum")]
        public Task<InvoicesSum> GetInvoicesSum([FromQuery] GetInvoicesSumRequest request)
        {
            return _flowExecutor.Send(request);
        }
    }
}
