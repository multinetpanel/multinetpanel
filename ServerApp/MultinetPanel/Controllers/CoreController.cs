﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace MultinetPanel.Controllers
{
    [Route("api/v1/[controller]"), Authorize]
    public abstract class CoreController : Controller
    {
    }
}
