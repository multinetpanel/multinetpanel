﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MultinetPanel.Application.Boundary.Auth.Requests;
using MultinetPanel.Application.Boundary.Auth.Responses;
using MultinetPanel.Core.Boundary.AppFlow;

namespace MultinetPanel.Controllers
{
    [Route("[controller]/[action]")]
    public class AccountController : Controller
    {
        private readonly IFlowExecutor _flow;

        public AccountController(IFlowExecutor flow)
        {
            _flow = flow;
        }

        [HttpPost, AllowAnonymous]
        public Task<SignInResponse> SignIn([FromBody] SignInRequest request)
        {
            return _flow.Send(request);
        }
    }
}
