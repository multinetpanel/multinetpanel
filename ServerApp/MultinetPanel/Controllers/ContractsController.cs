﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MultinetPanel.Core.Boundary.AppFlow;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Invoices.Models;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Invoices.Requests;

namespace MultinetPanel.Controllers
{
    public class ContractsController : CoreController
    {
        private readonly IFlowExecutor _flowExecutor;

        public ContractsController(IFlowExecutor flowExecutor)
        {
            _flowExecutor = flowExecutor;
        }

        [HttpGet("{id:int}")]
        public Task<Invoice> GetContract([FromRoute] GetInvoiceRequest request)
        {
            return _flowExecutor.Send(request);
        }

        [HttpGet]
        public Task<IEnumerable<Invoice>> GetContracts([FromQuery] GetInvoicesRequest request)
        {
            return _flowExecutor.Send(request);
        }

        [HttpGet("count")]
        public Task<int> GetContractsCount([FromQuery] GetInvoicesCountRequest request)
        {
            return _flowExecutor.Send(request);
        }
    }
}
