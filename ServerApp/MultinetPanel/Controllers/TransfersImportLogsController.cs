﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MultinetPanel.Core.Boundary.AppFlow;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Importers.Models;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Requests;

namespace MultinetPanel.Controllers
{
    public class TransfersImportLogsController : CoreController
    {
        private readonly IFlowExecutor _flowExecutor;

        public TransfersImportLogsController(IFlowExecutor flowExecutor)
        {
            _flowExecutor = flowExecutor;
        }

        [HttpGet]
        public Task<IEnumerable<TransfersImportLog>> GetLogs([FromQuery] GetTransfersImportLogsRequest request)
        {
            return _flowExecutor.Send(request);
        }

        [HttpGet("count")]
        public Task<int> GetLogsCount([FromQuery] GetTransfersImportLogsCountRequest request)
        {
            return _flowExecutor.Send(request);
        }

        [HttpGet("{id}")]
        public Task<TransfersImportLog> GetLog([FromRoute] GetTransfersImportLogRequest request)
        {
            return _flowExecutor.Send(request);
        }
    }
}