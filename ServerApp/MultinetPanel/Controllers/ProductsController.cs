﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MultinetPanel.Core.Boundary.AppFlow;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Invoices.Models;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Invoices.Requests;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Products.Models;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Products.Requests;

namespace MultinetPanel.Controllers
{
    public class ProductsController : CoreController
    {
        private readonly IFlowExecutor _flowExecutor;

        public ProductsController(IFlowExecutor flowExecutor)
        {
            _flowExecutor = flowExecutor;
        }

        [HttpGet("{id:int}")]
        public Task<Product> GetProduct([FromRoute] GetProductRequest request)
        {
            return _flowExecutor.Send(request);
        }

        [HttpGet]
        public Task<IEnumerable<Product>> GetProducts([FromQuery] GetProductsRequest request)
        {
            return _flowExecutor.Send(request);
        }

        [HttpGet("count")]
        public Task<int> GetProductsCount([FromQuery] GetProductsCountRequest request)
        {
            return _flowExecutor.Send(request);
        }
    }
}
