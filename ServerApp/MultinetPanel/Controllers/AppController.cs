﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace MultinetPanel.Controllers
{
    public class AppController : CoreController
    {
        [HttpGet]
        public string GetVersion()
        {
            return typeof(Startup).Assembly.GetName().Version.ToString(3);
        }
    }
}
