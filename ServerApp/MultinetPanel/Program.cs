﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using MultinetPanel.Repository;
using MultinetPanel.Repository.Boundary.Migrations;
using MultinetPanel.Repository.Migrations;

namespace MultinetPanel
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = CreateWebHostBuilder(args).Build();

            RunMigrations(host.Services);

            host.Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args)
        {
            var enviroment = args.Length > 0 ? args[0] : "Development";

            return WebHost.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration((ctx, config) => ConfigConfiguration(ctx, config, enviroment))
                .UseStartup<Startup>();
        }


        public static void ConfigConfiguration(WebHostBuilderContext ctx, IConfigurationBuilder config, string environment)
        {
            config
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile("passwords.json", optional: true)
                .AddEnvironmentVariables()
                .AddUserSecrets<Startup>()
                .Build();
        }

        private static void RunMigrations(IServiceProvider serviceProvider)
        {
            try
            {
                using (var scope = serviceProvider.CreateScope())
                {
                    var db = scope.ServiceProvider.GetService<AppDbContext>();

                    db.RunMigrations();

                    scope.ServiceProvider.GetService<ISeederManager>().Seed();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}
