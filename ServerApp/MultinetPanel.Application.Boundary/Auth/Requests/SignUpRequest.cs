﻿using MediatR;

namespace MultinetPanel.Application.Boundary.Auth.Requests
{
    public class SignUpRequest : IRequest
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
    }
}