﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;
using MultinetPanel.Application.Boundary.Auth.Responses;

namespace MultinetPanel.Application.Boundary.Auth.Requests
{
    public class SignInRequest : IRequest<SignInResponse>
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
