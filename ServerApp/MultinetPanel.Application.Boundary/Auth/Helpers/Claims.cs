﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;

namespace MultinetPanel.Application.Boundary.Auth.Helpers
{
    public static class Claims
    {
        public static Claim AccountActivatedClaim => new Claim(nameof(AccountActivatedClaim), "true");
        public static Claim Email => new Claim(nameof(Email), string.Empty);
    }
}
