﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MultinetPanel.Application.Boundary.Auth.Responses
{
    public enum SignInResponse
    {
        Failed = 0,
        Ok = 1,
        ResetPassword = 2
    }
}
