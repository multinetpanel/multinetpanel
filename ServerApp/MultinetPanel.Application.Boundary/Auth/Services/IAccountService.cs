﻿using System;
using System.Collections.Generic;
using System.Text;
using MultinetPanel.Application.Boundary.Auth.Requests;
using MultinetPanel.Application.Boundary.Auth.Responses;

namespace MultinetPanel.Application.Boundary.Auth.Services
{
    public interface IAccountService
    {
        SignInResponse SignIn(SignInRequest request);
        void SignUp(SignUpRequest request);
    }
}
