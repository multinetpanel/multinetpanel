﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MultinetPanel.Application.Boundary.Auth.Exceptions
{
    public class SignInFailedException : Exception
    {
        public SignInFailedException()
        {
            
        }

        public SignInFailedException(string msg) : base(msg)
        {
            
        }
    }
}
