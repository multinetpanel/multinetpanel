﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MultinetPanel.Core.Boundary.CoreModels;
using MultinetPanel.Core.Boundary.Handlers;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Importers.Models;
using MultinetPanel.Repository.Boundary.Transfers;
using MultinetPanel.Repository.Extensions;
using MultinetPanel.Repository.Transfers.Entities;

namespace MultinetPanel.Repository.Transfers
{
    public class TransferImporterLogsRepository : ITransferImporterLogsRepository
    {
        private readonly AppDbContext _context;
        private readonly IObjectMapper _objectMapper;

        public TransferImporterLogsRepository(AppDbContext context, IObjectMapper objectMapper)
        {
            _context = context;
            _objectMapper = objectMapper;
        }

        public void AddLog(TransfersImportLog log)
        {
            var entity = _objectMapper.Map<TransfersImportLogEntity>(log);

            _context.TransfersImportLogs.Add(entity);
        }

        public IEnumerable<TransfersImportLog> GetLogs(PagedModel paginationModel)
        {
            var entities = _context.TransfersImportLogs
                .OrderByDescending(n => n.Date)
                .AddPagination(paginationModel)
                .ToArray();

            return _objectMapper.Map<IEnumerable<TransfersImportLog>>(entities);
        }

        public TransfersImportLog GetLog(int id)
        {
            var entity = _context.TransfersImportLogs.Find(id);

            return _objectMapper.Map<TransfersImportLog>(entity);
        }

        public int GetLogsCount()
        {
            return _context.TransfersImportLogs.Count();
        }
    }
}
