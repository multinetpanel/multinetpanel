﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using MultinetPanel.Core.Boundary.Handlers;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Importers.Models;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Invoices.Models;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Models;
using MultinetPanel.Repository.Boundary;
using MultinetPanel.Repository.Boundary.Invoices;
using MultinetPanel.Repository.Boundary.Transfers;
using MultinetPanel.Repository.Extensions;
using MultinetPanel.Repository.Transfers.Entities;

namespace MultinetPanel.Repository.Transfers
{
    public class TransferRepository : ITransferRepository
    {
        private readonly AppDbContext _appDbContext;
        private readonly IObjectMapper _objectMapper;
        private readonly IInvoiceRepository _invoiceRepository;

        public TransferRepository(AppDbContext appDbContext, IObjectMapper objectMapper, IInvoiceRepository invoiceRepository)
        {
            _appDbContext = appDbContext;
            _objectMapper = objectMapper;
            _invoiceRepository = invoiceRepository;
        }

        public IEnumerable<Transfer> GetTransfers(GetTransfersOptions getOptions)
        {
            var transfersEntity = GetTransfersQuery(getOptions)
                .OrderByDescending(n => n.Date)
                .AddPagination(getOptions);

            return _objectMapper.Map<IEnumerable<Transfer>>(transfersEntity);
        }

        public int GetTransfersCount(GetTransfersOptions options)
        {
            return GetTransfersQuery(options).Count();
        }

        public Transfer GetTransfer(int id)
        {
            var entity = _appDbContext.Transfers.Find(id);

            return _objectMapper.Map<Transfer>(entity);
        }

        public DetailedTransfer GetDetailedTransfer(int id)
        {
            var entity = _appDbContext.Transfers
                .Include(n => n.TransferInvoices).ThenInclude(n => n.Invoice).ThenInclude(n => n.InvoiceItems)
                .Include(n => n.TransferInvoices).ThenInclude(n => n.Invoice).ThenInclude(n => n.Purchaser)
                .Include(n => n.TransferIndexerLogs)
                .SingleOrDefault(n => n.Id == id);

            return _objectMapper.Map<DetailedTransfer>(entity);
        }

        private IQueryable<TransferEntity> GetTransfersQuery(GetTransfersOptions getOptions)
        {
            return _appDbContext.Transfers
                .Include(n => n.TransferInvoices)
                .AddConditionIfNotNull(getOptions.DateFrom, n => n.Date >= getOptions.DateFrom)
                .AddConditionIfNotNull(getOptions.DateTo, n => n.Date <= getOptions.DateTo)
                .AddConditionIfNotNull(getOptions.Receiver, n => n.Receiver.ToLower().Contains(getOptions.Receiver.ToLower()))
                .AddConditionIfNotNull(getOptions.SenderAccountNumber, n => n.SenderAccountNumber.ToLower().Contains(getOptions.SenderAccountNumber.ToLower()))
                .AddConditionIfNotNull(getOptions.SenderCity, n => n.SenderCity.ToLower().Contains(getOptions.SenderCity.ToLower()))
                .AddConditionIfNotNull(getOptions.SenderName, n => n.SenderName.ToLower().Contains(getOptions.SenderName.ToLower()))
                .AddConditionIfNotNull(getOptions.SenderStreet, n => n.SenderStreet.ToLower().Contains(getOptions.SenderStreet.ToLower()))
                .AddConditionIfNotNull(getOptions.Title, n => n.Title.ToLower().Contains(getOptions.Title))
                .AddCondition(() => getOptions.HasInvoices.HasValue && getOptions.HasInvoices.Value, n => n.TransferInvoices.Any());
        }

        public IEnumerable<Invoice> GetTransferInvoices(int transferId)
        {
            var transferEntity = _appDbContext.Transfers
                .Include(n => n.TransferInvoices).ThenInclude(n => n.Invoice)
                .SingleOrDefault(n => n.Id == transferId);

            var invoices = transferEntity?.TransferInvoices.Select(n => n.Invoice);

            return _objectMapper.Map<IEnumerable<Invoice>>(invoices);
        }

        public void IndexTransfer(int transferId, int invoiceId)
        {
            var entity = new TransferInvoiceEntity()
            {
                InvoiceId = invoiceId,
                TransferId = transferId
            };

            _appDbContext.TransferInvoices.Add(entity);
        }

        public void AddTransferIndexerLog(int transferId, string message)
        {
            var entity = new TransferIndexerLogEntity()
            {
                TransferId = transferId,
                Message = message,
                Date = DateTime.Now,
            };

            _appDbContext.TransferIndexerLogs.Add(entity);
        }

        public IEnumerable<TransfersImportLog> GetImportLogs(GetTransfersImportLogsOptions options)
        {
            var logsEntity = _appDbContext.TransfersImportLogs
                .AddCondition(() => options.IsSuccessful.HasValue, n => n.IsSuccessful == options.IsSuccessful)
                .AddPagination(options);

            return _objectMapper.Map<IEnumerable<TransfersImportLog>>(logsEntity);
        }

        public void AddTransfer(Transfer transfer)
        {
            var entity = _objectMapper.Map<TransferEntity>(transfer);

            _appDbContext.Transfers.Add(entity);
        }
    }
}
