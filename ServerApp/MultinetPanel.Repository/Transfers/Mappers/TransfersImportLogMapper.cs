﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Importers.Models;
using MultinetPanel.Repository.Transfers.Entities;

namespace MultinetPanel.Repository.Transfers.Mappers
{
    public class TransfersImportLogMapper : Profile
    {
        public TransfersImportLogMapper()
        {
            CreateMap<TransfersImportLog, TransfersImportLogEntity>();

            CreateMap<TransfersImportLogEntity, TransfersImportLog>();
        }
    }
}
