﻿using AutoMapper;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Models;
using MultinetPanel.Repository.Transfers.Entities;

namespace MultinetPanel.Repository.Transfers.Mappers
{
    public class TransferIndexerLogMapper : Profile
    {
        public TransferIndexerLogMapper()
        {
            CreateMap<TransferIndexerLogEntity, TransferIndexerLog>();

            CreateMap<TransferIndexerLog, TransferIndexerLogEntity>();
        }
    }
}