﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Invoices.Models;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Models;
using MultinetPanel.Repository.Transfers.Entities;

namespace MultinetPanel.Repository.Transfers.Mappers
{
    public class TransferEntityMapper : Profile
    {
        public TransferEntityMapper()
        {
            CreateMap<TransferEntity, Transfer>();

            CreateMap<TransferEntity, Transfer>()
                .ForMember(n => n.HasInvoice, n => n.MapFrom(f => f.TransferInvoices.Any()));

            CreateMap<TransferEntity, DetailedTransfer>()
                .ForMember(n => n.Invoices, n => n.MapFrom(u => u.TransferInvoices.Select(v => v.Invoice)))
                .ForMember(n => n.Logs, n => n.MapFrom(u => u.TransferIndexerLogs))
                .ForMember(n => n.HasInvoice, n => n.MapFrom(f => f.TransferInvoices.Any()));
        }
    }
}
