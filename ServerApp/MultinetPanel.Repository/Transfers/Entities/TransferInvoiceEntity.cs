﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MultinetPanel.Repository.Invoices.Entities;

namespace MultinetPanel.Repository.Transfers.Entities
{
    public class TransferInvoiceEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int TransferId { get; set; }
        public int InvoiceId { get; set; }

        [ForeignKey("TransferId")]
        public TransferEntity Transfer { get; set; }

        [ForeignKey("InvoiceId")]
        public InvoiceEntity Invoice { get; set; }
    }
}