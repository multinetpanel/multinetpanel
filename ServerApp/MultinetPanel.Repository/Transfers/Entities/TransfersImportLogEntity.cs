﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MultinetPanel.Repository.Transfers.Entities
{
    public class TransfersImportLogEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]

        public int Id { get; set; }
        public DateTime Date { get; set; }
        public bool IsSuccessful { get; set; }
        public int TransfersCount { get; set; }
        public string Message { get; set; }
    }
}
