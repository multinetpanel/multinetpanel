﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MultinetPanel.Repository.Transfers.Entities
{
    public class TransferEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public decimal Value { get; set; }
        public string SenderAccountNumber { get; set; }
        public string SenderName { get; set; }
        public string SenderStreet { get; set; }
        public string SenderCity { get; set; }
        public string Receiver { get; set; }
        public string Title { get; set; }
        public DateTime Date { get; set; }

        public ICollection<TransferInvoiceEntity> TransferInvoices { get; set; }
        public ICollection<TransferIndexerLogEntity> TransferIndexerLogs { get; set; }
    }
}
