﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MultinetPanel.Repository.Transfers.Entities
{
    public class TransferIndexerLogEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int TransferId { get; set; }
        public string Message { get; set; }
        public DateTime Date { get; set; }

        [ForeignKey("TransferId")]
        public TransferEntity Transfer { get; set; }
    }
}
