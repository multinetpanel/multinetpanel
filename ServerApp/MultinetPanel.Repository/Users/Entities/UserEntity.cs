﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Microsoft.AspNetCore.Identity;
using MultinetPanel.Repository.Clients.Entities;

namespace MultinetPanel.Repository.Users.Entities
{
    public class UserEntity : IdentityUser
    {
        public string OldPassword { get; set; }

        [ForeignKey("Client")]
        public int? ClientId { get; set; }
        public ClientEntity Client { get; set; }
    }
}
