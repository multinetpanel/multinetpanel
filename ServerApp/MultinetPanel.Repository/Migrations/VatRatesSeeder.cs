﻿using System.Linq;
using MultinetPanel.Application.Boundary.Auth.Services;
using MultinetPanel.Repository.Boundary.Migrations;
using MultinetPanel.Repository.VatRates.Entities;

namespace MultinetPanel.Repository.Migrations
{
    public class VatRatesSeeder : ISeeder
    {
        private readonly AppDbContext _context;
        private readonly IAccountService _accountService;

        public VatRatesSeeder(AppDbContext context, IAccountService accountService)
        {
            _context = context;
            _accountService = accountService;
        }

        public bool ShouldSeed()
        {
            return !_context.VatRates.Any();
        }

        public void Seed()
        {
            var vatRates = new[]
            {
                new VatRateEntity() { Name = "23%", Value = 23 },
                new VatRateEntity() { Name = "8%", Value = 8 },
                new VatRateEntity() { Name = "5%", Value = 5 },
                new VatRateEntity() { Name = "0%", Value = 0 },
                new VatRateEntity() { Name = "Zwolniona", Value = 0 }
            };

            _context.VatRates.AddRange(vatRates);

            _context.SaveChanges();
        }
    }
}