﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MultinetPanel.Application.Boundary.Auth.Requests;
using MultinetPanel.Application.Boundary.Auth.Services;
using MultinetPanel.Repository.Boundary.Migrations;

namespace MultinetPanel.Repository.Migrations
{
    public class UserSeeder : ISeeder
    {
        private readonly AppDbContext _context;
        private readonly IAccountService _accountService;

        public UserSeeder(AppDbContext context, IAccountService accountService)
        {
            _context = context;
            _accountService = accountService;
        }

        public bool ShouldSeed()
        {
            return !_context.Users.Any(n => n.UserName == "admin");
        }

        public void Seed()
        {
            _accountService.SignUp(new SignUpRequest()
            {
                Username = "admin",
                Password = "multinet",
                Email = "dev@rples.net"
            });
        }
    }
}
