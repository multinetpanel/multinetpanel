﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace MultinetPanel.Repository.Migrations
{
    public partial class VatRatesMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "VatRate",
                schema: "multinet",
                table: "InvoiceItems");

            migrationBuilder.AddColumn<int>(
                name: "VatRateId",
                schema: "multinet",
                table: "InvoiceItems",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "VatRates",
                schema: "multinet",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(nullable: true),
                    Value = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VatRates", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_InvoiceItems_VatRateId",
                schema: "multinet",
                table: "InvoiceItems",
                column: "VatRateId");

            migrationBuilder.AddForeignKey(
                name: "FK_InvoiceItems_VatRates_VatRateId",
                schema: "multinet",
                table: "InvoiceItems",
                column: "VatRateId",
                principalSchema: "multinet",
                principalTable: "VatRates",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_InvoiceItems_VatRates_VatRateId",
                schema: "multinet",
                table: "InvoiceItems");

            migrationBuilder.DropTable(
                name: "VatRates",
                schema: "multinet");

            migrationBuilder.DropIndex(
                name: "IX_InvoiceItems_VatRateId",
                schema: "multinet",
                table: "InvoiceItems");

            migrationBuilder.DropColumn(
                name: "VatRateId",
                schema: "multinet",
                table: "InvoiceItems");

            migrationBuilder.AddColumn<double>(
                name: "VatRate",
                schema: "multinet",
                table: "InvoiceItems",
                nullable: false,
                defaultValue: 0.0);
        }
    }
}
