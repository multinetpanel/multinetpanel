﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MultinetPanel.Repository.Migrations
{
    public partial class RemovedColMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "StreetType",
                schema: "multinet",
                table: "People");

            migrationBuilder.AddColumn<decimal>(
                name: "Vat",
                schema: "multinet",
                table: "InvoiceItems",
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Vat",
                schema: "multinet",
                table: "InvoiceItems");

            migrationBuilder.AddColumn<string>(
                name: "StreetType",
                schema: "multinet",
                table: "People",
                nullable: true);
        }
    }
}
