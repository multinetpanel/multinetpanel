﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MultinetPanel.Repository.Boundary.Migrations;

namespace MultinetPanel.Repository.Migrations
{
    public class SeederManager : ISeederManager
    {
        private readonly AppDbContext _appDbContext;
        private readonly IServiceProvider _serviceProvider;

        public SeederManager(AppDbContext appDbContext, IServiceProvider serviceProvider)
        {
            _appDbContext = appDbContext;
            _serviceProvider = serviceProvider;
        }

        public void Seed()
        {
            var seeders = GetSeeders()
                .Select(n => _serviceProvider.GetService(n) as ISeeder)
                .Where(n => n?.ShouldSeed() ?? false);

            foreach (var seeder in seeders)
            {
                seeder.Seed();
            }
        }

        private IEnumerable<Type> GetSeeders()
        {
            return new []
            {
                typeof(UserSeeder),
                typeof(VatRatesSeeder)
            };
        }
    }
}
