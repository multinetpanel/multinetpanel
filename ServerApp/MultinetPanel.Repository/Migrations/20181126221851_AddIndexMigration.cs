﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MultinetPanel.Repository.Migrations
{
    public partial class AddIndexMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Index",
                schema: "multinet",
                table: "InvoiceItems",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Index",
                schema: "multinet",
                table: "InvoiceItems");
        }
    }
}
