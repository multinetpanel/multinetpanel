﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MultinetPanel.Repository.Migrations
{
    public partial class AddClientMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ClientId",
                schema: "multinet",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OldPassword",
                schema: "multinet",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_ClientId",
                schema: "multinet",
                table: "AspNetUsers",
                column: "ClientId");

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_Clients_ClientId",
                schema: "multinet",
                table: "AspNetUsers",
                column: "ClientId",
                principalSchema: "multinet",
                principalTable: "Clients",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_Clients_ClientId",
                schema: "multinet",
                table: "AspNetUsers");

            migrationBuilder.DropIndex(
                name: "IX_AspNetUsers_ClientId",
                schema: "multinet",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "ClientId",
                schema: "multinet",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "OldPassword",
                schema: "multinet",
                table: "AspNetUsers");
        }
    }
}
