﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using MultinetPanel.Repository.Boundary;
using MultinetPanel.Repository.Boundary.Clients;
using MultinetPanel.Repository.Boundary.Invoices;
using MultinetPanel.Repository.Boundary.People;
using MultinetPanel.Repository.Boundary.Transfers;
using MultinetPanel.Repository.Boundary.VatRates;
using MultinetPanel.Repository.Clients;
using MultinetPanel.Repository.Invoices;
using MultinetPanel.Repository.Transfers;

namespace MultinetPanel.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        private ITransferRepository _transferRepository;
        public ITransferRepository TransferRepository => _transferRepository ?? (_transferRepository = GetRepository<ITransferRepository>());

        private IInvoiceRepository _invoiceRepository;
        public IInvoiceRepository InvoiceRepository => _invoiceRepository ?? (_invoiceRepository = GetRepository<IInvoiceRepository>());

        private IClientRepository _clientRepository;
        public IClientRepository ClientRepository => _clientRepository ?? (_clientRepository = GetRepository<IClientRepository>());

        private IPersonRepository _personRepository;
        public IPersonRepository PersonRepository => _personRepository ?? (_personRepository = GetRepository<IPersonRepository>());

        private IVatRateRepository _vatRateRepository;
        public IVatRateRepository VatRateRepository => _vatRateRepository ?? (_vatRateRepository = GetRepository<IVatRateRepository>());

        private ITransferImporterLogsRepository _transferImporterLogsRepository;
        public ITransferImporterLogsRepository TransferImporterLogsRepository => _transferImporterLogsRepository ?? (_transferImporterLogsRepository = GetRepository<ITransferImporterLogsRepository>());

        private readonly AppDbContext _appDbContext;
        private readonly IServiceProvider _serviceProvider;

        public UnitOfWork(AppDbContext appDbContext, IServiceProvider serviceProvider)
        {
            _appDbContext = appDbContext;
            _serviceProvider = serviceProvider;
        }

        private TRepository GetRepository<TRepository>()
        {
            return _serviceProvider.GetService<TRepository>();
        }

        public void SaveChanges()
        {
            _appDbContext.SaveChanges();
        }

        public void Dispose()
        {
            _appDbContext.Dispose();
        }
    }
}

