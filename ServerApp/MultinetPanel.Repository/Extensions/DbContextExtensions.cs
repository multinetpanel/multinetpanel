﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using MultinetPanel.Core.Boundary.CoreModels;

namespace MultinetPanel.Repository.Extensions
{
    public static class DbContextExtensions
    {
        public static IQueryable<TEntity> AddPagination<TEntity>(this IQueryable<TEntity> entities, PagedModel model)
        {
            if (model.Page == 0 && model.PageSize == 0)
            {
                return entities;
            }

            return entities
                .Skip((model.Page - 1) * model.PageSize)
                .Take(model.PageSize);
        }

        public static IQueryable<TEntity> AddCondition<TEntity>(this IQueryable<TEntity> entities, Func<bool> addCondition, Expression<Func<TEntity, bool>> condition)
        {
            return !addCondition() 
                ? entities 
                : entities.Where(condition);
        }

        public static IQueryable<TEntity> AddConditionIfNotNull<TEntity>(this IQueryable<TEntity> entities, object objectToCheck, Expression<Func<TEntity, bool>> condition)
        {
            if (objectToCheck == null)
            {
                return entities;
            }

            return entities.Where(condition);
        }
    }
}
