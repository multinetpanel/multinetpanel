﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using MultinetPanel.Core.Boundary.Handlers;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Clients.Models;
using MultinetPanel.DomainModules.FinancialModule.Boundary.People.Models;
using MultinetPanel.Repository.Boundary.People;
using MultinetPanel.Repository.Invoices.Entities;

namespace MultinetPanel.Repository.People
{
    public class PersonRepository : IPersonRepository
    {
        private readonly AppDbContext _appDbContext;
        private readonly IObjectMapper _objectMapper;

        public PersonRepository(AppDbContext appDbContext, IObjectMapper objectMapper)
        {
            _appDbContext = appDbContext;
            _objectMapper = objectMapper;
        }

        public IEnumerable<Person> SearchPerson(SearchPersonOptions options)
        {
            var phrases = options.Search.Split(' ').Select(n => n.ToLower()).ToArray();

            var people = _appDbContext.People
                .Where(n => phrases.All(p => (n.Name.ToLower() + n.Surname.ToLower()).Contains(p)))
                .GroupBy(n => new { n.Name, n.Surname, n.PostCode, n.City, n.Street, n.House, n.Flat, n.Pesel, n.Type, n.CompanyName, n.Regon, n.Nip, n.Email })
                .AsQueryable();

            if (options.Page > 0 && options.PageSize > 0)
            {
                people = people
                    .Skip((options.Page - 1))
                    .Take(options.Page * options.PageSize);
            }

            return _objectMapper.Map<IEnumerable<Person>>(people.Select(n => new PersonEntity()
            {
                Name = n.Key.Name,
                Surname = n.Key.Surname,
                PostCode = n.Key.PostCode,
                City = n.Key.City,
                Street = n.Key.Street,
                House = n.Key.House,
                Flat = n.Key.Flat,
                Pesel = n.Key.Pesel,
                Type = n.Key.Type,
                CompanyName = n.Key.CompanyName,
                Regon = n.Key.Regon,
                Nip = n.Key.Nip,
                Email = n.Key.Email
            }));
        }
    }
}
