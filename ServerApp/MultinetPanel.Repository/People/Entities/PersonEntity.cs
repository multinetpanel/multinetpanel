﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MultinetPanel.Repository.Clients.Entities;

namespace MultinetPanel.Repository.Invoices.Entities
{
    [Table("People")]
    public class PersonEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int Type { get; set; }
        public string Pesel { get; set; }
        public string Nip { get; set; }
        public string Regon { get; set; }
        public string IdentityCard { get; set; }
        public string Name { get; set; }
        public string Secondname { get; set; }
        public string Surname { get; set; }
        public string CompanyName { get; set; }
        public string CompanyShortName { get; set; }
        public DateTime Birthday { get; set; }
        public string Birthplace { get; set; }
        public int Sex { get; set; }
        public string Province { get; set; }
        public string District { get; set; }
        public string Community { get; set; }
        public string City { get; set; }
        public string PostCity { get; set; }
        public string PostCode { get; set; }
        public string Street { get; set; }
        public string House { get; set; }
        public string Flat { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string MobilePhone { get; set; }
        public string ServicePhone { get; set; }
        public string Fax { get; set; }
        public string Krs { get; set; }
    }
}