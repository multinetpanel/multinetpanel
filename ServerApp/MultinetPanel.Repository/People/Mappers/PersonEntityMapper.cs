﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Clients.Models;
using MultinetPanel.DomainModules.FinancialModule.Boundary.People.Models;
using MultinetPanel.Repository.Invoices.Entities;

namespace MultinetPanel.Repository.People.Mappers
{
    public class PersonEntityMapper : Profile
    {
        public PersonEntityMapper()
        {
            CreateMap<Person, PersonEntity>().ReverseMap();

//            CreateMap<Person, PersonEntity>()
//                .ForMember(n => n.Type, n => n.MapFrom(f => (int)f.Type));
//
//            CreateMap<PersonEntity, Person>()
//                .ForMember(n => n.Type, n => n.MapFrom(f => (PersonType)f.Type));
        }
    }
}
