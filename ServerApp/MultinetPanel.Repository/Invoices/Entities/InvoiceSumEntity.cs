﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MultinetPanel.Repository.Invoices.Entities
{
    public class InvoiceSumEntity
    {
        public bool IsPaid { get; set; }
        public decimal Vat { get; set; }
        public decimal Net { get; set; }
        public decimal Gross { get; set; }
        public int Count { get; set; }
    }
}
