﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using MultinetPanel.Repository.VatRates.Entities;

namespace MultinetPanel.Repository.Invoices.Entities
{
    [Table("InvoiceItems")]
    public class InvoiceItemEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int InvoiceId { get; set; }
        public int Index { get; set; }
        public string Name { get; set; }
        public string Classification { get; set; }
        public int MeasureUnitId { get; set; }
        public double Quantity { get; set; }
        public decimal NetUnitPrice { get; set; }
        public decimal GrossUnitPrice { get; set; }
        public int VatRateId { get; set; }
        public decimal Net { get; set; }
        public decimal Gross { get; set; }
        public decimal Vat { get; set; }

        [ForeignKey("InvoiceId")]
        public InvoiceEntity Invoice { get; set; }

        [ForeignKey("VatRateId")]
        public VatRateEntity VatRate { get; set; }
    }
}
