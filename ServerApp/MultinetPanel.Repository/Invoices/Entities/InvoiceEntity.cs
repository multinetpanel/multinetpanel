﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using MultinetPanel.Repository.Clients.Entities;
using MultinetPanel.Repository.Transfers.Entities;

namespace MultinetPanel.Repository.Invoices.Entities
{
    [Table("Invoices")]
    public class InvoiceEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string Number { get; set; }
        public DateTime InvoiceDate { get; set; }
        public DateTime SaleDate { get; set; }
        public DateTime PaymentDeadlineDate { get; set; }
        public int IssuerId { get; set; }
        public int PurchaserId { get; set; }
        public int ClientId { get; set; }

        [ForeignKey("IssuerId")]
        public PersonEntity Issuer { get; set; }

        [ForeignKey("PurchaserId")]
        public PersonEntity Purchaser { get; set; }

        [ForeignKey("ClientId")]
        public ClientEntity Client { get; set; }

        public ICollection<InvoiceItemEntity> InvoiceItems { get; set; }
        public ICollection<TransferInvoiceEntity> InvoiceTransfers { get; set; }
    }
}
