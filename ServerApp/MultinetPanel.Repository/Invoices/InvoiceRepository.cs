﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Microsoft.EntityFrameworkCore;
using MultinetPanel.Core.Boundary.Handlers;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Invoices.Models;
using MultinetPanel.Repository.Boundary.Invoices;
using MultinetPanel.Repository.Extensions;
using MultinetPanel.Repository.Invoices.Entities;

namespace MultinetPanel.Repository.Invoices
{
    public class InvoiceRepository : IInvoiceRepository
    {
        private readonly AppDbContext _appDbContext;
        private readonly IObjectMapper _objectMapper;

        public InvoiceRepository(AppDbContext appDbContext, IObjectMapper objectMapper)
        {
            _appDbContext = appDbContext;
            _objectMapper = objectMapper;
        }

        public Invoice GetInvoice(int id)
        {
            var invoice = _appDbContext.Invoices
                .Include(n => n.Purchaser)
                .Include(n => n.Issuer)
                .Include(n => n.InvoiceItems)
                .Include(n => n.InvoiceTransfers).ThenInclude(n => n.Transfer)
                .SingleOrDefault(n => n.Id == id);

            return _objectMapper.Map<Invoice>(invoice);
        }

        public IEnumerable<Invoice> GetInvoices(GetInvoicesOptions getInvoicesOptions)
        {
            var invoicesEntity = BuildGetInvoicesQuery(getInvoicesOptions)
                .OrderByDescending(n => n.InvoiceDate)
                .AddPagination(getInvoicesOptions);

            return _objectMapper.Map<IEnumerable<Invoice>>(invoicesEntity);
        }

        public InvoicesSum GetInvoicesSum(GetInvoicesOptions getInvoicesOptions)
        {
            var query = BuildGetInvoicesQuery(getInvoicesOptions)
                .Include(n => n.InvoiceItems)
                .Include(n => n.InvoiceTransfers)
                .Select(n => new InvoiceSumEntity
                {
                    IsPaid = n.InvoiceTransfers.Any(),
                    Vat = n.InvoiceItems.Sum(s => s.Vat),
                    Net = n.InvoiceItems.Sum(s => s.Net),
                    Gross = n.InvoiceItems.Sum(s => s.Gross)
                }).ToArray()
                .GroupBy(n => new { n.IsPaid })
                .Select(n => new InvoiceSumEntity()
                {
                    IsPaid = n.Key.IsPaid,
                    Vat = n.Sum(s => s.Vat),
                    Gross = n.Sum(s => s.Gross),
                    Net = n.Sum(s => s.Net),
                    Count = n.Count()
                })
                .ToArray();

            var paidInvoices = query.SingleOrDefault(n => n.IsPaid) ?? new InvoiceSumEntity();
            var noPaidInvoices = query.SingleOrDefault(n => !n.IsPaid) ?? new InvoiceSumEntity();

            return new InvoicesSum()
            {
                TotalCount = noPaidInvoices.Count + paidInvoices.Count,
                PaidCount = paidInvoices.Count,
                NoPaidCount = noPaidInvoices.Count,
   
                TotalNet = noPaidInvoices.Net + paidInvoices.Net,
                PaidNet = paidInvoices.Net,
                NoPaidNet = noPaidInvoices.Net,

                TotalVat = noPaidInvoices.Vat + paidInvoices.Vat,
                PaidVat = paidInvoices.Vat,
                NoPaidVat = noPaidInvoices.Vat,

                TotalGross = noPaidInvoices.Gross + paidInvoices.Gross,
                PaidGross = paidInvoices.Gross,
                NoPaidGross = noPaidInvoices.Gross
            };
        }

        public int GetInvoicesCount(GetInvoicesOptions getInvoicesOptions)
        {
            return BuildGetInvoicesQuery(getInvoicesOptions).Count();
        }

        private IQueryable<InvoiceEntity> BuildGetInvoicesQuery(GetInvoicesOptions getInvoicesOptions)
        {
            var personType = getInvoicesOptions.Type != null 
                ? (int)getInvoicesOptions.Type 
                : 0;

            return _appDbContext.Invoices
                .Include(n => n.Purchaser)
                .Include(n => n.InvoiceItems)
                .AddConditionIfNotNull(getInvoicesOptions.InvoiceDateFrom, n => n.InvoiceDate >= getInvoicesOptions.InvoiceDateFrom)
                .AddConditionIfNotNull(getInvoicesOptions.InvoiceDateTo, n => n.InvoiceDate <= getInvoicesOptions.InvoiceDateTo)
                .AddConditionIfNotNull(getInvoicesOptions.SaleDateFrom, n => n.SaleDate >= getInvoicesOptions.SaleDateFrom)
                .AddConditionIfNotNull(getInvoicesOptions.SaleDateTo, n => n.SaleDate <= getInvoicesOptions.SaleDateTo)
                .AddConditionIfNotNull(getInvoicesOptions.PaymentDeadlineDateFrom, n => n.PaymentDeadlineDate >= getInvoicesOptions.PaymentDeadlineDateFrom)
                .AddConditionIfNotNull(getInvoicesOptions.PaymentDeadlineDateTo, n => n.PaymentDeadlineDate <= getInvoicesOptions.PaymentDeadlineDateTo)
                .AddConditionIfNotNull(getInvoicesOptions.IsPaid, n => n.InvoiceTransfers.Any())
                .AddConditionIfNotNull(getInvoicesOptions.Type, n => n.Purchaser.Type == personType)
                .AddConditionIfNotNull(getInvoicesOptions.Pesel, n => n.Purchaser.Pesel.Contains(getInvoicesOptions.Pesel))
                .AddConditionIfNotNull(getInvoicesOptions.Regon, n => n.Purchaser.Regon.Contains(getInvoicesOptions.Regon))
                .AddConditionIfNotNull(getInvoicesOptions.Nip, n => n.Purchaser.Nip.Contains(getInvoicesOptions.Nip))
                .AddConditionIfNotNull(getInvoicesOptions.Phone, n => n.Purchaser.Phone.Contains(getInvoicesOptions.Phone))
                .AddConditionIfNotNull(getInvoicesOptions.Addrress, GetAddressCondition(getInvoicesOptions))
                .AddConditionIfNotNull(getInvoicesOptions.Number, n => n.Number.Contains(getInvoicesOptions.Number))
                .AddConditionIfNotNull(getInvoicesOptions.Net, n => n.InvoiceItems.Sum(s => s.Net) == getInvoicesOptions.Net)
                .AddConditionIfNotNull(getInvoicesOptions.Vat, n => n.InvoiceItems.Sum(s => s.Net + s.Gross) == getInvoicesOptions.Vat)
                .AddConditionIfNotNull(getInvoicesOptions.Gross, n => n.InvoiceItems.Sum(s => s.Gross) == getInvoicesOptions.Gross)
                .AsQueryable();
        }

        private Expression<Func<InvoiceEntity, bool>> GetAddressCondition(GetInvoicesOptions getInvoicesOptions)
        {
            if (string.IsNullOrWhiteSpace(getInvoicesOptions.Addrress))
            {
                return n => true;
            }

            var addressParts = getInvoicesOptions.Addrress.Split(' ').Select(n => n.ToLower()).ToArray();

            return n => new []
            {
                n.Purchaser.Street.ToLower(),
                n.Purchaser.House.ToLower(),
                n.Purchaser.Flat.ToLower(),
                n.Purchaser.PostCode.ToLower(),
                n.Purchaser.City.ToLower()
            }.Any(addressPart => addressParts.Any(addressPart.Contains));
        }
    }
}
