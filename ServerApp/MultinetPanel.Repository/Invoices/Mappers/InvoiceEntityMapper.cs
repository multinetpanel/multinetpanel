﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Invoices.Models;
using MultinetPanel.Repository.Invoices.Entities;

namespace MultinetPanel.Repository.Invoices.Mappers
{
    public class InvoiceEntityMapper : Profile
    {
        public InvoiceEntityMapper()
        {
            CreateMap<Invoice, InvoiceEntity>();

            CreateMap<InvoiceEntity, Invoice>()
                .ForMember(n => n.Transfers, n => n.MapFrom(f => f.InvoiceTransfers.Select(s => s.Transfer)));

            CreateMap<InvoiceItemEntity, InvoiceItem>();

            CreateMap<InvoiceItem, InvoiceItemEntity>();
        }
    }
}
