﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Clients.Models;
using MultinetPanel.Repository.Clients.Entities;

namespace MultinetPanel.Repository.Clients.Mappers
{
    public class ClientEntityMapper : Profile
    {
        public ClientEntityMapper()
        {
            CreateMap<Client, ClientEntity>().ReverseMap();
        }
    }
}
