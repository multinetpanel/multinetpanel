﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using MultinetPanel.Core.Boundary.Handlers;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Clients.Models;
using MultinetPanel.Repository.Boundary.Clients;

namespace MultinetPanel.Repository.Clients
{
    public class ClientRepository : IClientRepository
    {
        private readonly AppDbContext _appDbContext;
        private readonly IObjectMapper _objectMapper;

        public ClientRepository(AppDbContext appDbContext, IObjectMapper objectMapper)
        {
            _appDbContext = appDbContext;
            _objectMapper = objectMapper;
        }

        public Client GetClientById(int clientId)
        {
            var client = _appDbContext.Clients.Find(clientId);

            return _objectMapper.Map<Client>(client);
        }

        public IEnumerable<Client> SearchClient(SearchClientOptions options)
        {
            var phrases = options.Search.Split(' ').Select(n => n.ToLower()).ToArray();

            var clients = _appDbContext.Clients
                .Include(n => n.Person)
                .Where(n => phrases.All(p => (n.Person.Name.ToLower() + n.Person.Surname.ToLower()).Contains(p)))
                .AsQueryable();

            if (options.Page > 0 && options.PageSize > 0)
            {
                clients = clients
                    .Skip((options.Page - 1))
                    .Take(options.Page * options.PageSize);
            }

            return _objectMapper.Map<IEnumerable<Client>>(clients.ToArray());
        }
    }
}
