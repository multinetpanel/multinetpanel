﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using MultinetPanel.DomainModules.FinancialModule.Boundary.VatRates.Models;
using MultinetPanel.Repository.VatRates.Entities;

namespace MultinetPanel.Repository.VatRates.Mappers
{
    public class VatRateEntityMapper : Profile
    {
        public VatRateEntityMapper()
        {
            CreateMap<VatRateEntity, VatRate>().ReverseMap();
        }
    }
}
