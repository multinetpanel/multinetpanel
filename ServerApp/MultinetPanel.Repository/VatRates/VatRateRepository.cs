﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MultinetPanel.Core.Boundary.Handlers;
using MultinetPanel.DomainModules.FinancialModule.Boundary.VatRates.Models;
using MultinetPanel.Repository.Boundary.VatRates;

namespace MultinetPanel.Repository.VatRates
{
    public class VatRateRepository : IVatRateRepository
    {
        private readonly AppDbContext _appDbContext;
        private readonly IObjectMapper _objectMapper;

        public VatRateRepository(AppDbContext appDbContext, IObjectMapper objectMapper)
        {
            _appDbContext = appDbContext;
            _objectMapper = objectMapper;
        }

        public IEnumerable<VatRate> GetVatRates()
        {
            var vatRates = _appDbContext.VatRates
                .OrderByDescending(n => n.Value)
                .ThenBy(n => n.Id)
                .ToArray();

            return _objectMapper.Map<IEnumerable<VatRate>>(vatRates);
        }
    }
}
