﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MultinetPanel.Repository.Boundary;
using MultinetPanel.Repository.Boundary.Clients;
using MultinetPanel.Repository.Boundary.Invoices;
using MultinetPanel.Repository.Boundary.Migrations;
using MultinetPanel.Repository.Boundary.People;
using MultinetPanel.Repository.Boundary.Transfers;
using MultinetPanel.Repository.Boundary.VatRates;
using MultinetPanel.Repository.Clients;
using MultinetPanel.Repository.Invoices;
using MultinetPanel.Repository.Migrations;
using MultinetPanel.Repository.People;
using MultinetPanel.Repository.Transfers;
using MultinetPanel.Repository.VatRates;

namespace MultinetPanel.Repository
{
    public static class RepositoryDependencyResolver
    {
        public static void AddRepository(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<AppDbContext>(options => options.UseNpgsql(configuration.GetConnectionString("DefaultConnection")));

            services.AddTransient<UserSeeder>();
            services.AddTransient<VatRatesSeeder>();

            services.AddTransient<IUnitOfWork, UnitOfWork>();
            services.AddTransient<ITransferRepository, TransferRepository>();
            services.AddTransient<IInvoiceRepository, InvoiceRepository>();
            services.AddTransient<IClientRepository, ClientRepository>();
            services.AddTransient<IPersonRepository, PersonRepository>();
            services.AddTransient<IVatRateRepository, VatRateRepository>();
            services.AddTransient<ITransferImporterLogsRepository, TransferImporterLogsRepository>();
            services.AddTransient<ISeederManager, SeederManager>();
        }
    }
}
