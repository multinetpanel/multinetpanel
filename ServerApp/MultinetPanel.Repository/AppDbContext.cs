﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using MultinetPanel.Repository.Clients.Entities;
using MultinetPanel.Repository.Invoices.Entities;
using MultinetPanel.Repository.Migrations;
using MultinetPanel.Repository.Transfers.Entities;
using MultinetPanel.Repository.Users.Entities;
using MultinetPanel.Repository.VatRates.Entities;

namespace MultinetPanel.Repository
{
    public class AppDbContext : IdentityDbContext<UserEntity>
    {
        public DbSet<TransferEntity> Transfers { get; set; }
        public DbSet<TransferIndexerLogEntity> TransferIndexerLogs { get; set; }
        public DbSet<TransferInvoiceEntity> TransferInvoices { get; set; }
        public DbSet<TransfersImportLogEntity> TransfersImportLogs { get; set; }
        public DbSet<InvoiceEntity> Invoices { get; set; }
        public DbSet<InvoiceItemEntity> InvoiceItems { get; set; }
        public DbSet<VatRateEntity> VatRates { get; set; }
        public DbSet<ClientEntity> Clients { get; set; }
        public DbSet<PersonEntity> People { get; set; }

        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options)
        {
            
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("multinet");

            base.OnModelCreating(modelBuilder);
        }

        public void RunMigrations()
        {
            if (Database.GetPendingMigrations().Any())
            {
                Database.Migrate();
            }
        }
    }
}
