﻿using System;
using System.Collections.Generic;
using System.Text;
using MultinetPanel.Core.Boundary.EmailHandlers.Models;

namespace MultinetPanel.Core.Boundary.EmailHandlers
{
    public interface IEmailHandler
    {
        IEnumerable<EmailMessage> GetMessages(EmailServerCredentials credentials, DateTime fromDate);
        IEnumerable<EmailMessage> GetMessages(EmailServerCredentials credentials, Func<EmailMessage, bool> condition);
    }
}
