﻿using System;
using System.Collections.Generic;

namespace MultinetPanel.Core.Boundary.EmailHandlers.Models
{
    public class EmailMessage
    {
        public string Subject { get; set; }
        public string Content { get; set; }
        public DateTime Date { get; set; }

        public IEnumerable<EmailMessageAttachment> Attachments { get; set; }
    }
}