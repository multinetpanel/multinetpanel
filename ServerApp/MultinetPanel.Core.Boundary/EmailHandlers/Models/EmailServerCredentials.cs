﻿namespace MultinetPanel.Core.Boundary.EmailHandlers.Models
{
    public class EmailServerCredentials
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Host { get; set; }
        public int Port { get; set; }
    }
}