﻿namespace MultinetPanel.Core.Boundary.EmailHandlers.Models
{
    public class EmailMessageAttachment
    {
        public string Name { get; set; }
        public byte[] Attachment { get; set; }
    }
}