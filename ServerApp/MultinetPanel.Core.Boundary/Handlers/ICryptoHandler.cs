﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MultinetPanel.Core.Boundary.Handlers
{
    public interface ICryptoHandler
    {
        string Md5Hash(string input);
        string Sha256Hash(string input);
    }
}
