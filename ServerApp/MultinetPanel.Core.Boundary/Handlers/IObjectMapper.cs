﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MultinetPanel.Core.Boundary.Handlers
{
    public interface IObjectMapper
    {
        TDestination Map<TDestination>(object source);
    }
}
