﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MultinetPanel.Core.Boundary.Serializers
{
    public interface ICsvSerializer
    {
        IEnumerable<TDestination> Deserialize<TDestination>(string csv);
    }
}
