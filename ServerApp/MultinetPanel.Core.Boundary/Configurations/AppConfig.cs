﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MultinetPanel.Core.Boundary.Configurations
{
    public class AppConfig
    {
        public string ConnectionString { get; set; }
        public string AppUrl { get; set; }
        public string OldAuthHash { get; set; }
    }
}
