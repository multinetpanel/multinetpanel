﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MultinetPanel.Core.Boundary.CoreModels
{
    public class PagedModel
    {
        public int Page { get; set; }
        public int PageSize { get; set; }
    }
}
