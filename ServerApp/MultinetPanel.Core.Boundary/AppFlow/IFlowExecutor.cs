﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using MediatR;

namespace MultinetPanel.Core.Boundary.AppFlow
{
    public interface IFlowExecutor
    {
        Task Send(IRequest request);
        Task<TResponse> Send<TResponse>(IRequest<TResponse> request);
    }
}
