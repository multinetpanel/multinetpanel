﻿using System;
using System.Collections.Generic;
using System.Text;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Models;

namespace MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Parsers
{
    public interface ITransferParser
    {
        IEnumerable<Transfer> GetTransfers(string transfers);
        IEnumerable<Transfer> GetTransfers(byte[] transfers, Encoding encoding);
    }
}
