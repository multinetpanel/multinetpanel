﻿using System;
using System.Collections.Generic;
using System.Text;
using MultinetPanel.Core.Boundary.CoreModels;

namespace MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Models
{
    public class GetTransfersOptions : PagedModel
    {
        public bool? HasInvoices { get; set; }
        public string SenderAccountNumber { get; set; }
        public string SenderName { get; set; }
        public string SenderStreet { get; set; }
        public string SenderCity { get; set; }
        public string Receiver { get; set; }
        public string Title { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
    }
}
