﻿using System;
using System.Text;

namespace MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Models
{
    public class Transfer
    {
        public int Id { get; set; }
        public decimal Value { get; set; }
        public string SenderAccountNumber { get; set; }
        public string SenderName { get; set; }
        public string SenderStreet { get; set; }
        public string SenderCity { get; set; }
        public string Receiver { get; set; }
        public string Title { get; set; }
        public DateTime Date { get; set; }
        public bool HasInvoice { get; set; }
    }
}
