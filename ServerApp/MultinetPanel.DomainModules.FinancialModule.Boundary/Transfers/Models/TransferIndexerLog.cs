﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Models
{
    public class TransferIndexerLog
    {
        public int Id { get; set; }
        public int TransferId { get; set; }
        public string Message { get; set; }
        public DateTime Date { get; set; }
    }
}
