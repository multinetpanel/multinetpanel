﻿using System.Collections.Generic;
using System.Linq;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Invoices.Models;

namespace MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Models
{
    public class DetailedTransfer : Transfer
    {
        public IEnumerable<Invoice> Invoices { get; set; }
        public IEnumerable<TransferIndexerLog> Logs { get; set; }
    }
}