﻿using MediatR;

namespace MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Requests
{
    public class GetTransfersImportLogsCountRequest : IRequest<int>
    {

    }
}