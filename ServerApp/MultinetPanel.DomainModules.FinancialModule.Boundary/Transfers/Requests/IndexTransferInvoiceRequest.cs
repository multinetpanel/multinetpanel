﻿using MediatR;

namespace MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Requests
{
    public class IndexTransferInvoiceRequest : IRequest
    {
        public int TransferId { get; set; }
        public int InvoiceId { get; set; }
    }
}