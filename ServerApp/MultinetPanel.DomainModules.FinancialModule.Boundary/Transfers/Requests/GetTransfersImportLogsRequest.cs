﻿using System.Collections.Generic;
using MediatR;
using MultinetPanel.Core.Boundary.CoreModels;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Importers.Models;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Models;

namespace MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Requests
{
    public class GetTransfersImportLogsRequest : PagedModel, IRequest<IEnumerable<TransfersImportLog>>
    {

    }
}