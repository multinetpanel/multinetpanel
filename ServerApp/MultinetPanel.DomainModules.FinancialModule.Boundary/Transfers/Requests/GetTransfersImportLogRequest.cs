﻿using MediatR;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Importers.Models;

namespace MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Requests
{
    public class GetTransfersImportLogRequest : IRequest<TransfersImportLog>
    {
        public int Id { get; set; }
    }
}