﻿using System.Collections.Generic;
using MediatR;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Models;

namespace MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Requests
{
    public class GetTransfersCountRequest : GetTransfersOptions, IRequest<int>
    {

    }
}