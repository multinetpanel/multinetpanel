﻿using MediatR;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Models;

namespace MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Requests
{
    public class GetDetailedTransferRequest : IRequest<DetailedTransfer>
    {
        public int Id { get; set; }
    }
}