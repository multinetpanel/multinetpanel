﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Models;

namespace MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Requests
{
    public class GetTransfersRequest : GetTransfersOptions, IRequest<IEnumerable<Transfer>>
    {
    }
}
