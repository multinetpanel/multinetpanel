﻿using System;
using System.Collections.Generic;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Invoices.Models;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Models;

namespace MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Indexers
{
    public interface ITransferIndexerAttempt
    {
        IEnumerable<Invoice> SearchInvoices(Transfer transfer, Action<string> callback);
    }
}