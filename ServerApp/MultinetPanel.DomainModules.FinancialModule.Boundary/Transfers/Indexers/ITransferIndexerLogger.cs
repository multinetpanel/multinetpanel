﻿using MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Models;

namespace MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Indexers
{
    public interface ITransferIndexerLogger
    {
        void Log(Transfer transfer, string msg);
    }
}