﻿using System;
using System.Collections.Generic;
using System.Text;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Models;

namespace MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Indexers
{
    public interface ITransferIndexer
    {
        void IndexTransfers();
        bool IndexTransfer(Transfer transfer);
    }
}
