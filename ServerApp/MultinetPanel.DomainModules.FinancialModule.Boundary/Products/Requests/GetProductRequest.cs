﻿using MediatR;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Clients.Models;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Products.Models;

namespace MultinetPanel.DomainModules.FinancialModule.Boundary.Products.Requests
{
    public class GetProductRequest : IRequest<Product>
    {
        public int Id { get; set; }
    }
}