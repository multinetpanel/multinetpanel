﻿using System;
using System.Collections.Generic;
using System.Text;
using MultinetPanel.Core.Boundary.CoreModels;

namespace MultinetPanel.DomainModules.FinancialModule.Boundary.People.Models
{
    public class SearchPersonOptions : PagedModel
    {
        public string Search { get; set; }
    }
}
