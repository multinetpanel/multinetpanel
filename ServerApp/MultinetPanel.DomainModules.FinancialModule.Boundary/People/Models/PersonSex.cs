﻿namespace MultinetPanel.DomainModules.FinancialModule.Boundary.People.Models
{
    public enum PersonSex
    {
        Male = 1,
        Female = 2
    }
}