﻿using System;
using System.Linq;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Clients.Models;

namespace MultinetPanel.DomainModules.FinancialModule.Boundary.People.Models
{
    public class Person
    {
        public int Id { get; set; }
        public PersonType Type { get; set; }
        public string Pesel { get; set; }
        public string Nip { get; set; }
        public string Regon { get; set; }
        public string IdentityCard { get; set; }
        public string Name { get; set; }
        public string Secondname { get; set; }
        public string Surname { get; set; }
        public string CompanyName { get; set; }
        public string CompanyShortName { get; set; }
        public DateTime Birthday { get; set; }
        public string Birthplace { get; set; }
        public PersonSex Sex { get; set; }
        public string Province { get; set; }
        public string District { get; set; }
        public string Community { get; set; }
        public string City { get; set; }
        public string PostCity { get; set; }
        public string PostCode { get; set; }
        public string Street { get; set; }
        public string House { get; set; }
        public string Flat { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string MobilePhone { get; set; }
        public string ServicePhone { get; set; }
        public string Fax { get; set; }
        public string Krs { get; set; }

        public string NameDisplay => GetName();
        public string AddressDisplay => GetAddress();

        private string GetName()
        {
            if (Type == PersonType.Company)
            {
                return !string.IsNullOrWhiteSpace(CompanyShortName) 
                    ? CompanyShortName 
                    : CompanyName;
            }

            var name = new[] { Name, Secondname, Surname };

            return string.Join(" ", name.Where(n => !string.IsNullOrWhiteSpace(n)));
        }

        private string GetAddress()
        {
            var house = string.IsNullOrWhiteSpace(Flat) 
                ? House 
                : House + "/" + Flat;

            return Street + " " + house;
        }

    }
}