﻿namespace MultinetPanel.DomainModules.FinancialModule.Boundary.People.Models
{
    public enum PersonType
    {
        Person = 1,
        Company = 2
    }
}