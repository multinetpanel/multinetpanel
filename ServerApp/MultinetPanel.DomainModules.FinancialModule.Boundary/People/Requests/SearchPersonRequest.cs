﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;
using MultinetPanel.Core.Boundary.CoreModels;
using MultinetPanel.DomainModules.FinancialModule.Boundary.People.Models;

namespace MultinetPanel.DomainModules.FinancialModule.Boundary.People.Requests
{
    public class SearchPersonRequest : SearchPersonOptions, IRequest<IEnumerable<Person>>
    {
        
    }
}
