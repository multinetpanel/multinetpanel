﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;
using MultinetPanel.DomainModules.FinancialModule.Boundary.VatRates.Models;

namespace MultinetPanel.DomainModules.FinancialModule.Boundary.VatRates.Requests
{
    public class GetVatRatesRequest : IRequest<IEnumerable<VatRate>>
    {
    }
}
