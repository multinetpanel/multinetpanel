﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MultinetPanel.DomainModules.FinancialModule.Boundary.VatRates.Models
{
    public class VatRate
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Value { get; set; }
    }
}
