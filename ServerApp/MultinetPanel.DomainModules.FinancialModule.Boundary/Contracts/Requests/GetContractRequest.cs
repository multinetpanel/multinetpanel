﻿using MediatR;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Clients.Models;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Contracts.Models;

namespace MultinetPanel.DomainModules.FinancialModule.Boundary.Contracts.Requests
{
    public class GetContractRequest : IRequest<Contract>
    {
        public int Id { get; set; }
    }
}