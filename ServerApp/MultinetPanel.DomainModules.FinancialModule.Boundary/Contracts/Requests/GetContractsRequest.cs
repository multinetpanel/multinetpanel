﻿using System.Collections.Generic;
using MediatR;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Clients.Models;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Contracts.Models;

namespace MultinetPanel.DomainModules.FinancialModule.Boundary.Contracts.Requests
{
    public class GetContractsRequest : GetContractsOptions, IRequest<IEnumerable<Contract>>
    {

    }
}
