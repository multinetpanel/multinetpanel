﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Clients.Models;
using MultinetPanel.DomainModules.FinancialModule.Boundary.People.Models;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Models;

namespace MultinetPanel.DomainModules.FinancialModule.Boundary.Invoices.Models
{
    public class Invoice
    {
        public int Id { get; set; }
        public string Number { get; set; }
        public DateTime InvoiceDate { get; set; }
        public DateTime SaleDate { get; set; }
        public DateTime PaymentDeadlineDate { get; set; }
        public int? ClientId { get; set; }
        public int IssuerId { get; set; }
        public int PurchaserId { get; set; }
        public Client Client { get; set; }
        public decimal Net => InvoiceItems.Sum(n => n.Net);
        public decimal Vat => InvoiceItems.Sum(n => n.Vat);
        public decimal Gross => InvoiceItems.Sum(n => n.Gross);        
        public Person Issuer { get; set; }
        public Person Purchaser { get; set; }
        public IEnumerable<InvoiceItem> InvoiceItems { get; set; }
        public IEnumerable<Transfer> Transfers { get; set; }
    }
}
