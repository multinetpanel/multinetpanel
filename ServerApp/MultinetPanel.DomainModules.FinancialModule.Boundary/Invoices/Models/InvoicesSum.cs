﻿namespace MultinetPanel.DomainModules.FinancialModule.Boundary.Invoices.Models
{
    public class InvoicesSum
    {
        public int TotalCount { get; set; }
        public int PaidCount { get; set; }
        public int NoPaidCount { get; set; }

        public decimal TotalNet { get; set; }
        public decimal PaidNet { get; set; }
        public decimal NoPaidNet { get; set; }

        public decimal TotalVat { get; set; }
        public decimal PaidVat { get; set; }
        public decimal NoPaidVat { get; set; }

        public decimal TotalGross { get; set; }
        public decimal PaidGross { get; set; }
        public decimal NoPaidGross { get; set; }
    }
}