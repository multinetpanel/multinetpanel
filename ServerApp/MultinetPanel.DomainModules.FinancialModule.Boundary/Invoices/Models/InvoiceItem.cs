﻿namespace MultinetPanel.DomainModules.FinancialModule.Boundary.Invoices.Models
{
    public class InvoiceItem
    {
        public int Id { get; set; }
        public int InvoiceId { get; set; }
        public int Index { get; set; }
        public string Name { get; set; }
        public string Classification { get; set; }
        public int MeasureUnitId { get; set; }
        public double Quantity { get; set; }
        public decimal NetUnitPrice { get; set; }
        public decimal GrossUnitPrice { get; set; }
        public double VatRate { get; set; }
        public decimal Net { get; set; }
        public decimal Gross { get; set; }
        public decimal Vat { get; set; }
    }
}