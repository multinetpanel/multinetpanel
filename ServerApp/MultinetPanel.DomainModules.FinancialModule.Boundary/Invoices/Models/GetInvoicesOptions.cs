﻿using System;
using System.Collections.Generic;
using System.Text;
using MultinetPanel.Core.Boundary.CoreModels;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Clients.Models;
using MultinetPanel.DomainModules.FinancialModule.Boundary.People.Models;


namespace MultinetPanel.DomainModules.FinancialModule.Boundary.Invoices.Models
{
    public class GetInvoicesOptions : PagedModel
    {        
        public PersonType? Type { get; set; }
        public string Pesel { get; set; }
        public string Regon { get; set; }
        public string Nip { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Addrress { get; set; }
        public string Number { get; set; }
        public bool? IsPaid { get; set; }
        public decimal? Net { get; set; }
        public decimal? Vat { get; set; }
        public decimal? Gross { get; set; }
        public DateTime? InvoiceDateFrom { get; set; }
        public DateTime? InvoiceDateTo { get; set; }
        public DateTime? SaleDateFrom { get; set; }
        public DateTime? SaleDateTo { get; set; }
        public DateTime? PaymentDeadlineDateFrom { get; set; }
        public DateTime? PaymentDeadlineDateTo { get; set; }
    }
}
