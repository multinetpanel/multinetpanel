﻿using MediatR;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Invoices.Models;

namespace MultinetPanel.DomainModules.FinancialModule.Boundary.Invoices.Requests
{
    public class AddInvoiceRequest : IRequest
    {
        public Invoice Invoice { get; set; }
    }
}