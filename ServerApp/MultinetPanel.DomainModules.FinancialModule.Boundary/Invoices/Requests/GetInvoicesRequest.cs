﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Invoices.Models;

namespace MultinetPanel.DomainModules.FinancialModule.Boundary.Invoices.Requests
{
    public class GetInvoicesRequest : GetInvoicesOptions, IRequest<IEnumerable<Invoice>>
    {

    }
}
