﻿using MediatR;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Invoices.Models;

namespace MultinetPanel.DomainModules.FinancialModule.Boundary.Invoices.Requests
{
    public class GetInvoiceRequest : IRequest<Invoice>
    {
        public int Id { get; set; }
    }
}