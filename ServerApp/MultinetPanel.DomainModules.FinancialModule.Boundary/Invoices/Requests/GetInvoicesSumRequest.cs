﻿using MediatR;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Invoices.Models;

namespace MultinetPanel.DomainModules.FinancialModule.Boundary.Invoices.Requests
{
    public class GetInvoicesSumRequest : GetInvoicesOptions, IRequest<InvoicesSum>
    {

    }
}