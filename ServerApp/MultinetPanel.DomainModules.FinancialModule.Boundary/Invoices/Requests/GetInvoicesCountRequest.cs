﻿using MediatR;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Invoices.Models;

namespace MultinetPanel.DomainModules.FinancialModule.Boundary.Invoices.Requests
{
    public class GetInvoicesCountRequest : GetInvoicesOptions, IRequest<int>
    {

    }
}