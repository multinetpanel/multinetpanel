﻿using System.Collections.Generic;
using System.Text;
using MultinetPanel.DomainModules.FinancialModule.Boundary.People.Models;

namespace MultinetPanel.DomainModules.FinancialModule.Boundary.Clients.Models
{
    public class Client
    {
        public int Id { get; set; }
        public int PersonId { get; set; }
        public Person Person { get; set; }
    }
}
