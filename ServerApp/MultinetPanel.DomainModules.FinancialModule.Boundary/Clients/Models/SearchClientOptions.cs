﻿using MultinetPanel.Core.Boundary.CoreModels;

namespace MultinetPanel.DomainModules.FinancialModule.Boundary.Clients.Models
{
    public class SearchClientOptions : PagedModel
    {
        public string Search { get; set; }
    }
}