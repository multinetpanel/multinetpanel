﻿using System.Collections.Generic;
using MediatR;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Clients.Models;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Invoices.Models;

namespace MultinetPanel.DomainModules.FinancialModule.Boundary.Clients.Requests
{
    public class GetClientsRequest : GetClientsOptions, IRequest<IEnumerable<Client>>
    {

    }
}
