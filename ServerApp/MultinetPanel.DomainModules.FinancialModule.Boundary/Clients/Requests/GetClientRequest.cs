﻿using MediatR;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Clients.Models;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Invoices.Models;

namespace MultinetPanel.DomainModules.FinancialModule.Boundary.Clients.Requests
{
    public class GetClientRequest : IRequest<Client>
    {
        public int Id { get; set; }
    }
}