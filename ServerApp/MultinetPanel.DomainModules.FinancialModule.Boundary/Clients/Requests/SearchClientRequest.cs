﻿using System.Collections.Generic;
using MediatR;
using MultinetPanel.Core.Boundary.CoreModels;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Clients.Models;
using MultinetPanel.DomainModules.FinancialModule.Boundary.People.Models;

namespace MultinetPanel.DomainModules.FinancialModule.Boundary.Clients.Requests
{
    public class SearchClientRequest : SearchClientOptions, IRequest<IEnumerable<Client>>
    {
        
    }
}