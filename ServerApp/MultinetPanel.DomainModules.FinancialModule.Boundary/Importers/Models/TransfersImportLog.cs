﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MultinetPanel.DomainModules.FinancialModule.Boundary.Importers.Models
{
    public class TransfersImportLog
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public bool IsSuccessful { get; set; }
        public int TransfersCount { get; set; }
        public string Message { get; set; }
    }
}
