﻿using MultinetPanel.Core.Boundary.CoreModels;

namespace MultinetPanel.DomainModules.FinancialModule.Boundary.Importers.Models
{
    public class GetTransfersImportLogsOptions : PagedModel
    {
        public bool? IsSuccessful { get; set; }
    }
}