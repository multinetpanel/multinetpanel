﻿using System;
using System.Collections.Generic;
using System.Text;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Importers.Models;

namespace MultinetPanel.DomainModules.FinancialModule.Boundary.Importers
{
    public interface ITransfersImporterLogger
    {
        void Log(TransfersImportLog log);
    }
}
