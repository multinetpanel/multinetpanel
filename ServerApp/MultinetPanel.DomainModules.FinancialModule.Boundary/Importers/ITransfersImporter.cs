﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MultinetPanel.DomainModules.FinancialModule.Boundary.Importers
{
    public interface ITransfersImporter
    {
        void Import();
    }
}
