﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MultinetPanel.DataSyncApp.Definitions
{
    public interface ISourceSyncHandler
    {
        void Sync();
    }
}
