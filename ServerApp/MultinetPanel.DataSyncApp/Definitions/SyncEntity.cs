﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MultinetPanel.DataSyncApp.Definitions
{
    public class SyncEntity<TEntity>
    {
        public int SourceId { get; set; }
        public TEntity DestinationEntity { get; set; }
    }
}
