﻿namespace MultinetPanel.DataSyncApp.Definitions
{
    public interface IEntitySync
    {
        string Name { get; }
        void Sync();
    }
}