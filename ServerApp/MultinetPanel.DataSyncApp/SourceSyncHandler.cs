﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using MultinetPanel.DataSyncApp.Definitions;
using MultinetPanel.DataSyncApp.SourceSystemDb;
using MultinetPanel.DataSyncApp.SyncInfoDb;
using MultinetPanel.DataSyncApp.SyncInfoDb.SyncHandlers;
using MultinetPanel.Repository;
using Serilog;

namespace MultinetPanel.DataSyncApp
{
    public class SourceSyncHandler : ISourceSyncHandler
    {
        private readonly SyncInfoDbContext _syncInfoDbContext;
        private readonly IServiceProvider _serviceProvider;

        public SourceSyncHandler(SyncInfoDbContext syncInfoDbContext, IServiceProvider serviceProvider)
        {
            _syncInfoDbContext = syncInfoDbContext;
            _serviceProvider = serviceProvider;

            SyncMigrations();
        }

        public void Sync()
        {
            var syncHandlers = GetSyncHandlers();

            foreach (var syncHandler in syncHandlers)
            {
                Log.Information("Synchronizuję {Name}", syncHandler.Name);

                syncHandler.Sync();
            }
        }

        private IEnumerable<IEntitySync> GetSyncHandlers()
        {
            return new IEntitySync[]
            {
                _serviceProvider.GetService<ClientSyncHandler>(),
                _serviceProvider.GetService<InvoiceSyncHandler>(),
                _serviceProvider.GetService<TransferSyncHandler>(),
                _serviceProvider.GetService<UserSyncHandler>()
            };
        }

        private void SyncMigrations()
        {
            if (_syncInfoDbContext.Database.GetPendingMigrations().Any())
            {
                _syncInfoDbContext.Database.Migrate();
            }
        }
    }
}
