﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using MultinetPanel.DataSyncApp.Definitions;
using MultinetPanel.DataSyncApp.SourceSystemDb;
using MultinetPanel.DataSyncApp.SyncInfoDb;
using MultinetPanel.DataSyncApp.SyncInfoDb.SyncHandlers;
using MultinetPanel.Repository;
using MultinetPanel.Repository.Users.Entities;

namespace MultinetPanel.DataSyncApp
{
    public class ProgramDependencyResolver
    {
        public static IServiceProvider CreateDependency(SyncConfiguration configuration)
        {
            var services = new ServiceCollection();

            services.AddDbContext<SyncInfoDbContext>(options =>
                options.UseNpgsql(configuration.DestinationConnectionString));

            services.AddDbContext<AppDbContext>(options =>
                options.UseNpgsql(configuration.DestinationConnectionString));

            services.AddDbContext<SourceSystemDbContext>(options =>
                options.UseMySql(configuration.SourceConnectionString));

            services.AddTransient<ISourceSyncHandler, SourceSyncHandler>();

            services.AddTransient<ClientSyncHandler>();
            services.AddTransient<InvoiceSyncHandler>();
            services.AddTransient<TransferSyncHandler>();
            services.AddTransient<UserSyncHandler>();

            AddAuth(services);

            return services.BuildServiceProvider();
        }

        private static void AddAuth(ServiceCollection services)
        {
            services.AddIdentity<UserEntity, IdentityRole>(options =>
                {
                    options.Password.RequireDigit = false;
                    options.Password.RequiredLength = 3;
                    options.Password.RequireNonAlphanumeric = false;
                    options.Password.RequireUppercase = false;
                    options.Password.RequireLowercase = false;
                })
                .AddEntityFrameworkStores<AppDbContext>()
                .AddDefaultTokenProviders();
        }
    }

    public class SyncConfiguration
    {
        public string SourceConnectionString { get; set; }
        public string DestinationConnectionString { get; set; }
    }
}
