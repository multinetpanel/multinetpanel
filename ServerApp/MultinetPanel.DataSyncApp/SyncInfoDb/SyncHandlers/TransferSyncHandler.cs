﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text.RegularExpressions;
using Microsoft.EntityFrameworkCore;
using MultinetPanel.DataSyncApp.Definitions;
using MultinetPanel.DataSyncApp.SourceSystemDb;
using MultinetPanel.DataSyncApp.SyncInfoDb.SyncEntities;
using MultinetPanel.Repository;
using MultinetPanel.Repository.Clients.Entities;
using MultinetPanel.Repository.Transfers.Entities;
using Serilog;

namespace MultinetPanel.DataSyncApp.SyncInfoDb.SyncHandlers
{
    public class TransferSyncHandler : IEntitySync
    {
        public int PartSize => 1000;
        public string Name => "Przelew";

        private readonly SourceSystemDbContext _sourceDbContext;
        private readonly AppDbContext _appDbContext;
        private readonly SyncInfoDbContext _syncInfoDbContext;

        public TransferSyncHandler(SourceSystemDbContext sourceDbContext, AppDbContext appDbContext, SyncInfoDbContext syncInfoDbContext)
        {
            _sourceDbContext = sourceDbContext;
            _appDbContext = appDbContext;
            _syncInfoDbContext = syncInfoDbContext;
        }

        public void Sync()
        {
            SyncPart(1);
        }

        private void SyncPart(int part)
        {
            var transfers = GetTransfersPart(part).ToArray();

            if (!transfers.Any())
            {
                return;
            }

            _appDbContext.SaveChanges();

            var syncInfo = transfers.Select(n => new TransferSyncEntity()
            {
                SourceId = n.SourceId,
                DestinationId = n.DestinationEntity.Id
            });

            _syncInfoDbContext.TransferSync.AddRange(syncInfo);

            _syncInfoDbContext.SaveChanges();

            SyncPart(++part);
        }

        private IEnumerable<SyncEntity<TransferEntity>> GetTransfersPart(int part)
        {
            var transfers = _sourceDbContext
                .Transfers
                .Include(n => n.Invoices)
                .Skip((part - 1) * PartSize)
                .Take(part * PartSize)
                .OrderBy(n => n.Id)
                .ToArray();

            foreach (var transfer in transfers)
            {
                if (_syncInfoDbContext.TransferSync.Any(n => n.SourceId == transfer.Id))
                {
                    Log.Debug("Taki przelew znajduje się już w docelowym systemie. Tytuł: {Title} Id: {Id}", transfer.Title, transfer.Id);

                    continue;
                }

                Log.Information("Dodaję przelew do systemu docelowego. Tytuł: {Title} Id: {Id}", transfer.Title, transfer.Id);

                var invoices = _syncInfoDbContext.InvoiceSync.Where(n => transfer.Invoices.Select(i => i.Id).Contains(n.SourceId));

                var entity = new TransferEntity()
                {
                    Receiver = transfer.Receiver,
                    Value = transfer.Price,
                    Title = transfer.Title,
                    Date = transfer.Date ?? DateTime.MinValue,
                    SenderName = transfer.Sender,
                    SenderCity = string.Empty,
                    SenderStreet = string.Empty,
                    SenderAccountNumber = Regex.Match(transfer.BankAccount, "[0-9]+").Value,
                    TransferInvoices = invoices.Select(n => new TransferInvoiceEntity()
                    {
                        InvoiceId = n.DestinationId
                    }).ToList()
                };

                _appDbContext.Transfers.Add(entity);

                yield return new SyncEntity<TransferEntity>()
                {
                    SourceId = transfer.Id,
                    DestinationEntity = entity
                };
            }
        }
    }
}