﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Microsoft.AspNetCore.Identity;
using MultinetPanel.DataSyncApp.Definitions;
using MultinetPanel.DataSyncApp.SourceSystemDb;
using MultinetPanel.DataSyncApp.SourceSystemDb.Entities;
using MultinetPanel.DataSyncApp.SyncInfoDb.SyncEntities;
using MultinetPanel.Repository;
using MultinetPanel.Repository.Clients.Entities;
using MultinetPanel.Repository.Invoices.Entities;
using MultinetPanel.Repository.Users.Entities;
using Serilog;

namespace MultinetPanel.DataSyncApp.SyncInfoDb.SyncHandlers
{
    public class UserSyncHandler : IEntitySync
    {
        public int PartSize => 1000;
        public string Name => "Użytkownik";

        private readonly SourceSystemDbContext _sourceDbContext;
        private readonly AppDbContext _appDbContext;
        private readonly SyncInfoDbContext _syncInfoDbContext;
        private readonly SignInManager<UserEntity> _signInManager;
        private readonly UserManager<UserEntity> _userManager;

        public UserSyncHandler(
            SourceSystemDbContext sourceDbContext, 
            AppDbContext appDbContext, 
            SyncInfoDbContext syncInfoDbContext,
            SignInManager<UserEntity> signInManager,
            UserManager<UserEntity> userManager)
        {
            _sourceDbContext = sourceDbContext;
            _appDbContext = appDbContext;
            _syncInfoDbContext = syncInfoDbContext;
            _signInManager = signInManager;
            _userManager = userManager;
        }


        public void Sync()
        {
            SyncPart(1);
        }

        private void SyncPart(int part)
        {
            var users = GetUsersPart(part).ToArray();

            if (!users.Any())
            {
                return;
            }

            _appDbContext.SaveChanges();

            var syncInfo = users.Select(n => new UserSyncEntity()
            {
                SourceId = n.SourceId,
                DestinationId = n.DestinationEntity.Id
            });

            _syncInfoDbContext.UserSync.AddRange(syncInfo);

            _syncInfoDbContext.SaveChanges();

            SyncPart(++part);
        }

        private IEnumerable<SyncEntity<UserEntity>> GetUsersPart(int part)
        {
            var users = _sourceDbContext.Users
                .Skip((part - 1) * PartSize)
                .Take(part * PartSize)
                .OrderBy(n => n.Id)
                .ToArray();

            foreach (var user in users)
            {
                if (_syncInfoDbContext.UserSync.Any(n => n.SourceId == user.Id))
                {
                    Log.Debug("Taki użytkownik znajduje się już w docelowym systemie. Nazwa: {Name} Id: {Id}", user.Login, user.Id);

                    continue;
                }
//
//                if (user.GroupId > 0)
//                {
//                    Log.Debug("Nie przenoszę kont administracji {0}...", user.Login);
//
//                    continue;
//                }

                Log.Information("Dodaję użytkownika do systemu docelowego: Nazwa: {Name} Id: {Id}", user.Login, user.Id);

                var client = _syncInfoDbContext.ClientSync.FirstOrDefault(n => n.SourceId == user.ClientId);

                var entity = new UserEntity()
                {
                    UserName = user.Login,
                    Email = user.Email,
                    OldPassword = user.Password,
                    ClientId = client?.DestinationId,
                    EmailConfirmed = true
                };

                var result = _userManager.CreateAsync(entity).Result;

                yield return new SyncEntity<UserEntity>()
                {
                    SourceId = user.Id,
                    DestinationEntity = entity
                };
            }
        }
    }
}