﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using MultinetPanel.DataSyncApp.Definitions;
using MultinetPanel.DataSyncApp.SourceSystemDb;
using MultinetPanel.DataSyncApp.SourceSystemDb.Entities;
using MultinetPanel.DataSyncApp.SyncInfoDb.SyncEntities;
using MultinetPanel.Repository;
using MultinetPanel.Repository.Invoices.Entities;
using MultinetPanel.Repository.VatRates.Entities;
using Serilog;

namespace MultinetPanel.DataSyncApp.SyncInfoDb.SyncHandlers
{
    public class InvoiceSyncHandler : IEntitySync
    {
        public int PartSize => 1000;
        public string Name => "Faktura";

        private readonly SourceSystemDbContext _sourceDbContext;
        private readonly AppDbContext _appDbContext;
        private readonly SyncInfoDbContext _syncInfoDbContext;
        private IEnumerable<VatRateEntity> _vatRates;

        public InvoiceSyncHandler(SourceSystemDbContext sourceDbContext, AppDbContext appDbContext, SyncInfoDbContext syncInfoDbContext)
        {
            _sourceDbContext = sourceDbContext;
            _appDbContext = appDbContext;
            _syncInfoDbContext = syncInfoDbContext;
        }

        public void Sync()
        {
            GetVatRates();
            SyncPart(1);
        }

        private void GetVatRates()
        {
            _vatRates = _appDbContext.VatRates.ToArray();
        }

        private void SyncPart(int part)
        {
            var invoices = GetInvoicesPart(part).ToArray();

            if (!invoices.Any())
            {
                return;
            }

            _appDbContext.SaveChanges();

            var syncInfo = invoices.Select(n => new InvoiceSyncEntity()
            {
                SourceId = n.SourceId,
                DestinationId = n.DestinationEntity.Id
            });

            _syncInfoDbContext.InvoiceSync.AddRange(syncInfo);

            _syncInfoDbContext.SaveChanges();

            SyncPart(++part);
        }

        private IEnumerable<SyncEntity<InvoiceEntity>> GetInvoicesPart(int part)
        {
            var invoices = _sourceDbContext
                .Invoices
                .Include(n => n.InvoiceProducts)
                .Include(n => n.InvoiceParticipants)
                .Skip((part - 1) * PartSize)
                .Take(part * PartSize)
                .OrderBy(n => n.Id)
                .ToArray();

            foreach (var invoice in invoices)
            {
                if (_syncInfoDbContext.InvoiceSync.Any(n => n.SourceId == invoice.Id))
                {
                    Log.Debug("Taka faktura znajduje się już w docelowym systemie. Numer: {InvoiceNumber} Id: {Id}", invoice.Number, invoice.Id);

                    continue;
                }

                Log.Information("Dodaję fakturę do systemu docelowego. Numer {InvoiceNumber} Id: {Id}", invoice.Number, invoice.Id);

                var issuer = invoice.InvoiceParticipants.FirstOrDefault(n => n.Options == 1);
                var purchaser = invoice.InvoiceParticipants.FirstOrDefault(n => n.Options == 2);
                var client = _syncInfoDbContext.ClientSync.FirstOrDefault(n => n.SourceId == invoice.ClientId);
                var index = 1;

                if (issuer == null || purchaser == null)
                {
                    continue;
                }

                var entity = new InvoiceEntity()
                {
                    InvoiceDate = invoice.Created ?? DateTime.MinValue,
                    Number = invoice.Number,
                    SaleDate = invoice.Selled ?? DateTime.MinValue,
                    PaymentDeadlineDate = invoice.PaymentDate ?? DateTime.MinValue,
                    InvoiceItems = invoice.InvoiceProducts.Select(n => new InvoiceItemEntity()
                    {
                        Index = index++,
                        Name = n.Name,
                        Gross = n.SumGross,
                        Net = n.SumNet,
                        Vat = n.SumGross - n.SumNet,
                        Quantity = Convert.ToDouble(n.Amount),
                        NetUnitPrice = n.PriceNet,
                        GrossUnitPrice = n.PriceGross,
                        VatRateId = _vatRates.FirstOrDefault(vatRate => vatRate.Value == Convert.ToDouble(n.Tax))?.Id ?? 0,
                        MeasureUnitId = n.Unit,
                        Classification = n.Pkwiu,
                    }).ToList(),
                    Issuer = MapToPerson(issuer),
                    Purchaser = MapToPerson(purchaser),
                    ClientId = client.DestinationId
                };

                _appDbContext.Invoices.Add(entity);

                yield return new SyncEntity<InvoiceEntity>()
                {
                    SourceId = invoice.Id,
                    DestinationEntity = entity
                };
            }
        }

        private PersonEntity MapToPerson(InvoiceParticipant participant)
        {
            if (participant == null)
            {
                return null;
            }

            return new PersonEntity()
            {
                Name = participant.Name,
                Secondname = participant.Secondname,
                Surname = participant.Surname,
                CompanyName = participant.CompanyName,
                Pesel = participant.Pesel.ToString(),
                Nip = participant.Nip.ToString(),
                Regon = participant.Regon,
                Krs = participant.Krs,
                Province = participant.Province,
                District = participant.District,
                Community = participant.Community,
                City = participant.City,
                PostCity = participant.PostCity,
                PostCode = participant.PostCode,
                Street = (participant.StreetType + " " + participant.Street).Trim(),
                House = participant.House,
                Flat = participant.Flat,
                Email = participant.Email,
                Phone = participant.Phone.ToString(),
                ServicePhone = participant.ServicePhone.ToString(),
                Fax = participant.Fax.ToString(),
                Type = string.IsNullOrWhiteSpace(participant.CompanyName) ? 1 : 2
            };
        }
    }
}