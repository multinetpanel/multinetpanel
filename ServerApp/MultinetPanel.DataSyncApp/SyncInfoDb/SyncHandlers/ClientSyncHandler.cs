﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using MultinetPanel.DataSyncApp.Definitions;
using MultinetPanel.DataSyncApp.SourceSystemDb;
using MultinetPanel.DataSyncApp.SourceSystemDb.Entities;
using MultinetPanel.DataSyncApp.SyncInfoDb.SyncEntities;
using MultinetPanel.Repository;
using MultinetPanel.Repository.Clients.Entities;
using MultinetPanel.Repository.Invoices.Entities;
using Serilog;

namespace MultinetPanel.DataSyncApp.SyncInfoDb.SyncHandlers
{
    public class ClientSyncHandler : IEntitySync
    {
        public int PartSize => 1000;
        public string Name => "Klient";

        private readonly SourceSystemDbContext _sourceDbContext;
        private readonly AppDbContext _appDbContext;
        private readonly SyncInfoDbContext _syncInfoDbContext;

        public ClientSyncHandler(SourceSystemDbContext sourceDbContext, AppDbContext appDbContext,
            SyncInfoDbContext syncInfoDbContext)
        {
            _sourceDbContext = sourceDbContext;
            _appDbContext = appDbContext;
            _syncInfoDbContext = syncInfoDbContext;
        }


        public void Sync()
        {
            SyncPart(1);
        }

        private void SyncPart(int part)
        {
            var clients = GetClientsPart(part).ToArray();

            if (!clients.Any())
            {
                return;
            }

            _appDbContext.SaveChanges();

            var syncInfo = clients.Select(n => new ClientSyncEntity()
            {
                SourceId = n.SourceId,
                DestinationId = n.DestinationEntity.Id
            });

            _syncInfoDbContext.ClientSync.AddRange(syncInfo);

            _syncInfoDbContext.SaveChanges();

            SyncPart(++part);
        }

        private IEnumerable<SyncEntity<ClientEntity>> GetClientsPart(int part)
        {
            var clients = _sourceDbContext.Clients
                .Skip((part - 1) * PartSize)
                .Take(part * PartSize)
                .OrderBy(n => n.Id)
                .ToArray();

            foreach (var client in clients)
            {
                if (_syncInfoDbContext.ClientSync.Any(n => n.SourceId == client.Id))
                {
                    Log.Debug("Taki klient znajduje się już w docelowym systemie. Nazwa: {Name} Id: {Id}", client.Name,
                        client.Id);

                    continue;
                }

                Log.Information("Dodaję klienta do systemu docelowego: Nazwa: {Name} Id: {Id}", client.Name, client.Id);

                var entity = new ClientEntity()
                {
                    Person = new PersonEntity()
                    {
                        Email = client.Email,
                        Birthday = client.Birthday ?? DateTime.MinValue,
                        Birthplace = client.Birthplace,
                        City = client.City,
                        Community = client.Community,
                        CompanyName = client.CompanyName,
                        CompanyShortName = client.CompanyShortName,
                        District = client.District,
                        Fax = client.Fax.ToString(),
                        Flat = client.Flat,
                        House = client.House,
                        IdentityCard = client.IdentityCard,
                        MobilePhone = client.MobilePhone.ToString(),
                        Name = client.Name,
                        Nip = client.Nip.ToString(),
                        Pesel = client.Pesel.ToString(),
                        Phone = client.Phone.ToString(),
                        PostCity = client.PostCity,
                        PostCode = client.PostCode,
                        Province = client.Province,
                        Regon = client.Regon,
                        Secondname = client.Secondname,
                        ServicePhone = client.ServicePhone.ToString(),
                        Sex = client.Sex,
                        Street = !string.IsNullOrWhiteSpace(client.StreetType)
                            ? client.StreetType + " " + client.Street
                            : client.Street,
                        Surname = client.Surname,
                        Type = client.Type
                    }
                };

                _appDbContext.Clients.Add(entity);

                yield return new SyncEntity<ClientEntity>()
                {
                    DestinationEntity = entity,
                    SourceId = client.Id
                };
            }
        }
    }
}