﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using MultinetPanel.DataSyncApp.SyncInfoDb.SyncEntities;

namespace MultinetPanel.DataSyncApp.SyncInfoDb
{
    public class SyncInfoDbContext : DbContext
    {
        public DbSet<ClientSyncEntity> ClientSync { get; set; }
        public DbSet<InvoiceSyncEntity> InvoiceSync { get; set; }
        public DbSet<TransferSyncEntity> TransferSync { get; set; }
        public DbSet<UserSyncEntity> UserSync { get; set; }

        public SyncInfoDbContext(DbContextOptions<SyncInfoDbContext> options) : base(options)
        {
            
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("multinet_sync");
        }
    }
}
