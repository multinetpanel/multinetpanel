﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MultinetPanel.DataSyncApp.SyncInfoDb.SyncEntities
{
    [Table("InvoiceSync")]
    public class InvoiceSyncEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int SourceId { get; set; }
        public int DestinationId { get; set; }
    }
}