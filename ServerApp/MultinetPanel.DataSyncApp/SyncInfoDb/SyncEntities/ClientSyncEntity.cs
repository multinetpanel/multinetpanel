﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MultinetPanel.DataSyncApp.Definitions;

namespace MultinetPanel.DataSyncApp.SyncInfoDb.SyncEntities
{
    [Table("ClientSync")]
    public class ClientSyncEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int SourceId { get; set; }
        public int DestinationId { get; set; }
    }
}
