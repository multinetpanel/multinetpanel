﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MultinetPanel.DataSyncApp.SyncInfoDb.SyncEntities
{
    [Table("TransferSync")]
    public class TransferSyncEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int SourceId { get; set; }
        public int DestinationId { get; set; }
    }
}