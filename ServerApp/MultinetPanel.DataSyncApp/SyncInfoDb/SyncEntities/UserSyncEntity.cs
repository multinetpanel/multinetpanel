﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MultinetPanel.DataSyncApp.SyncInfoDb.SyncEntities
{
    [Table("UserSync")]
    public class UserSyncEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int SourceId { get; set; }
        public string DestinationId { get; set; }
    }
}