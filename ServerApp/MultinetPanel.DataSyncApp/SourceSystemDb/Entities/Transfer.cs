﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MultinetPanel.DataSyncApp.SourceSystemDb.Entities
{
    [Table("transfers")]
    public class Transfer
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("Import")]
        public int ImportId { get; set; }

        [ForeignKey("Provider")]
        public int ProviderId { get; set; }

        [ForeignKey("Client")]
        public int ClientId { get; set; }

        public int Session { get; set; }
        public int Transaction { get; set; }
        public string AccountBase { get; set; }
        public string Account { get; set; }
        public string Type { get; set; }
        public decimal Price { get; set; }
        public string Currency { get; set; }
        public string BankAccount { get; set; }
        public string Sender { get; set; }
        public string Receiver { get; set; }
        public string Title { get; set; }
        public DateTime? Date { get; set; }
        public int Options { get; set; }
        public DateTime? Created { get; set; }

        public TransferImport Import { get; set; }
        public Provider Provider { get; set; }
        public Client Client { get; set; }

        public ICollection<Invoice> Invoices { get; set; }
        public ICollection<Correction> Corrections { get; set; }
        public ICollection<Note> Notes { get; set; }
    }
}
