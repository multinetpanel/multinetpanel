﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MultinetPanel.DataSyncApp.SourceSystemDb.Entities
{
    [Table("providers")]
    public class Provider
    {
        [Key]
        public int Id { get; set; }
        public Int64? Nip { get; set; }
        public string Regon { get; set; }
        public string Rpt { get; set; }
        public string Krs { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string CompanyName { get; set; }
        public string Province { get; set; }
        public string District { get; set; }
        public string Community { get; set; }
        public string City { get; set; }
        public string PostCity { get; set; }
        public string PostCode { get; set; }
        public string StreetType { get; set; }
        public string Street { get; set; }
        public string House { get; set; }
        public string Flat { get; set; }
        public string Website { get; set; }
        public string Email { get; set; }
        public string ServiceEmail { get; set; }
        public string MarketingEmail { get; set; }
        public string PaymentEmail { get; set; }
        public string AccountantEmail { get; set; }
        public string VindicationEmail { get; set; }
        public Int64? Phone { get; set; }
        public Int64? MobilePhone { get; set; }
        public Int64? ServicePhone { get; set; }
        public Int64? PaymentPhone { get; set; }
        public Int64? Fax { get; set; }
        public string InvoiceSerie { get; set; }
        public string CorrectionSerie { get; set; }
        public string NoteSerie { get; set; }
        public string AccountBase { get; set; }
        public string AccountClientPrefix { get; set; }
        public decimal DemandPriceNet { get; set; }
        public string DemandProductName { get; set; }
        public string PenaltyProductName { get; set; }
        public string Note { get; set; }
        public int Options { get; set; }
        public DateTime? Created { get; set; }

        public ICollection<Invoice> Invoices { get; set; }
        public ICollection<Grant> Grants { get; set; }
        public ICollection<Correction> Corrections { get; set; }
        public ICollection<Contract> Contracts { get; set; }
        public ICollection<Node> Nodes { get; set; }
        public ICollection<Note> Notes { get; set; }
        public ICollection<Pricing> Pricings { get; set; }
        public ICollection<Transfer> Transfers { get; set; }
    }
}
