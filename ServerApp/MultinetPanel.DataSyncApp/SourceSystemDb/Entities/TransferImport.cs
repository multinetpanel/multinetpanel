﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MultinetPanel.DataSyncApp.SourceSystemDb.Entities
{
    [Table("transfer_imports")]
    public class TransferImport
    {
        [Key]
        public int Id { get; set; }
        public int FileType { get; set; }
        public string FileMime { get; set; }
        public string File { get; set; }
        public DateTime? Imported { get; set; }
        public DateTime? Created { get; set; }

        public ICollection<Transfer> Transfers { get; set; }
    }
}
