﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MultinetPanel.DataSyncApp.SourceSystemDb.Entities
{
    [Table("grants")]
    public class Grant
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("Provider")]
        public int ProviderId { get; set; }

        public int Status { get; set; }
        public string Number { get; set; }
        public string Designation { get; set; }
        public string Description { get; set; }
        public string Created { get; set; }

        public Provider Provider { get; set; }

        public ICollection<Invoice> Invoices { get; set; }
        public ICollection<Correction> Corrections { get; set; }
        public ICollection<Contract> Contracts { get; set; }
        public ICollection<Note> Notes { get; set; }
        public ICollection<Pricing> Pricings { get; set; }
    }
}
