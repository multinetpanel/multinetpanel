﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MultinetPanel.DataSyncApp.SourceSystemDb.Entities
{
    [Table("terms")]
    public class Term
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Options { get; set; }
        public DateTime? Created { get; set; }

        public ICollection<Pricing> Pricings { get; set; }
    }
}
