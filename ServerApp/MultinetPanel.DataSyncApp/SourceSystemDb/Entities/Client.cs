﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MultinetPanel.DataSyncApp.SourceSystemDb.Entities
{
    [Table("clients")]
    public class Client
    {
        [Key]
        public int Id { get; set; }
        public int Number { get; set; }
        public int Type { get; set; }
        public Int64? Pesel { get; set; }
        public Int64? Nip { get; set; }
        public string Regon { get; set; }
        public string IdentityCard { get; set; }
        public string Name { get; set; }
        public string Secondname { get; set; }
        public string Surname { get; set; }
        public string CompanyName { get; set; }
        public string CompanyShortName { get; set; }
        public DateTime? Birthday { get; set; }
        public string Birthplace { get; set; }
        public int Sex { get; set; }
        public string Province { get; set; }
        public string District { get; set; }
        public string Community { get; set; }
        public string City { get; set; }
        public string PostCity { get; set; }
        public string PostCode { get; set; }
        public string StreetType { get; set; }
        public string Street { get; set; }
        public string House { get; set; }
        public string Flat { get; set; }
        public string Email { get; set; }
        public Int64? Phone { get; set; }
        public Int64? MobilePhone { get; set; }
        public Int64? ServicePhone { get; set; }
        public Int64? Fax { get; set; }
        public string Note { get; set; }
        public int Options { get; set; }
        public DateTime? Created { get; set; }

        public ICollection<Invoice> Invoices { get; set; }
        public ICollection<Correction> Corrections { get; set; }
        public ICollection<Contract> Contracts { get; set; }
        public ICollection<Note> Notes { get; set; }
        public ICollection<Transfer> Transfers { get; set; }
    }
}
