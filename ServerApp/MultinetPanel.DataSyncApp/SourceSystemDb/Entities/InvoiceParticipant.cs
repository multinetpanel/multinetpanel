﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MultinetPanel.DataSyncApp.SourceSystemDb.Entities
{
    [Table("invoice_participants")]
    public class InvoiceParticipant
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("Invoice")]
        public int InvoiceId { get; set; }

        public string Name { get; set; }
        public string Secondname { get; set; }
        public string Surname { get; set; }
        public string CompanyName { get; set; }

        public Int64? Pesel { get; set; }
        public Int64? Nip { get; set; }
        public string Regon { get; set; }
        public string Krs { get; set; }
        public string Province { get; set; }
        public string District { get; set; }
        public string Community { get; set; }
        public string City { get; set; }
        public string PostCity { get; set; }
        public string PostCode { get; set; }
        public string StreetType { get; set; }
        public string Street { get; set; }
        public string House { get; set; }
        public string Flat { get; set; }
        public string Website { get; set; }
        public string Email { get; set; }
        public string ServiceEmail { get; set; }
        public string MarketingEmail { get; set; }
        public string PaymentEmail { get; set; }
        public Int64? Phone { get; set; }
        public Int64? MobilePhone { get; set; }
        public Int64? ServicePhone { get; set; }
        public Int64? PaymentPhone { get; set; }
        public Int64? Fax { get; set; }
        public Int64 Options { get; set; }
        public DateTime? Created { get; set; }

        public Invoice Invoice { get; set; }
    }
}
