﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MultinetPanel.DataSyncApp.SourceSystemDb.Entities
{
    [Table("pricings")]
    public class Pricing
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("Provider")]
        public int ProviderId { get; set; }

        [ForeignKey("Grant")]
        public int GrantId { get; set; }

        [ForeignKey("Term")]
        public int TermId { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public string PromotionTerm { get; set; }
        public string ElectronicTerm { get; set; }
        public string Information { get; set; }
        public int Amount { get; set; }
        public int Options { get; set; }
        public DateTime? Created { get; set; }

        public Provider Provider { get; set; }
        public Grant Grant { get; set; }
        public Term Term { get; set; }

        public ICollection<Contract> Contracts { get; set; }
    }
}
