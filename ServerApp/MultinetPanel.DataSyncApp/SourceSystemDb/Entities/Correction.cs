﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MultinetPanel.DataSyncApp.SourceSystemDb.Entities
{
    [Table("corrections")]
    public class Correction
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("Provider")]
        public int ProviderId { get; set; }

        [ForeignKey("Client")]
        public int ClientId { get; set; }

        [ForeignKey("GrantUd")]
        public int GrantId { get; set; }

        [ForeignKey("Transfer")]
        public int TransferId { get; set; }

        public string Number { get; set; }
        public decimal SumNet { get; set; }
        public decimal SumGross { get; set; }
        public string Currency { get; set; }
        public string City { get; set; }
        public DateTime? Inserted { get; set; }
        public DateTime? Selled { get; set; }
        public int Payment { get; set; }
        public int PaymentDay { get; set; }
        public DateTime? PaymentDate { get; set; }
        public DateTime? Paid { get; set; }
        public string BankAccount { get; set; }
        public int Options { get; set; }
        public DateTime? Created { get; set; }

        public Provider Provider { get; set; }
        public Client Client { get; set; }
        public Grant Grant { get; set; }
        public Transfer Transfer { get; set; }

        public ICollection<Invoice> Invoices { get; set; }
    }
}
