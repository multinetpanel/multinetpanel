﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MultinetPanel.DataSyncApp.SourceSystemDb.Entities
{
    [Table("nodes")]
    public class Node
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("Provider")]
        public int ProviderId { get; set; }

        public int Type { get; set; }
        public int Object { get; set; }
        public string Designation { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Province { get; set; }
        public string District { get; set; }
        public string Community { get; set; }
        public string City { get; set; }
        public string PostCode { get; set; }
        public string StreetType { get; set; }
        public string Street { get; set; }
        public string House { get; set; }
        public string Flat { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
        public int Range { get; set; }
        public int Options { get; set; }
        public DateTime? Created { get; set; }

        public Provider Provider { get; set; }

        public ICollection<Contract> Contracts { get; set; }
    }
}
