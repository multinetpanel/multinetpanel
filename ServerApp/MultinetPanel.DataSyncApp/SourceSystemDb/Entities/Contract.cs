﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MultinetPanel.DataSyncApp.SourceSystemDb.Entities
{
    [Table("contracts")]
    public class Contract
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("Provider")]
        public int ProviderId { get; set; }

        [ForeignKey("Client")]
        public int ClientId { get; set; }
        
        public int ReferrerId { get; set; }

        [ForeignKey("Pricing")]
        public int PricingId { get; set; }

        [ForeignKey("Node")]
        public int NodeId { get; set; }

        [ForeignKey("Grant")]
        public int GrantId { get; set; }

        public int Identifier { get; set; }
        public int Type { get; set; }
        public int Template { get; set; }
        public string Number { get; set; }
        public string ConcludedCity { get; set; }
        public DateTime? Concluded { get; set; }
        public DateTime? BeginDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Province { get; set; }
        public string District { get; set; }
        public string Community { get; set; }
        public string City { get; set; }
        public string PostCity { get; set; }
        public string PostCode { get; set; }
        public string StreetType { get; set; }
        public string Street { get; set; }
        public string House { get; set; }
        public string Flat { get; set; }
        public string BankAccount { get; set; }
        public string Donation { get; set; }
        public string Mac { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
        public string Currency { get; set; }
        public int Internship { get; set; }
        public int TestType { get; set; }
        public int TestDay { get; set; }
        public DateTime? TestBegin { get; set; }
        public DateTime ?TestEnd { get; set; }
        public int SuspensionType { get; set; }
        public int SuspensionMonth { get; set; }
        public DateTime? SuspensionBegin { get; set; }
        public DateTime? SuspensionEnd { get; set; }
        public DateTime? AnnexBegin { get; set; }
        public DateTime? AnnexEnd { get; set; }
        public int FileType { get; set; }
        public string FileMime { get; set; }
        public string File { get; set; }
        public string Note { get; set; }
        public int Options { get; set; }
        public DateTime? Created { get; set; }

        public Provider Provider { get; set; }
        public Client Client { get; set; }
        public Pricing Pricing { get; set; }
        public Node Node { get; set; }
        public Grant Grant { get; set; }

        public ICollection<Invoice> Invoices { get; set; }
    }
}
