﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MultinetPanel.DataSyncApp.SourceSystemDb.Entities
{
    [Table("invoice_products")]
    public class InvoiceProduct
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("Invoice")]
        public int InvoiceId { get; set; }

        public string Name { get; set; }
        public string Pkwiu { get; set; }
        public int Unit { get; set; }
        public decimal Amount { get; set; }
        public decimal Tax { get; set; }
        public decimal PriceNet { get; set; }
        public decimal PriceGross { get; set; }
        public decimal SumNet { get; set; }
        public decimal SumGross { get; set; }
        public string Currency { get; set; }
        public DateTime Created { get; set; }

        public Invoice Invoice { get; set; }
    }
}
