﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using McMaster.Extensions.CommandLineUtils;
using Microsoft.Extensions.DependencyInjection;
using MultinetPanel.DataSyncApp.Definitions;
using Serilog;
using Serilog.Events;

namespace MultinetPanel.DataSyncApp
{
    public class Program
    {
        public static int Main(string[] args) => CommandLineApplication.Execute<Program>(args);

        [Required, Option(ShortName = "s", LongName = "source", Description = "Source ConnectionString (mysql)")]
        public string SourceConnectionString { get; set; }

        [Required, Option(ShortName = "d", LongName = "destination", Description = "Destination ConnectionString (postgresql)")]
        public string DestinationConnectionString { get; set; }

        public void OnExecute()
        {
            CreateLogger();

            var provider = ProgramDependencyResolver.CreateDependency(new SyncConfiguration()
            {
                SourceConnectionString = SourceConnectionString,
                DestinationConnectionString = DestinationConnectionString
            });

            provider.GetService<ISourceSyncHandler>().Sync();
        }

        private void CreateLogger()
        {
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Information()
                .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
                .WriteTo.Console()
                .WriteTo.File("logs\\log.txt", rollingInterval: RollingInterval.Day)
                .CreateLogger();
        }
    }
}
