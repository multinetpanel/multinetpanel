﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using MultinetPanel.DataSyncApp.SyncInfoDb;

namespace MultinetPanel.DataSyncApp
{
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<SyncInfoDbContext>
    {
        public SyncInfoDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<SyncInfoDbContext>();

            builder.UseNpgsql("Server=localhost;Database=MultinetPanel;User ID=postgres;Password=1;");

            return new SyncInfoDbContext(builder.Options);
        }
    }
}
