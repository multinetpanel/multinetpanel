﻿using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using CsvHelper;
using MultinetPanel.Core.Boundary.Serializers;

namespace MultinetPanel.Core.Serializers
{
    public class CsvSerializer : ICsvSerializer
    {
        public IEnumerable<TDestination> Deserialize<TDestination>(string csv)
        {
            using (var stringReader = new StringReader(csv))
            using (var csvReader = new CsvReader(stringReader) { Configuration = { HasHeaderRecord = false, CultureInfo = CultureInfo.InvariantCulture }})
            {
                return csvReader.GetRecords<TDestination>().ToArray();
            }
        }
    }
}
