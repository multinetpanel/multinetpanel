﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using MultinetPanel.Core.Boundary.Handlers;

namespace MultinetPanel.Core.Handlers
{
    public class CryptoHandler : ICryptoHandler
    {
        public string Md5Hash(string input)
        {
            return ComputeHash(input, new MD5CryptoServiceProvider());
        }

        public string Sha256Hash(string input)
        {
            StringBuilder Sb = new StringBuilder();

            using (var hash = SHA256.Create())
            {
                Encoding enc = Encoding.UTF8;
                Byte[] result = hash.ComputeHash(enc.GetBytes(input));

                foreach (Byte b in result)
                    Sb.Append(b.ToString("x2"));
            }

            return Sb.ToString();
        }

        private string ComputeHash(string input, HashAlgorithm algorithm)
        {
            var inputBytes = Encoding.UTF8.GetBytes(input);

            var hashedBytes = algorithm.ComputeHash(inputBytes);

            return BitConverter.ToString(hashedBytes);
        }
    }
}
