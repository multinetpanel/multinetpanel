﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using IObjectMapper = MultinetPanel.Core.Boundary.Handlers.IObjectMapper;

namespace MultinetPanel.Core.Handlers
{
    public class ObjectMapper : IObjectMapper
    {
        private readonly IMapper _mapper;

        public ObjectMapper(IMapper mapper)
        {
            _mapper = mapper;
        }

        public TDestination Map<TDestination>(object source)
        {
            return _mapper.Map<TDestination>(source);
        }
    }
}
