﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using MultinetPanel.Core.AppFlow;
using MultinetPanel.Core.Boundary.AppFlow;
using MultinetPanel.Core.Boundary.EmailHandlers;
using MultinetPanel.Core.Boundary.Handlers;
using MultinetPanel.Core.Boundary.Serializers;
using MultinetPanel.Core.EmailHandlers;
using MultinetPanel.Core.Handlers;
using MultinetPanel.Core.Serializers;
using IObjectMapper = MultinetPanel.Core.Boundary.Handlers.IObjectMapper;

namespace MultinetPanel.Core
{
    public static class CoreDependencyResolver
    {
        public static void AddCore(this IServiceCollection serviceCollection, IEnumerable<Assembly> assemblies)
        {
            serviceCollection.AddTransient<IFlowExecutor, FlowExecutor>();
            serviceCollection.AddTransient<IEmailHandler, EmailHandler>();
            serviceCollection.AddTransient<ICsvSerializer, CsvSerializer>();
            serviceCollection.AddTransient<IObjectMapper, ObjectMapper>();
            serviceCollection.AddTransient<ICryptoHandler, CryptoHandler>();

            serviceCollection.AddAutoMapper(assemblies);
        }
    }
}
