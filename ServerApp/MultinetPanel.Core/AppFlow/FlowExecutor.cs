﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using MediatR;
using MultinetPanel.Core.Boundary.AppFlow;

namespace MultinetPanel.Core.AppFlow
{
    public class FlowExecutor : IFlowExecutor
    {
        private readonly IMediator _mediator;

        public FlowExecutor(IMediator mediator)
        {
            _mediator = mediator;
        }

        public Task Send(IRequest request)
        {
            return _mediator.Send(request);
        }

        public Task<TResponse> Send<TResponse>(IRequest<TResponse> request)
        {
            return _mediator.Send(request);
        }
    }
}
