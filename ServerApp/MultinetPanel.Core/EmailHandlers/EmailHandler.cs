﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using MailKit.Net.Pop3;
using MailKit.Security;
using MimeKit;
using MultinetPanel.Core.Boundary.EmailHandlers;
using MultinetPanel.Core.Boundary.EmailHandlers.Models;

namespace MultinetPanel.Core.EmailHandlers
{
    public class EmailHandler : IEmailHandler
    {
        public IEnumerable<EmailMessage> GetMessages(EmailServerCredentials credentials, DateTime fromDate)
        {
            return GetMessages(credentials, n => n.Date >= fromDate);
        }

        public IEnumerable<EmailMessage> GetMessages(EmailServerCredentials credentials, Func<EmailMessage, bool> condition)
        {
            using (var client = new Pop3Client())
            {
                client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                client.Connect(credentials.Host, credentials.Port);

                client.Authenticate(credentials.Username, credentials.Password);

                for (var i = client.Count - 1; i >= 0; i--)
                {
                    var message = client.GetMessage(i);
                    
                    var emailMessage = new EmailMessage()
                    {
                        Subject = message.Subject,
                        Content = message.TextBody,
                        Date = message.Date.DateTime,
                        Attachments = message.Attachments.Select(attachment => new EmailMessageAttachment()
                        {
                            Name = attachment.ContentDisposition?.FileName ?? attachment.ContentType.Name,
                            Attachment = GetAttachment(attachment)
                        })
                    };

                    if (!condition(emailMessage))
                    {
                        break;                        
                    }

                    yield return emailMessage;
                }

                client.Disconnect(true);
            }
        }

        private byte[] GetAttachment(MimeEntity attachment)
        {
            using (var stream = new MemoryStream())
            {
                if (attachment is MessagePart rfc822)
                {
                    rfc822.Message.WriteTo(stream);

                    return stream.ToArray();
                }

                (attachment as MimePart)?.Content.DecodeTo(stream);

                return stream.ToArray();
            }
        }
    }
}
