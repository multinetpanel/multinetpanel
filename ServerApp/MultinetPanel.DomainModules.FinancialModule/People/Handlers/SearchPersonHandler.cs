﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using MultinetPanel.DomainModules.FinancialModule.Boundary.People.Models;
using MultinetPanel.DomainModules.FinancialModule.Boundary.People.Requests;
using MultinetPanel.Repository.Boundary;

namespace MultinetPanel.DomainModules.FinancialModule.People.Handlers
{
    public class SearchPersonHandler : IRequestHandler<SearchPersonRequest, IEnumerable<Person>>
    {
        private readonly IUnitOfWork _unitOfWork;

        public SearchPersonHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Task<IEnumerable<Person>> Handle(SearchPersonRequest request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_unitOfWork.PersonRepository.SearchPerson(request));
        }
    }
}
