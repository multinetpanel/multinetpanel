﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Invoices.Requests;
using MultinetPanel.Repository.Boundary;

namespace MultinetPanel.DomainModules.FinancialModule.Invoices.Handlers
{
    public class GetInvoicesCountHandler : IRequestHandler<GetInvoicesCountRequest, int>
    {
        private readonly IUnitOfWork _unitOfWork;

        public GetInvoicesCountHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Task<int> Handle(GetInvoicesCountRequest request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_unitOfWork.InvoiceRepository.GetInvoicesCount(request));
        }
    }
}