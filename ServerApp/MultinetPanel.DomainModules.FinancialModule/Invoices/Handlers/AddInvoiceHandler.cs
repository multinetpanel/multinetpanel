﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Invoices.Requests;

namespace MultinetPanel.DomainModules.FinancialModule.Invoices.Handlers
{
    public class AddInvoiceHandler : IRequestHandler<AddInvoiceRequest>
    {
        public AddInvoiceHandler()
        {
            
        }

        public Task<Unit> Handle(AddInvoiceRequest request, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }
    }
}