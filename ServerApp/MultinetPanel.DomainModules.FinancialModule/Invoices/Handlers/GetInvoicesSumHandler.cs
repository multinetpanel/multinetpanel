﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Invoices.Models;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Invoices.Requests;
using MultinetPanel.Repository.Boundary;

namespace MultinetPanel.DomainModules.FinancialModule.Invoices.Handlers
{
    public class GetInvoicesSumHandler : IRequestHandler<GetInvoicesSumRequest, InvoicesSum>
    {
        private readonly IUnitOfWork _unitOfWork;

        public GetInvoicesSumHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Task<InvoicesSum> Handle(GetInvoicesSumRequest request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_unitOfWork.InvoiceRepository.GetInvoicesSum(request));
        }
    }
}