﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Invoices.Models;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Invoices.Requests;
using MultinetPanel.Repository.Boundary;

namespace MultinetPanel.DomainModules.FinancialModule.Invoices.Handlers
{
    public class GetInvoiceHandler : IRequestHandler<GetInvoiceRequest, Invoice>
    {
        private readonly IUnitOfWork _unitOfWork;

        public GetInvoiceHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Task<Invoice> Handle(GetInvoiceRequest request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_unitOfWork.InvoiceRepository.GetInvoice(request.Id));
        }
    }
}