﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Invoices.Models;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Invoices.Requests;
using MultinetPanel.Repository.Boundary;

namespace MultinetPanel.DomainModules.FinancialModule.Invoices.Handlers
{
    public class GetInvoicesHandler : IRequestHandler<GetInvoicesRequest, IEnumerable<Invoice>>
    {
        private readonly IUnitOfWork _unitOfWork;

        public GetInvoicesHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Task<IEnumerable<Invoice>> Handle(GetInvoicesRequest request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_unitOfWork.InvoiceRepository.GetInvoices(request));
        }
    }
}
