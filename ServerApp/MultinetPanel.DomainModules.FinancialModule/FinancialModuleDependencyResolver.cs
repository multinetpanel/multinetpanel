﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Configurations;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Importers;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Indexers;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Parsers;
using MultinetPanel.DomainModules.FinancialModule.Transfers.Importers;
using MultinetPanel.DomainModules.FinancialModule.Transfers.Indexers;
using MultinetPanel.DomainModules.FinancialModule.Transfers.Indexers.Attempts;
using MultinetPanel.DomainModules.FinancialModule.Transfers.Parsers;

namespace MultinetPanel.DomainModules.FinancialModule
{
    public static class FinancialModuleDependencyResolver
    {
        public static void AddFinancialModule(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<ITransfersImporter, TransfersImporter>();
            services.AddTransient<ITransfersImporterLogger, TransfersImporterLogger>();
            services.AddTransient<ITransferIndexer, TransferIndexer>();
            services.AddTransient<ITransferIndexerLogger, TransferIndexerLogger>();
            services.AddTransient<IndexByBankAccountTransferIndexerAttempt>();
            services.AddTransient<IndexByTitleTransferIndexerAttempt>();
            services.AddTransient<ITransferParser, TransferParser>();

            AddFinancialModuleConfiguration(services, configuration);
        }

        private static void AddFinancialModuleConfiguration(IServiceCollection services, IConfiguration configuration)
        {
            var model = new TransfersEmailConfig();

            configuration.Bind("TransfersEmail", model);
            model.Password = configuration.GetValue<string>("TransfersEmailPassword");

            services.AddSingleton(model);
        }
    }
}
