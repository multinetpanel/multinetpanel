﻿using System;
using System.Collections.Generic;
using System.Text;
using MultinetPanel.Core.Boundary.Handlers;
using MultinetPanel.Core.Boundary.Serializers;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Models;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Parsers;
using MultinetPanel.DomainModules.FinancialModule.Transfers.Parsers.TransferPatterns;

namespace MultinetPanel.DomainModules.FinancialModule.Transfers.Parsers
{
    public class TransferParser : ITransferParser
    {
        private readonly ICsvSerializer _csvSerializer;
        private readonly IObjectMapper _objectMapper;

        public TransferParser(ICsvSerializer csvSerializer, IObjectMapper objectMapper)
        {
            _csvSerializer = csvSerializer;
            _objectMapper = objectMapper;
        }

        public IEnumerable<Transfer> GetTransfers(string transfers)
        {
            var csvTransfers = _csvSerializer.Deserialize<Csv1TransferPattern>(transfers);

            return _objectMapper.Map<IEnumerable<Transfer>>(csvTransfers);
        }

        public IEnumerable<Transfer> GetTransfers(byte[] transfers, Encoding encoding)
        {
            return GetTransfers(encoding.GetString(transfers));
        }
    }
}
