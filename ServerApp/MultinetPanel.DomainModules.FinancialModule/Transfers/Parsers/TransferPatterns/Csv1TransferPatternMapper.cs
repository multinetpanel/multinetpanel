﻿using System;
using System.Collections.Generic;
using System.Security;
using System.Text;
using AutoMapper;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Models;

namespace MultinetPanel.DomainModules.FinancialModule.Transfers.Parsers.TransferPatterns
{
    public class Csv1TransferPatternMapper : Profile
    {
        public Csv1TransferPatternMapper()
        {
            CreateMap<Csv1TransferPattern, Transfer>();
        }
    }
}
