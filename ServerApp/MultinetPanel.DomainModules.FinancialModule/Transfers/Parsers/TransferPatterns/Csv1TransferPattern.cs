﻿using System;
using System.Collections.Generic;
using System.Security;
using System.Text;

namespace MultinetPanel.DomainModules.FinancialModule.Transfers.Parsers.TransferPatterns
{
    public class Csv1TransferPattern
    {
        public string TitleShort { get; set; }
        public decimal Unknown1 { get; set; }
        public decimal Value { get; set; }
        public string Unknown2 { get; set; }
        public decimal Unknown3 { get; set; }
        public string SenderAccountNumber { get; set; }
        public DateTime Date { get; set; }
        public string Title { get; set; }
        public string Unknown4 { get; set; }
        public string Unknown5 { get; set; }
        public string Unknown6 { get; set; }
        public string Unknown7 { get; set; }
        public string Unknown8 { get; set; }
        public decimal BalanceAfterTransfer { get; set; }
        public DateTime Unknown9 { get; set; }
        public string ReceiverAccountNumber { get; set; }
        public string Unknown10 { get; set; }
        public string Unknown11 { get; set; }
        public string SenderName { get; set; }
        public string SenderStreet { get; set; }
        public string SenderCity { get; set; }
        public string Unknown12 { get; set; }
        public string Receiver { get; set; }
    }
}
