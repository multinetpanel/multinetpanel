﻿using System;
using System.Collections.Generic;
using System.Linq;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Clients.Models;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Invoices.Models;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Indexers;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Models;
using MultinetPanel.Repository.Boundary;

namespace MultinetPanel.DomainModules.FinancialModule.Transfers.Indexers.Attempts
{
    public class IndexByBankAccountTransferIndexerAttempt : ITransferIndexerAttempt
    {
        private readonly IUnitOfWork _unitOfWork;
        private Action<string> _callback;

        public IndexByBankAccountTransferIndexerAttempt(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<Invoice> SearchInvoices(Transfer transfer, Action<string> callback)
        {
            _callback = callback;

            _callback("Indeksuję przelew po numerze konta...");

            var transfers = SearchTransfersByBankAccount(transfer.SenderAccountNumber);
            var clients = GetClientsForThisBankAccount(transfers).ToArray();

            if (clients.Length > 1)
            {
                _callback("Znalazłem więcej niż jednego klienta dla tego numeru konta, nie mogę dopasować...");

                return new Invoice[0];
            }

            return GetClientInvoicesToPay(clients.FirstOrDefault(), transfer);
        }

        private IEnumerable<Transfer> SearchTransfersByBankAccount(string senderAccountNumber)
        {
            _callback($"Szukam przelewów z numeru konta o numerze {senderAccountNumber}...");

            return _unitOfWork.TransferRepository.GetTransfers(new GetTransfersOptions()
            {
                SenderAccountNumber = senderAccountNumber,
                HasInvoices = true
            });
        }

        private IEnumerable<Client> GetClientsForThisBankAccount(IEnumerable<Transfer> transfers)
        {
            _callback($"Znalazłem zaksięgowany przelew z tego numeru konta, sprawdzam czy mogę go wykorzystać...");

            foreach (var transfer in transfers)
            {
                var invoices = _unitOfWork.TransferRepository.GetTransferInvoices(transfer.Id);

                foreach (var invoice in invoices)
                {
                    if (invoice.ClientId.HasValue)
                    {
                        _callback($"Znalazłem zaksięgowany przelew z tego numeru konta, sprawdzam czy mogę go wykorzystać...");

                        var client = _unitOfWork.ClientRepository.GetClientById(invoice.ClientId.Value);

                        yield return client;
                    }
                }
            }
        }

        private IEnumerable<Invoice> GetClientInvoicesToPay(Client client, Transfer transfer)
        {
            _callback("Typuję fakturę, którą opłacono przez klienta...");

            var invoices = _unitOfWork.InvoiceRepository
                .GetInvoices(new GetInvoicesOptions() { IsPaid = false })
                .OrderByDescending(n => n.InvoiceDate)
                .ToArray();

            var invoice = invoices.FirstOrDefault(n => n.Gross == transfer.Value);

            if (invoice != null)
            {
                _callback("Znaleziono fakturę, która pasuje do przelewu...");

                return new[] { invoice };
            }

            return CheckIfClientPaysForMoreInvoices(invoices, transfer);
        }

        private IEnumerable<Invoice> CheckIfClientPaysForMoreInvoices(Invoice[] invoices, Transfer transfer)
        {
            _callback("Nie znalazłej jednej pasującej faktury, ale sprawdzam czy klient nie zapłacił jednym przelewem za kilka...");

            while (invoices.Sum(n => n.Gross) >= transfer.Value)
            {
                Array.Resize(ref invoices, invoices.Length - 1);
            }

            if (invoices.Sum(n => n.Gross) == transfer.Value)
            {
                _callback("Znalaeziono faktury, które pasują do przelewu...");

                return invoices;
            }

            return new Invoice[0];
        }
    }
}