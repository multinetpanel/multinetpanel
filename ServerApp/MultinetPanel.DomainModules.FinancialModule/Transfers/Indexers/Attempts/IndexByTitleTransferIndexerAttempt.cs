﻿using System;
using System.Collections.Generic;
using System.Linq;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Invoices.Models;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Indexers;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Models;
using MultinetPanel.Repository.Boundary;

namespace MultinetPanel.DomainModules.FinancialModule.Transfers.Indexers.Attempts
{
    public class IndexByTitleTransferIndexerAttempt : ITransferIndexerAttempt
    {
        private readonly IUnitOfWork _unitOfWork;
        private Action<string> _callback;

        public IndexByTitleTransferIndexerAttempt(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<Invoice> SearchInvoices(Transfer transfer, Action<string> callback)
        {
            _callback = callback;

            _callback("Indeksuję przelew po tytule przelewu...");

            var numbers = TryGetInvoiceNumberFromTransferTitle(transfer.Title);
            var invoices = SearchInvoicesByNumbers(numbers);

            return invoices;
        }

        private IEnumerable<string> TryGetInvoiceNumberFromTransferTitle(string title)
        {
            _callback("Próbuję wyciągnąć numer faktury z tytułu przelewu...");

            if (!title.Contains(" "))
            {
                return new [] { title };
            }

            var numbers = title.Split(' ').Where(n => n.Length > 2 && n.Contains("/"));

            return numbers;
        }

        private IEnumerable<Invoice> SearchInvoicesByNumbers(IEnumerable<string> numbers)
        {
            foreach (var number in numbers)
            {
                _callback($"Szukam faktury po '{number}'...");

                var invoices = _unitOfWork.InvoiceRepository
                    .GetInvoices(new GetInvoicesOptions() { Number = number })
                    .ToArray();

                if (invoices.Length == 1)
                {
                    _callback("Znaleziono fakturę");

                    yield return invoices.FirstOrDefault();
                }
            }
        }
    }
}