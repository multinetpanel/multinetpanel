﻿using System;
using System.Collections.Generic;
using System.Text;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Indexers;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Models;
using MultinetPanel.Repository.Boundary;

namespace MultinetPanel.DomainModules.FinancialModule.Transfers.Indexers
{
    public class TransferIndexerLogger : ITransferIndexerLogger
    {
        private readonly IUnitOfWork _uow;

        public TransferIndexerLogger(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public void Log(Transfer transfer, string msg)
        {
            _uow.TransferRepository.AddTransferIndexerLog(transfer.Id, msg);
        }
    }
}
