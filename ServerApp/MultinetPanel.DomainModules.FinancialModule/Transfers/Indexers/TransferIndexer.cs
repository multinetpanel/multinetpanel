﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Microsoft.Extensions.DependencyInjection;
using MultinetPanel.Core.Boundary.Handlers;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Invoices.Models;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Indexers;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Models;
using MultinetPanel.DomainModules.FinancialModule.Transfers.Indexers.Attempts;
using MultinetPanel.Repository.Boundary;

namespace MultinetPanel.DomainModules.FinancialModule.Transfers.Indexers
{
    public class TransferIndexer : ITransferIndexer
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IServiceProvider _serviceProvider;
        private readonly ITransferIndexerLogger _logger;
        private readonly IObjectMapper _objectMapper;
        private Transfer _transfer;

        public TransferIndexer(IUnitOfWork unitOfWork, IServiceProvider serviceProvider, ITransferIndexerLogger logger, IObjectMapper objectMapper)
        {
            _unitOfWork = unitOfWork;
            _serviceProvider = serviceProvider;
            _logger = logger;
            _objectMapper = objectMapper;
        }

        public void IndexTransfers()
        {
            var transfers = GetNotIndexedTransfers();

            foreach (var transfer in transfers)
            {
                IndexTransfer(transfer);
            }
        }

        private IEnumerable<Transfer> GetNotIndexedTransfers()
        {
            return _unitOfWork.TransferRepository.GetTransfers(new GetTransfersOptions()
            {
                HasInvoices = false,
                PageSize = int.MaxValue
            });
        }

        public bool IndexTransfer(Transfer transfer)
        {
            _transfer = transfer;

            var invoices = GetTransferInvoices().ToArray();

            if (invoices.Length == 0)
            {
                Log("Nie mogę dopasować przelewu do żadnej faktury...");

                return false;
            }

            AddTransferToInvoice(invoices);

            return true;
        }

        private IEnumerable<Invoice> GetTransferInvoices()
        {
            foreach (var attempt in GetIndexerAttempts())
            {
                var invoices = attempt.SearchInvoices(_transfer, Log).ToArray();

                if (invoices.Length > 0)
                {
                    return invoices;
                }
            }

            return new Invoice[0];
        }

        private void AddTransferToInvoice(IEnumerable<Invoice> invoices)
        {
            foreach (var invoice in invoices)
            {
                Log($"Przypinam fakturę nr {invoice.Number} do przelewu...");

                _unitOfWork.TransferRepository.IndexTransfer(_transfer.Id, invoice.Id);
            }

            _unitOfWork.SaveChanges();
        }

        private void Log(string msg)
        {
            _logger.Log(_transfer, msg);
        }

        private IEnumerable<ITransferIndexerAttempt> GetIndexerAttempts()
        {
            return new ITransferIndexerAttempt[]
            {
                _serviceProvider.GetService<IndexByTitleTransferIndexerAttempt>(),
                _serviceProvider.GetService<IndexByBankAccountTransferIndexerAttempt>()
            };
        }
    }
}
