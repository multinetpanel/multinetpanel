﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MultinetPanel.Core.Boundary.EmailHandlers;
using MultinetPanel.Core.Boundary.EmailHandlers.Models;
using MultinetPanel.Core.Boundary.Handlers;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Configurations;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Importers;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Importers.Models;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Indexers;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Models;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Parsers;
using MultinetPanel.Repository.Boundary;

namespace MultinetPanel.DomainModules.FinancialModule.Transfers.Importers
{
    public class TransfersImporter : ITransfersImporter
    {
        private readonly IUnitOfWork _uow;
        private readonly IEmailHandler _emailHandler;
        private readonly TransfersEmailConfig _config;
        private readonly ITransfersImporterLogger _logger;
        private readonly ITransferParser _transferParser;
        private readonly ITransferIndexer _transferIndexer;
        private readonly IObjectMapper _objectMapper;
        private readonly StringBuilder _log;

        public TransfersImporter(
            IUnitOfWork uow,
            IEmailHandler emailHandler,
            TransfersEmailConfig config,
            ITransfersImporterLogger logger,
            ITransferParser transferParser,
            ITransferIndexer transferIndexer,
            IObjectMapper objectMapper)
        {
            _uow = uow;
            _emailHandler = emailHandler;
            _config = config;
            _logger = logger;
            _transferParser = transferParser;
            _transferIndexer = transferIndexer;
            _objectMapper = objectMapper;
            _log = new StringBuilder();
        }

        public void Import()
        {
            var lastImportDate = GetLastImportDate();
            var messagesWithAttachments = GetMessagesWithAttachments(lastImportDate);
            var attachments = GetAttachments(messagesWithAttachments);
            var transfers = GetTransfersFromAttachments(attachments).ToArray();

            SaveTransfers(transfers);
            IndexTransfers(transfers);

            CreateLog(transfers.Length);
        }

        private DateTime GetLastImportDate()
        {
            var logs = _uow.TransferRepository
                .GetImportLogs(new GetTransfersImportLogsOptions() { IsSuccessful = true, PageSize = 1, Page = 1 })
                .FirstOrDefault();

            var lastImportDate = logs?.Date ?? DateTime.MinValue;

            AddToLog($"Datę ostatniego udanego importu faktur ustawiono na {lastImportDate:yyyy-MM-dd HH:mm:ss}");

            return lastImportDate;
        }

        private IEnumerable<EmailMessage> GetMessagesWithAttachments(DateTime toDate)
        {
            var getMessagesWithTransfers = _emailHandler
                .GetMessages(_config, toDate)
                .Where(message => message
                    .Attachments.Any(IsTransfersAttachment))
                .ToArray();

            AddToLog($"Pobrano {getMessagesWithTransfers.Length} wiadomości z przelewami.");

            return getMessagesWithTransfers;
        }

        private IEnumerable<byte[]> GetAttachments(IEnumerable<EmailMessage> messages)
        {
            var attachments = messages
                .SelectMany(message => message.Attachments
                    .Where(IsTransfersAttachment)
                    .Select(n => n.Attachment))
                .ToArray();

            AddToLog($"Z wiadomości pobrano {attachments.Length} załączników.");

            return attachments;
        }

        private IEnumerable<Transfer> GetTransfersFromAttachments(IEnumerable<byte[]> transfersAttachments)
        {
            var transfers = transfersAttachments
                .SelectMany(attachment => _transferParser.GetTransfers(attachment, Encoding.GetEncoding("windows-1250")))
                .Where(n => !CheckIfTransferAlreadyExists(n))
                .ToArray();

            AddToLog($"Z załączników pobrano {transfers.Length} przelewów.");

            return transfers;
        }

        private void SaveTransfers(IEnumerable<Transfer> transfers)
        {
            foreach (var transfer in transfers)
            {
                _uow.TransferRepository.AddTransfer(transfer);
            }

            _uow.SaveChanges();
        }

        private IEnumerable<bool> IndexTransfers(IEnumerable<Transfer> transfers)
        {
            var indexed = transfers
                .Select(transfer => _transferIndexer.IndexTransfer(transfer))
                .Where(n => n)
                .ToArray();

            AddToLog($"Udało się zaindeksować {indexed.Length} przelewów.");

            return indexed;
        }

        private bool CheckIfTransferAlreadyExists(Transfer transfer)
        {
            var options = _objectMapper.Map<GetTransfersOptions>(transfer);

            var dbTransfer = _uow.TransferRepository.GetTransfers(options).FirstOrDefault();

            if (dbTransfer == null)
            {
                return false;
            }

            AddToLog("W systemie znaleziono już identyczny przelew, pomijam jego import...");

            return true;

        }

        private bool IsTransfersAttachment(EmailMessageAttachment attachment)
        {
            return attachment.Name?.EndsWith(".csv") ?? false;
        }

        private void AddToLog(string msg)
        {
            _log.AppendLine(msg);
        }

        private void CreateLog(int transfersCount)
        {
            _logger.Log(new TransfersImportLog()
            {
                Date = DateTime.Now,
                IsSuccessful = true,
                TransfersCount = transfersCount,
                Message = _log.ToString()
            });
        }
    }
}
