﻿using System;
using System.Collections.Generic;
using System.Text;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Importers;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Importers.Models;
using MultinetPanel.Repository.Boundary;

namespace MultinetPanel.DomainModules.FinancialModule.Transfers.Importers
{
    public class TransfersImporterLogger : ITransfersImporterLogger
    {
        private readonly IUnitOfWork _unitOfWork;

        public TransfersImporterLogger(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void Log(TransfersImportLog log)
        {
            _unitOfWork.TransferImporterLogsRepository.AddLog(log);

            _unitOfWork.SaveChanges();
        }
    }
}
