﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Models;

namespace MultinetPanel.DomainModules.FinancialModule.Transfers.Mappers
{
    public class GetTransfersOptionsMapper : Profile
    {
        public GetTransfersOptionsMapper()
        {
            CreateMap<Transfer, GetTransfersOptions>();
        }
    }
}
