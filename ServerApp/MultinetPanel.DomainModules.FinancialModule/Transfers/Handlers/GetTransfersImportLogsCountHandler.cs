﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Requests;
using MultinetPanel.Repository.Boundary;

namespace MultinetPanel.DomainModules.FinancialModule.Transfers.Handlers
{
    public class GetTransfersImportLogsCountHandler : IRequestHandler<GetTransfersImportLogsCountRequest, int>
    {
        private readonly IUnitOfWork _unitOfWork;

        public GetTransfersImportLogsCountHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Task<int> Handle(GetTransfersImportLogsCountRequest request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_unitOfWork.TransferImporterLogsRepository.GetLogsCount());
        }
    }
}