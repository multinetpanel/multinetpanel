﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Models;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Requests;
using MultinetPanel.Repository.Boundary;

namespace MultinetPanel.DomainModules.FinancialModule.Transfers.Handlers
{
    public class GetTransferHandler : IRequestHandler<GetTransferRequest, DetailedTransfer>
    {
        private readonly IUnitOfWork _unitOfWork;

        public GetTransferHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Task<DetailedTransfer> Handle(GetTransferRequest request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_unitOfWork.TransferRepository.GetDetailedTransfer(request.Id));
        }
    }
}
