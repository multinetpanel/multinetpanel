﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Importers.Models;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Requests;
using MultinetPanel.Repository.Boundary;

namespace MultinetPanel.DomainModules.FinancialModule.Transfers.Handlers
{
    public class GetTransfersImportLogHandler : IRequestHandler<GetTransfersImportLogRequest, TransfersImportLog>
    {
        private readonly IUnitOfWork _unitOfWork;

        public GetTransfersImportLogHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Task<TransfersImportLog> Handle(GetTransfersImportLogRequest request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_unitOfWork.TransferImporterLogsRepository.GetLog(request.Id));
        }
    }
}