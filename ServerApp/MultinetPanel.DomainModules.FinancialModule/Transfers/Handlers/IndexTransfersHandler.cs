﻿using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Indexers;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Models;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Requests;
using MultinetPanel.Repository.Boundary;

namespace MultinetPanel.DomainModules.FinancialModule.Transfers.Handlers
{
    public class IndexTransfersHandler : IRequestHandler<IndexTransfersRequest>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ITransferIndexer _indexer;

        public IndexTransfersHandler(IUnitOfWork unitOfWork, ITransferIndexer indexer)
        {
            _unitOfWork = unitOfWork;
            _indexer = indexer;
        }

        public Task<Unit> Handle(IndexTransfersRequest request, CancellationToken cancellationToken)
        {
            var transfers = _unitOfWork.TransferRepository.GetTransfers(new GetTransfersOptions()
            {
                HasInvoices = false
            });

            foreach (var transfer in transfers)
            {
                _indexer.IndexTransfer(transfer);
            }

            return Unit.Task;
        }
    }
}
