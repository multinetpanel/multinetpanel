﻿using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Models;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Requests;
using MultinetPanel.Repository.Boundary;

namespace MultinetPanel.DomainModules.FinancialModule.Transfers.Handlers
{
    public class GetTransfersHandler : IRequestHandler<GetTransfersRequest, IEnumerable<Transfer>>
    {
        private readonly IUnitOfWork _unitOfWork;

        public GetTransfersHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Task<IEnumerable<Transfer>> Handle(GetTransfersRequest request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_unitOfWork.TransferRepository.GetTransfers(request));
        }
    }
}
