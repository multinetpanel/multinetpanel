﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Importers.Models;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Requests;
using MultinetPanel.Repository.Boundary;

namespace MultinetPanel.DomainModules.FinancialModule.Transfers.Handlers
{
    public class GetTransfersImportLogsHandler : IRequestHandler<GetTransfersImportLogsRequest, IEnumerable<TransfersImportLog>>
    {
        private readonly IUnitOfWork _unitOfWork;

        public GetTransfersImportLogsHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Task<IEnumerable<TransfersImportLog>> Handle(GetTransfersImportLogsRequest request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_unitOfWork.TransferImporterLogsRepository.GetLogs(request));
        }
    }
}