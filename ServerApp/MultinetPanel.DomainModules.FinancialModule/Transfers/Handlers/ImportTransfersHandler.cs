﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using MultinetPanel.Core.Boundary.EmailHandlers;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Configurations;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Importers;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Importers.Models;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Indexers;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Parsers;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Requests;
using MultinetPanel.Repository.Boundary;

namespace MultinetPanel.DomainModules.FinancialModule.Transfers.Handlers
{
    public class ImportTransfersHandler : IRequestHandler<ImportTransfersRequest>
    {
        private readonly ITransfersImporter _transfersImporter;
        private readonly ITransferIndexer _transferIndexer;

        public ImportTransfersHandler(ITransfersImporter transfersImporter, ITransferIndexer transferIndexer)
        {
            _transfersImporter = transfersImporter;
            _transferIndexer = transferIndexer;
        }

        public Task<Unit> Handle(ImportTransfersRequest request, CancellationToken cancellationToken)
        {
            _transfersImporter.Import();
            _transferIndexer.IndexTransfers();

            return Unit.Task;
        }
    }
}
