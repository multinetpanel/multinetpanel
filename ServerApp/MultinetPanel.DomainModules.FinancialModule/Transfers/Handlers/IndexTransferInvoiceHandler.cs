﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Requests;
using MultinetPanel.Repository.Boundary;

namespace MultinetPanel.DomainModules.FinancialModule.Transfers.Handlers
{
    public class IndexTransferInvoiceHandler : IRequestHandler<IndexTransferInvoiceRequest>
    {
        private readonly IUnitOfWork _unitOfWork;

        public IndexTransferInvoiceHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Task<Unit> Handle(IndexTransferInvoiceRequest request, CancellationToken cancellationToken)
        {
            _unitOfWork.TransferRepository.IndexTransfer(request.TransferId, request.InvoiceId);

            _unitOfWork.TransferRepository.AddTransferIndexerLog(request.TransferId, "Przypięto ręcznie fakturę do przelewu.");

            _unitOfWork.SaveChanges();

            return Unit.Task;
        }
    }
}