﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Requests;
using MultinetPanel.Repository.Boundary;

namespace MultinetPanel.DomainModules.FinancialModule.Transfers.Handlers
{
    public class GetTransfersCountHandler : IRequestHandler<GetTransfersCountRequest, int>
    {
        private readonly IUnitOfWork _unitOfWork;

        public GetTransfersCountHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Task<int> Handle(GetTransfersCountRequest request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_unitOfWork.TransferRepository.GetTransfersCount(request));
        }
    }
}