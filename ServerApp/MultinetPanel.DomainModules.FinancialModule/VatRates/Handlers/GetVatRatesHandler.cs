﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using MultinetPanel.DomainModules.FinancialModule.Boundary.VatRates.Models;
using MultinetPanel.DomainModules.FinancialModule.Boundary.VatRates.Requests;
using MultinetPanel.Repository.Boundary;

namespace MultinetPanel.DomainModules.FinancialModule.VatRates.Handlers
{
    public class GetVatRatesHandler : IRequestHandler<GetVatRatesRequest, IEnumerable<VatRate>>
    {
        private readonly IUnitOfWork _unitOfWork;

        public GetVatRatesHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Task<IEnumerable<VatRate>> Handle(GetVatRatesRequest request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_unitOfWork.VatRateRepository.GetVatRates());
        }
    }
}
