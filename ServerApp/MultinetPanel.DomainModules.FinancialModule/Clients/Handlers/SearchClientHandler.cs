﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Clients.Models;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Clients.Requests;
using MultinetPanel.Repository.Boundary;

namespace MultinetPanel.DomainModules.FinancialModule.Clients.Handlers
{
    public class SearchClientHandler : IRequestHandler<SearchClientRequest, IEnumerable<Client>>
    {
        private readonly IUnitOfWork _unitOfWork;

        public SearchClientHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Task<IEnumerable<Client>> Handle(SearchClientRequest request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_unitOfWork.ClientRepository.SearchClient(request));
        }
    }
}
