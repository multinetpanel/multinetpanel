﻿using System.Collections.Generic;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Clients.Models;
using MultinetPanel.DomainModules.FinancialModule.Boundary.People.Models;

namespace MultinetPanel.Repository.Boundary.People
{
    public interface IPersonRepository
    {
        IEnumerable<Person> SearchPerson(SearchPersonOptions options);
    }
}
