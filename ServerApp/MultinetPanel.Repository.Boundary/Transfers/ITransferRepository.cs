﻿using System;
using System.Collections.Generic;
using System.Text;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Importers.Models;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Invoices.Models;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Models;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Transfers.Requests;

namespace MultinetPanel.Repository.Boundary.Transfers
{
    public interface ITransferRepository
    {
        IEnumerable<Transfer> GetTransfers(GetTransfersOptions getOptions);
        IEnumerable<Invoice> GetTransferInvoices(int transferId);
        void IndexTransfer(int transferId, int invoiceId);
        void AddTransferIndexerLog(int transferId, string message);
        IEnumerable<TransfersImportLog> GetImportLogs(GetTransfersImportLogsOptions options);
        void AddTransfer(Transfer transfer);
        int GetTransfersCount(GetTransfersOptions options);
        Transfer GetTransfer(int id);
        DetailedTransfer GetDetailedTransfer(int id);
    }
}
