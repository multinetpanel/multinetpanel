﻿using System;
using System.Collections.Generic;
using System.Text;
using MultinetPanel.Core.Boundary.CoreModels;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Importers.Models;

namespace MultinetPanel.Repository.Boundary.Transfers
{
    public interface ITransferImporterLogsRepository
    {
        void AddLog(TransfersImportLog log);
        IEnumerable<TransfersImportLog> GetLogs(PagedModel paginationModel);
        TransfersImportLog GetLog(int id);
        int GetLogsCount();
    }
}
