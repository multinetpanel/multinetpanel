﻿using System;
using System.Collections.Generic;
using System.Text;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Invoices.Models;

namespace MultinetPanel.Repository.Boundary.Invoices
{
    public interface IInvoiceRepository
    {
        Invoice GetInvoice(int id);
        IEnumerable<Invoice> GetInvoices(GetInvoicesOptions getInvoicesOptions);
        InvoicesSum GetInvoicesSum(GetInvoicesOptions getInvoicesOptions);
        int GetInvoicesCount(GetInvoicesOptions getInvoicesOptions);
    }
}
