﻿using System;
using System.Collections.Generic;
using System.Text;
using MultinetPanel.Repository.Boundary.Clients;
using MultinetPanel.Repository.Boundary.Invoices;
using MultinetPanel.Repository.Boundary.People;
using MultinetPanel.Repository.Boundary.Transfers;
using MultinetPanel.Repository.Boundary.VatRates;

namespace MultinetPanel.Repository.Boundary
{
    public interface IUnitOfWork : IDisposable
    {
        ITransferRepository TransferRepository { get; }
        IInvoiceRepository InvoiceRepository { get; }
        IClientRepository ClientRepository { get; }
        IPersonRepository PersonRepository { get; }
        IVatRateRepository VatRateRepository { get; }
        ITransferImporterLogsRepository TransferImporterLogsRepository { get; }

        void SaveChanges();
    }
}
