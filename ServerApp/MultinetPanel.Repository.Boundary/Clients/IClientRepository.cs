﻿using System;
using System.Collections.Generic;
using System.Text;
using MultinetPanel.DomainModules.FinancialModule.Boundary.Clients.Models;

namespace MultinetPanel.Repository.Boundary.Clients
{
    public interface IClientRepository
    {
        Client GetClientById(int clientId);
        IEnumerable<Client> SearchClient(SearchClientOptions options);
    }
}
