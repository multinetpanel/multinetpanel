﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MultinetPanel.Repository.Boundary.Migrations
{
    public interface ISeederManager
    {
        void Seed();
    }
}
