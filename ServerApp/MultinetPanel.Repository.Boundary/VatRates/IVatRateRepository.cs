﻿using System;
using System.Collections.Generic;
using System.Text;
using MultinetPanel.DomainModules.FinancialModule.Boundary.VatRates.Models;

namespace MultinetPanel.Repository.Boundary.VatRates
{
    public interface IVatRateRepository
    {
        IEnumerable<VatRate> GetVatRates();
    }
}
