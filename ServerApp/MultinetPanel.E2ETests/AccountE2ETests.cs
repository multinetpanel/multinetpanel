﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using MultinetPanel.Application.Boundary.Auth.Requests;
using MultinetPanel.E2ETests.Shared;
using Xunit;

namespace MultinetPanel.E2ETests
{
    [Collection(nameof(AppClientCollection))]
    public class AccountE2ETests
    {
        private readonly AppClientFixture _appClient;

        public AccountE2ETests(AppClientFixture appClient)
        {
            _appClient = appClient;
        }

        [Fact]
        public void When_pass_credentials_for_not_existed_user_Should_get_exception()
        {
            var request = new SignInRequest() { Username = "test", Password = "1" };

            var result = _appClient.Post("account/signin", request);

            Assert.Equal("Nie znaleziono użytkownika o takim loginie", result);
        }
    }
}
