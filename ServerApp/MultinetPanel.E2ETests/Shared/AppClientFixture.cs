﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace MultinetPanel.E2ETests.Shared
{
    public class AppClientFixture : IDisposable
    {
        public HttpClient Client { get; }

        public AppClientFixture()
        {
            var builder = new WebHostBuilder()
                .UseEnvironment("Development")
                .UseConfiguration(new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                    .Build())
                .UseStartup<Startup>();

            var testServer = new TestServer(builder);

            Client = testServer.CreateClient();
        }

        public string Post(string uri, object postObject)
        {
            var json = JsonConvert.SerializeObject(postObject);

            var content = new StringContent(json, Encoding.UTF8, "application/json");

            return Client.PostAsync(uri, content).Result.Content.ReadAsStringAsync().Result;
        }

        public TResponse Post<TResponse>(string uri, object postObject)
        {
            var response = Post(uri, postObject);

            return JsonConvert.DeserializeObject<TResponse>(response);
        }

        public void Dispose()
        {
            Client.Dispose();
        }
    }
}
