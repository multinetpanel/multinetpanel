import { Routes } from '@angular/router';
import { MainLayoutComponent } from './shared/layout/components/main-layout/main-layout.component';
import { Breadcrumb } from './shared/breadcrumbs/models/breadcrumb';
import { AuthGuard } from './user/auth/services/auth-guard.service';


export const appRoutes: Routes = [];
