import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InvoicesListComponent } from './invoices/components/invoices-list/invoices-list.component';
import { RouterModule } from '@angular/router';
import { financeRoutes } from './finance.routes';
import { TableModule } from 'primeng/table';
import { InvoiceComponent } from './invoices/components/invoice/invoice.component';
import { SharedModule } from '../shared/shared.module';
import { TransfersListComponent } from './transfers/components/transfers-list/transfers-list.component';
import { TransferComponent } from './transfers/components/transfer/transfer.component';
import { TransfersImportLogComponent } from './transfers/components/transfers-import-log/transfers-import-log.component';
import { TransfersImportLogListComponent } from './transfers/components/transfers-import-log-list/transfers-import-log-list.component';
import { InvoicesListCoreComponent } from './invoices/components/invoices-list-core/invoices-list-core.component';
import { InvoiceAddComponent } from './invoices/components/invoice-add/invoice-add.component';
import { InvoiceEditComponent } from './invoices/components/invoice-edit/invoice-edit.component';
import { InvoiceCorrectionComponent } from './invoices/components/invoice-correction/invoice-correction.component';
import { InvoiceEditCoreComponent } from './invoices/components/invoice-edit-core/invoice-edit-core.component';
import { InvoiceEditCorePersonComponent } from './invoices/components/invoice-edit-core-person/invoice-edit-core-person.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(financeRoutes),
    TableModule,
    SharedModule
  ],
  declarations: [
    InvoicesListComponent,
    InvoiceComponent,
    TransfersListComponent,
    TransferComponent,
    TransfersImportLogComponent,
    TransfersImportLogListComponent,
    InvoicesListCoreComponent,
    InvoiceAddComponent,
    InvoiceEditComponent,
    InvoiceCorrectionComponent,
    InvoiceEditCoreComponent,
    InvoiceEditCorePersonComponent
  ]
})
export class FinanceModule { }
