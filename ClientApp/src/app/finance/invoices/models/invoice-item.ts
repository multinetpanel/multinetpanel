import { VatRate } from '../../vatRates/models/vat-rate';

export class InvoiceItem {
    id: number;
    index: number;
    name: string;
    quantity: number;
    netUnitPrice: number;
    vatRateId: number;
    net: number;
    vat: number;
    gross: number;
    vatRate: VatRate;

    constructor(invoiceItem?: Partial<InvoiceItem>) {
        Object.assign(this, invoiceItem);
    }
}
