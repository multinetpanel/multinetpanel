import { NgbDate } from '@ng-bootstrap/ng-bootstrap';
import { GetInvoicesOptions } from './get-invoices-options';
import { NgbDateExtensions } from 'src/app/shared/extensions/ngb-date-extensions';

export class GetInvoicesDates {
    invoiceDateFrom: NgbDate;
    invoiceDateTo: NgbDate;
    saleDateFrom: NgbDate;
    saleDateTo: NgbDate;
    paymentDeadlineDateFrom: NgbDate;
    paymentDeadlineDateTo: NgbDate;

    constructor(dates?: Partial<GetInvoicesDates>) {
        Object.assign(this, dates);
    }

    public setDatesToOptions(options: GetInvoicesOptions): void {
        options.invoiceDateFrom = NgbDateExtensions.toDate(this.invoiceDateFrom);
        options.invoiceDateTo = NgbDateExtensions.toDate(this.invoiceDateTo);
        options.saleDateFrom = NgbDateExtensions.toDate(this.saleDateFrom);
        options.saleDateTo = NgbDateExtensions.toDate(this.saleDateTo);
        options.paymentDeadlineDateFrom = NgbDateExtensions.toDate(this.paymentDeadlineDateFrom);
        options.paymentDeadlineDateTo = NgbDateExtensions.toDate(this.paymentDeadlineDateTo);
    }
}
