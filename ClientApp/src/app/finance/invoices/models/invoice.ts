import { InvoiceItem } from './invoice-item';
import { Person } from 'src/app/client/people/models/person';
import { Transfer } from '../../transfers/models/transfer';
import { Client } from 'src/app/client/clients/models/client';

export class Invoice {
    id: number;
    number: string;
    invoiceDate: Date;
    saleDate: Date;
    paymentDeadlineDate: Date;
    clientId: number;
    client: Client;
    net: number;
    vat: number;
    gross: number;
    invoiceItems: InvoiceItem[];
    issuer: Person;
    purchaser: Person;
    issuerId: number;
    purchaserId: number;
    transfers: Transfer[];

    constructor(invoice?: Partial<Invoice>) {
        Object.assign(this, invoice);
    }
}


