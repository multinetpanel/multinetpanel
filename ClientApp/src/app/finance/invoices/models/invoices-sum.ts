export class InvoicesSum {
    totalCount: number;
    paidCount: number;
    noPaidCount: number;

    totalNet: number;
    paidNet: number;
    noPaidNet: number;

    totalVat: number;
    paidVat: number;
    noPaidVat: number;

    totalGross: number;
    paidGross: number;
    noPaidGross: number;

    constructor(invoiceSum?: Partial<InvoicesSum>) {
        Object.assign(this, invoiceSum);
    }
}
