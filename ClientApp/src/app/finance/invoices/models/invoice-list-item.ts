export class InvoiceListItem {
    id: number;
    number: string;
    issueDate: Date;
    paymentDate: Date;
    net: number;
    gross: number;
    interest: number;
    wasPaid: boolean;

    constructor(item: Partial<InvoiceListItem>) {
        Object.assign(this, item);
    }
}
