import { PagedModel } from 'src/app/shared/models/paged-model';
import { PersonType } from 'src/app/client/people/models/person-type';

export class GetInvoicesOptions extends PagedModel {
    type: PersonType;
    pesel: string;
    regon: string;
    nip: string;
    name: string;
    phone: string;
    address: string;
    number: string;
    isPaid?: boolean;
    net: number;
    vat: number;
    gross: number;
    invoiceDateFrom?: Date;
    invoiceDateTo?: Date;
    saleDateFrom?: Date;
    saleDateTo?: Date;
    paymentDeadlineDateFrom?: Date;
    paymentDeadlineDateTo?: Date;

    constructor(options?: Partial<GetInvoicesOptions>) {
        super();

        Object.assign(this, options);
    }
}
