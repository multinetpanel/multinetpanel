import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Person } from 'src/app/client/people/models/person';
import { PersonService } from 'src/app/client/people/services/person.service';

@Component({
  selector: 'app-invoice-edit-core-person',
  templateUrl: './invoice-edit-core-person.component.html',
  styleUrls: ['./invoice-edit-core-person.component.css']
})
export class InvoiceEditCorePersonComponent implements OnInit {

  @Input()
  public person: Person = new Person({ type: 1 });

  @Input()
  public name = '';

  public people: Person[] = [];
  public selectedPerson: Person = new Person();

  constructor(private personService: PersonService) { }

  ngOnInit() {
  }

  public getPeople(event) {
    this.personService.search(event.query).subscribe(n => this.people = n);
  }

  public onPersonSelect() {
    this.person.name = this.selectedPerson.name;
    this.person.surname = this.selectedPerson.surname;
    this.person.pesel = this.selectedPerson.pesel;
    this.person.email = this.selectedPerson.email;
    this.person.postCode = this.selectedPerson.postCode;
    this.person.city = this.selectedPerson.city;
    this.person.street = this.selectedPerson.street;
    this.person.house = this.selectedPerson.house;
    this.person.flat = this.selectedPerson.flat;
    this.person.type = this.selectedPerson.type;
    this.person.companyName = this.selectedPerson.companyName;
    this.person.regon = this.selectedPerson.regon;
    this.person.nip = this.selectedPerson.nip;

    this.selectedPerson = null;
  }
}
