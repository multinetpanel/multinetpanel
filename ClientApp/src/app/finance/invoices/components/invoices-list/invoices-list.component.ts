import { Component, OnInit } from '@angular/core';
import { InvoiceListItem } from '../../models/invoice-list-item';
import { Router } from '@angular/router';
import { GetInvoicesOptions } from '../../models/get-invoices-options';
import { PersonType } from 'src/app/client/people/models/person-type';
import { InvoiceService } from '../../services/invoice.service';
import { Invoice } from '../../models/invoice';
import { LazyLoadEvent } from 'primeng/components/common/lazyloadevent';
import { GetInvoicesDates } from '../../models/get-invoices-dates';
import { NgbDate } from '@ng-bootstrap/ng-bootstrap';
import { InvoicesSum } from '../../models/invoices-sum';

@Component({
  selector: 'app-invoices-list',
  templateUrl: './invoices-list.component.html',
  styleUrls: ['./invoices-list.component.scss']
})
export class InvoicesListComponent implements OnInit {

  public getOptions: GetInvoicesOptions = new GetInvoicesOptions({ isPaid: null, type: PersonType.person });
  public invoicesSum: InvoicesSum = new InvoicesSum();

  constructor(private router: Router, private invoiceService: InvoiceService) { }

  ngOnInit() {

  }

  public fetchData(event?: LazyLoadEvent): void {
    this.getInvoicesSum();
  }

  public onRowSelect(invoice: Invoice): void {
    this.router.navigate(['finance/invoice', invoice.id]);
  }

  public add(): void {
    this.router.navigate(['finance/invoice/add']);
  }

  private getInvoicesSum(): void {
    this.invoiceService.getInvoicesSum(this.getOptions).subscribe(n => this.invoicesSum = n);
  }
}
