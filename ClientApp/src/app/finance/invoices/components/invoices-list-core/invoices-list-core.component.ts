import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { InvoiceService } from '../../services/invoice.service';
import { GetInvoicesOptions } from '../../models/get-invoices-options';
import { InvoiceListItem } from '../../models/invoice-list-item';
import { PersonType } from 'src/app/client/people/models/person-type';
import { Invoice } from '../../models/invoice';
import { GetInvoicesDates } from '../../models/get-invoices-dates';
import { LazyLoadEvent } from 'primeng/components/common/lazyloadevent';
import { NgbDate } from '@ng-bootstrap/ng-bootstrap';
import { NgbDateExtensions } from 'src/app/shared/extensions/ngb-date-extensions';


@Component({
  selector: 'app-invoices-list-core',
  templateUrl: './invoices-list-core.component.html',
  styleUrls: ['./invoices-list-core.component.css']
})
export class InvoicesListCoreComponent implements OnInit {

  @Input()
  public getOptions: GetInvoicesOptions = new GetInvoicesOptions({ isPaid: null, type: PersonType.person });

  @Input()
  public dates: GetInvoicesDates = new GetInvoicesDates();

  @Output()
  public selectedEvent: EventEmitter<Invoice> = new EventEmitter<Invoice>();

  @Output()
  public fetchDataEvent: EventEmitter<LazyLoadEvent> = new EventEmitter<LazyLoadEvent>();

  public totalItems: number;
  public selectedInvoice: Invoice;
  public loading = true;
  public cols: any[];
  public invoices: Invoice[] = [];
  public currentTab = 1;

  constructor(private invoiceService: InvoiceService) { }

  ngOnInit() {
    this.setInitialDate();
    this.getCols();
    this.dates.setDatesToOptions(this.getOptions);
    this.getInvoicesCount();
  }

  public getInvoices(event?: LazyLoadEvent): void {
    this.fetchDataEvent.next(event);

    this.loading = true;

    this.getOptions.page = event.first / event.rows + 1;
    this.getOptions.pageSize = event.rows;

    this.invoiceService.getInvoices(this.getOptions).subscribe(n => {
      this.invoices = n;
      this.loading = false;
    });
  }

  public applyFilters(): void {
    this.dates.setDatesToOptions(this.getOptions);
    this.getInvoicesCount();
    this.getInvoices({first: 0, rows: 10});
  }

  public onRowSelect(event): void {
    this.selectedEvent.next(event.data as Invoice);
  }

  private getInvoicesCount(): void {
    this.invoiceService.getInvoicesCount(this.getOptions).subscribe(n => this.totalItems = n);
  }

  private getCols(): void {
    this.cols = [
      { field: 'number', header: 'Numer' },
      { field: 'invoiceDate', header: 'Daty', width: 165, class: 'text-center' },
      { field: 'client', header: 'Dane klienta', width: 200, class: 'text-center' },
      { field: 'client', header: 'Adres klienta', width: 200, class: 'text-center' },
      { field: 'net', header: 'Kwoty', width: 175, class: 'text-center' },
    ];
  }

  private setInitialDate(): void {
    this.dates = new GetInvoicesDates({
      invoiceDateFrom: NgbDateExtensions.getStartOfMonth(),
      invoiceDateTo: NgbDateExtensions.getEndOfMonth()
    });
  }
}
