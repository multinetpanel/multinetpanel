import { Component, OnInit } from '@angular/core';
import { Person } from 'src/app/client/people/models/person';
import { Invoice } from '../../models/invoice';
import { NgbDate } from '@ng-bootstrap/ng-bootstrap';
import { NgbDateExtensions } from 'src/app/shared/extensions/ngb-date-extensions';
import { Client } from 'src/app/client/clients/models/client';
import { ClientService } from 'src/app/client/clients/services/client.service';
import { InvoiceItem } from '../../models/invoice-item';
import { VatRate } from '../../../vatRates/models/vat-rate';
import { VatRateService } from 'src/app/finance/vatRates/services/vat-rate.service';

@Component({
  selector: 'app-invoice-edit-core',
  templateUrl: './invoice-edit-core.component.html',
  styleUrls: ['./invoice-edit-core.component.css']
})
export class InvoiceEditCoreComponent implements OnInit {

  public invoice: Invoice = new Invoice({
      issuer: new Person({type: 1}),
      purchaser: new Person({type: 1}),
      invoiceItems: []
    });

  public invoiceDate: NgbDate;
  public saleDate: NgbDate;
  public paymentDate: NgbDate;

  public selectedClient: Client;
  public clients: Client[] = [];

  public invoiceItem: InvoiceItem = new InvoiceItem({ quantity: 1, netUnitPrice: 0 });

  public vatRates: VatRate[] = [];

  constructor(
    private clientService: ClientService,
    private vatRateService: VatRateService) { }

  ngOnInit() {
    this.setInitialDate();
    this.getVatRates();
  }

  public getClients(event): void {
    this.clientService.search(event.query).subscribe(n => this.clients = n);
  }

  public onClientSelect(): void {
    this.invoice.purchaser = this.selectedClient.person;
  }

  public unpinClient(): void {
    this.selectedClient = null;
    this.invoice.purchaser = new Person({ type: 1 });
  }

  public addPosition(): void {
    this.invoice.invoiceItems.push(this.calculatePositionValues(this.invoiceItem));

    this.invoiceItem = new InvoiceItem({ quantity: 1, vatRateId: this.vatRates[0].id });
  }

  private calculatePositionValues(item: InvoiceItem): InvoiceItem {
    item.vatRate = this.vatRates.find(n => n.id === item.vatRateId);
    item.net = item.quantity * item.netUnitPrice;
    item.vat = item.net * item.vatRate.value / 100;
    item.gross = item.net + item.vat;

    return item;
  }

  public removePosition(item: InvoiceItem): void {
    this.invoice.invoiceItems = this.invoice.invoiceItems.filter(n => n !== item);
  }

  private setInitialDate(): void {
    this.invoiceDate = NgbDateExtensions.getCurrentDay();
    this.saleDate = NgbDateExtensions.getCurrentDay();
    this.paymentDate = NgbDateExtensions.getCurrentDay(14);
  }

  private getVatRates(): void {
    this.vatRateService.getVatRates().subscribe(n => {
      this.vatRates = n;

      this.invoiceItem.vatRateId = this.vatRates[0].id;
    });
  }
}
