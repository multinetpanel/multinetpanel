import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from '../../../../shared/breadcrumbs/services/breadcrumb.service';
import { InvoiceService } from '../../services/invoice.service';
import { Invoice } from '../../models/invoice';
import { ActivatedRoute, Router } from '@angular/router';
import { Person } from 'src/app/client/people/models/person';
import { NotifyService } from 'src/app/shared/services/notify.service';
import { Client } from 'src/app/client/clients/models/client';

@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.css']
})
export class InvoiceComponent implements OnInit {

  private id: number;
  public invoice: Invoice = new Invoice({issuer: new Person(), purchaser: new Person(), client: new Client()});

  constructor(
    private breadcrumbService: BreadcrumbService,
    private invoiceService: InvoiceService,
    private activetedRoute: ActivatedRoute,
    private notifyService: NotifyService,
    private router: Router) { }

  ngOnInit() {
    this.activetedRoute.params.subscribe(n => {
      this.id = n.id;

      this.getInvoice(n.id);
    });
  }

  public edit(): void {
    this.router.navigate(['finance/invoice/edit', this.id]);
  }

  public correction(): void {
    this.router.navigate(['finance/invoice/correction', this.id]);
  }

  public cancel(): void {
    this.invoiceService.cancelInvoice().subscribe(n => {
      this.notifyService.success('Faktura anulowana');

      this.getInvoice(this.id);
    });
  }

  private getInvoice(id: number): void {
    this.invoiceService.getInvoice(id).subscribe(n => {
      this.invoice = n;

      this.breadcrumbService.addItem('Faktura nr ' + this.invoice.number, 'finance/invoice/' + this.invoice.id);
    });
  }
}
