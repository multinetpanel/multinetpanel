import { Injectable } from '@angular/core';
import { RequestService } from 'src/app/shared/services/request.service';
import { Observable } from 'rxjs';
import { Invoice } from '../models/invoice';
import { GetInvoicesOptions } from '../models/get-invoices-options';
import { InvoicesSum } from '../models/invoices-sum';

@Injectable({
  providedIn: 'root'
})
export class InvoiceService {
  private baseUrl = 'api/v1/invoices/';

  constructor(private requestService: RequestService) { }

  public getInvoice(id: number): Observable<Invoice> {
    return this.requestService.get<Invoice>(this.baseUrl + id);
  }

  public getInvoices(options: GetInvoicesOptions): Observable<Invoice[]> {
    return this.requestService.get<Invoice[]>(this.baseUrl, options);
  }

  public getInvoicesCount(options: GetInvoicesOptions): Observable<number> {
    return this.requestService.get<number>(this.baseUrl + 'count', options);
  }

  public getInvoicesSum(options: GetInvoicesOptions): Observable<InvoicesSum> {
    return this.requestService.get<InvoicesSum>(this.baseUrl + 'sum', options);
  }

  public cancelInvoice(): Observable<object> {
    throw new Error('Method not implemented.');
  }
}
