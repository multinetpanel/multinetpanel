export class VatRate {
    id: number;
    name: string;
    value: number;
}
