import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { VatRate } from '../models/vat-rate';
import { RequestService } from 'src/app/shared/services/request.service';

@Injectable({
  providedIn: 'root'
})
export class VatRateService {

  private baseUrl = 'api/v1/vatrates/';

  constructor(private requestService: RequestService) { }

  public getVatRates(): Observable<VatRate[]> {
    return this.requestService.get<VatRate[]>(this.baseUrl);
  }
}
