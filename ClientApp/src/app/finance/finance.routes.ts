import { Routes } from '@angular/router';
import { InvoicesListComponent } from './invoices/components/invoices-list/invoices-list.component';
import { MainLayoutComponent } from '../shared/layout/components/main-layout/main-layout.component';
import { InvoiceComponent } from './invoices/components/invoice/invoice.component';
import { TransfersListComponent } from './transfers/components/transfers-list/transfers-list.component';
import { Breadcrumb } from '../shared/breadcrumbs/models/breadcrumb';
import { AuthGuard } from '../user/auth/services/auth-guard.service';
import { TransfersImportLogListComponent } from './transfers/components/transfers-import-log-list/transfers-import-log-list.component';
import { TransfersImportLogComponent } from './transfers/components/transfers-import-log/transfers-import-log.component';
import { TransferComponent } from './transfers/components/transfer/transfer.component';
import { InvoiceCorrectionComponent } from './invoices/components/invoice-correction/invoice-correction.component';
import { InvoiceEditComponent } from './invoices/components/invoice-edit/invoice-edit.component';
import { InvoiceAddComponent } from './invoices/components/invoice-add/invoice-add.component';

export const financeRoutes: Routes = [
    {
        path: 'finance',
        component: MainLayoutComponent,
        canActivate: [AuthGuard],
        children: [
            {
                path: 'invoices',
                component: InvoicesListComponent,
                data: {
                    breadcrumbs: [
                        new Breadcrumb('Fakturowanie', '/finance/invoices')
                    ]
                }
            },
            {
                path: 'invoice/add',
                component: InvoiceAddComponent,
                data: {
                    breadcrumbs: [
                        new Breadcrumb('Fakturowanie', '/finance/invoices'),
                        new Breadcrumb('Dodaj fakturę', '/finance/invoices/add'),
                    ]
                }
            },
            {
                path: 'invoice/edit/:id',
                component: InvoiceEditComponent,
                data: {
                    defaultBreadcrumbs: [
                        new Breadcrumb('Fakturowanie', '/finance/invoices')
                    ]
                }
            },
            {
                path: 'invoice/correction/:id',
                component: InvoiceCorrectionComponent,
                data: {
                    defaultBreadcrumbs: [
                        new Breadcrumb('Fakturowanie', '/finance/invoices')
                    ]
                }
            },
            {
                path: 'invoice/:id',
                component: InvoiceComponent,
                data: {
                    defaultBreadcrumbs: [
                        new Breadcrumb('Fakturowanie', '/finance/invoices')
                    ]
                }
            }
        ]
    },
    {
        path: 'finance/transfers',
        component: MainLayoutComponent,
        canActivate: [AuthGuard],
        children: [
            {
                path: '',
                component: TransfersListComponent,
                data: {
                    breadcrumbs: [
                        new Breadcrumb('Przelewy', '/finance/transfers')
                    ]
                }
            },
            {
                path: 'logs',
                component: TransfersImportLogListComponent,
                data: {
                    breadcrumbs: [
                        new Breadcrumb('Przelewy', '/finance/transfers'),
                        new Breadcrumb('Logi importów', '/finance/transfers/logs'),
                    ]
                }
            },
            {
                path: 'logs/:id',
                component: TransfersImportLogComponent,
                data: {
                    breadcrumbs: [
                        new Breadcrumb('Przelewy', '/finance/transfers'),
                        new Breadcrumb('Logi importów', '/finance/transfers/logs')
                    ]
                }
            },
            {
                path: ':id',
                component: TransferComponent,
                data: {
                    breadcrumbs: [
                        new Breadcrumb('Przelewy', '/finance/transfers')
                    ]
                }
            }
        ]
    }
];
