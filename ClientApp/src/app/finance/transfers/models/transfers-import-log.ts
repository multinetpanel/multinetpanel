export class TransfersImportLog {
    id: number;
    date: Date;
    isSuccessful: boolean;
    transfersCount: number;
    message: string;
}
