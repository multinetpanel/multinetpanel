import { PagedModel } from 'src/app/shared/models/paged-model';

export class GetTransfersOptions extends PagedModel {
    hasInvoices: boolean;
    senderName: string;
    title: string;
    dateFrom: Date;
    dateTo: Date;

    constructor(options?: Partial<GetTransfersOptions>) {
        super();

        Object.assign(this, options);
    }
}
