import { Invoice } from '../../invoices/models/invoice';
import { TransferIndexerLog } from './transfer-indexer-log';
import { Transfer } from './transfer';

export class DetailedTransfer extends Transfer {
    invoices: Invoice[] = [];
    logs: TransferIndexerLog[] = [];
}
