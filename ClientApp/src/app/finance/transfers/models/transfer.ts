export class Transfer {
    id: number;
    value: number;
    senderAccountNumber: string;
    senderName: string;
    senderStreet: string;
    senderCity: string;
    receiver: string;
    title: string;
    date: Date;
    hasInvoice: boolean;
}
