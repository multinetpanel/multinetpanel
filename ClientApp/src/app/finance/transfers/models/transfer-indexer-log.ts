export class TransferIndexerLog {
    id: number;
    transferId: number;
    message: string;
    date: Date;
}
