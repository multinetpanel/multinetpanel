import { Component, OnInit } from '@angular/core';
import { TransferService } from '../../services/transfer.service';
import { NotifyService } from 'src/app/shared/services/notify.service';
import { GetTransfersOptions } from '../../models/get-transfers-options';
import { LazyLoadEvent } from 'primeng/components/common/lazyloadevent';
import { Transfer } from '../../models/transfer';
import { NgbDate } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { NgbDateExtensions } from 'src/app/shared/extensions/ngb-date-extensions';

@Component({
  selector: 'app-transfers-list',
  templateUrl: './transfers-list.component.html',
  styleUrls: ['./transfers-list.component.css']
})
export class TransfersListComponent implements OnInit {
  public filters: GetTransfersOptions = new GetTransfersOptions({hasInvoices: null});
  public totalItems: number;
  public transfers: Transfer[] = [];
  public selectedTransfer: Transfer;
  public cols: any;
  public loading = true;
  public dateToFilter: NgbDate = null;
  public dateFromFilter: NgbDate = null;

  constructor(
    private transferService: TransferService,
    private notifyService: NotifyService,
    private router: Router) { }

  ngOnInit() {
    this.getCols();
    this.setInitialDate();
    this.setFilterDates();
    this.getTransfersCount();
  }

  public import(): void {
    this.notifyService.info('Rozpczęto procedurę importowania przelewów...');

    this.transferService.importTransfers().subscribe(n => {
      this.notifyService.success('Zaimportowano');
    });
  }

  public index(): void {
    this.notifyService.info('Rozpczęto procedurę indeksowania przelewów...');

    this.transferService.indexTransfers().subscribe(n => {
      this.notifyService.success('Zaindeksowano');
    });
  }

  public applyFilters(): void {
    this.setFilterDates();
    this.getTransfersCount();
    this.getTransfers({first: 0, rows: 10 });
  }

  private setFilterDates(): void {
    this.filters.dateFrom = NgbDateExtensions.toDate(this.dateFromFilter);
    this.filters.dateTo = NgbDateExtensions.toDate(this.dateToFilter);
  }

  public getTransfers(event?: LazyLoadEvent): void {
    this.loading = true;
    this.filters.page = event.first / event.rows + 1;
    this.filters.pageSize = event.rows;

    this.transferService.getTransfers(this.filters).subscribe(n => {
      this.transfers = n;
      this.loading = false;
    });
  }

  public onRowSelect(): void {
    this.router.navigate(['/finance/transfers', this.selectedTransfer.id]);
  }

  private getTransfersCount(): void {
    this.transferService.getTransfersCount(this.filters).subscribe(n => this.totalItems = n);
  }

  private setInitialDate(): void {
    this.dateFromFilter = NgbDateExtensions.getStartOfMonth();
    this.dateToFilter = NgbDateExtensions.getEndOfMonth();
  }

  private getCols(): void {
    this.cols = [
      { field: 'senderName', header: 'Nadawca' },
      { field: 'title', header: 'Tytuł' },
      { field: 'value', header: 'Kwota', width: 100 },
      { field: 'date', header: 'Data', width: 100 },
      { field: 'hasInvoice', header: 'Stan', width: 75 },
    ];
  }
}
