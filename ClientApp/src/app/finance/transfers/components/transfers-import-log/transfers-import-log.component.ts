import { Component, OnInit } from '@angular/core';
import { TransfersImportLogService } from '../../services/transfers-import-log.service';
import { ActivatedRoute } from '@angular/router';
import { TransfersImportLog } from '../../models/transfers-import-log';
import { BreadcrumbService } from 'src/app/shared/breadcrumbs/services/breadcrumb.service';

@Component({
  selector: 'app-transfers-import-log',
  templateUrl: './transfers-import-log.component.html',
  styleUrls: ['./transfers-import-log.component.css']
})
export class TransfersImportLogComponent implements OnInit {
  public log: TransfersImportLog = new TransfersImportLog();

  constructor(
    private transfersImportLogService: TransfersImportLogService,
    private activatedRoute: ActivatedRoute,
    private breadcrumbService: BreadcrumbService) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(n => {
      this.addBreadcrumb(n.id);
      this.getLog(n.id);
    });
  }

  private addBreadcrumb(id: number): void {
    this.breadcrumbService.addItem('Log importu przelewów', 'finance/transfers/logs/' + id);
  }

  private getLog(id: number): void {
    this.transfersImportLogService.getTransferImportLog(id).subscribe(n => {
      this.log = n;
    });
  }
}
