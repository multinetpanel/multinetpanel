import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BreadcrumbService } from 'src/app/shared/breadcrumbs/services/breadcrumb.service';
import { TransferService } from '../../services/transfer.service';
import { Transfer } from '../../models/transfer';
import { Invoice } from 'src/app/finance/invoices/models/invoice';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NotifyService } from 'src/app/shared/services/notify.service';

@Component({
  selector: 'app-transfer',
  templateUrl: './transfer.component.html',
  styleUrls: ['./transfer.component.css']
})
export class TransferComponent implements OnInit {
  public transfer: Transfer = new Transfer();
  public showLogs = false;
  public isSearchInvoiceDialog = false;
  public selectedInvoice: Invoice;

  constructor(
    private transferService: TransferService,
    private activatedRoute: ActivatedRoute,
    private breadcrumbService: BreadcrumbService,
    private modalService: NgbModal,
    private notifyService: NotifyService) { }

    ngOnInit() {
      this.activatedRoute.params.subscribe(n => {
        this.addBreadcrumb(n.id);
        this.getTransfer(n.id);
      });
    }

    public openInvoicesDialog(content) {
      this.selectedInvoice = null;

      this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', size: 'lg' }).result.then((result) => {
        this.transferService.attachInvoiceToTransfer(this.transfer.id, this.selectedInvoice.id).subscribe(n => {
          this.notifyService.success('Przypięto fakturę do przelewu');

          this.getTransfer(this.transfer.id);
        });
      });
    }

    public onRowSelect(invoice: Invoice): void {
      this.selectedInvoice = invoice;
    }

    private addBreadcrumb(id: number): void {
      this.breadcrumbService.addItem('Dane przelewu', 'finance/transfers/' + id);
    }

    private getTransfer(id: number): void {
      this.transferService.getTransfer(id).subscribe(n => {
        this.transfer = n;
      });
    }
}
