import { Component, OnInit } from '@angular/core';
import { TransfersImportLogService } from '../../services/transfers-import-log.service';
import { TransfersImportLog } from '../../models/transfers-import-log';
import { LazyLoadEvent } from 'primeng/components/common/lazyloadevent';
import { PagedModel } from 'src/app/shared/models/paged-model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-transfers-import-log-list',
  templateUrl: './transfers-import-log-list.component.html',
  styleUrls: ['./transfers-import-log-list.component.css']
})
export class TransfersImportLogListComponent implements OnInit {
  public totalItems: number;
  public logs: TransfersImportLog[] = [];
  public selectedLog: TransfersImportLog;
  public cols: any;
  public options: PagedModel = new PagedModel({page: 1, pageSize: 10 });

  constructor(private transfersImportLogService: TransfersImportLogService, private router: Router) { }

  ngOnInit() {
    this.getCols();
    this.getLogsCount();
  }

  private getLogsCount(): void {
    this.transfersImportLogService.getTransfersImportLogsCount().subscribe(n => this.totalItems = n);
  }

  public getLogs(event?: LazyLoadEvent): void {
    this.transfersImportLogService.getTransfersImportLogs(this.options).subscribe(n => this.logs = n);
  }

  public onRowSelect(): void {
    this.router.navigate(['finance/transfers/logs', this.selectedLog.id.toString()]);
  }

  private getCols(): void {
    this.cols = [
      { field: 'id', header: 'id' },
      { field: 'date', header: 'Data', width: 200 },
      { field: 'transfersCount', header: 'Liczba przelewów', width: 200 },
      { field: 'isSuccessful', header: 'Status', width: 100 }
    ];
  }
}
