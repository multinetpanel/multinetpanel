import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GetTransfersOptions } from '../models/get-transfers-options';
import { RequestService } from 'src/app/shared/services/request.service';
import { Transfer } from '../models/transfer';
import { TransfersImportLog } from '../models/transfers-import-log';

@Injectable({
  providedIn: 'root'
})
export class TransferService {
  private baseUrl = 'api/v1/transfers/';

  constructor(private requestService: RequestService) { }

  public importTransfers(): Observable<object> {
    return this.requestService.post(this.baseUrl + 'import', null);
  }

  public indexTransfers(): Observable<object> {
    return this.requestService.get(this.baseUrl + 'index', null);
  }

  public getTransfers(options: GetTransfersOptions): Observable<Transfer[]> {
    return this.requestService.get(this.baseUrl, options);
  }

  public getTransfer(id: number): Observable<Transfer> {
    return this.requestService.get<Transfer>(this.baseUrl + id + '/detailed');
  }

  public getTransfersCount(options: GetTransfersOptions): Observable<number> {
    return this.requestService.get(this.baseUrl + 'count', options);
  }

  public attachInvoiceToTransfer(transferId: number, invoiceId: number): Observable<object> {
    return this.requestService.get(this.baseUrl + 'index/invoice', { transferId: transferId, invoiceId: invoiceId });
  }
}
