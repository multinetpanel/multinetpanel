import { Injectable } from '@angular/core';
import { RequestService } from 'src/app/shared/services/request.service';
import { TransfersImportLog } from '../models/transfers-import-log';
import { Observable } from 'rxjs';
import { PagedModel } from 'src/app/shared/models/paged-model';

@Injectable({
  providedIn: 'root'
})
export class TransfersImportLogService {
  private baseUrl = 'api/v1/transfersImportLogs/';

  constructor(private requestService: RequestService) { }

  public getTransfersImportLogs(options: PagedModel): Observable<TransfersImportLog[]> {
    return this.requestService.get<TransfersImportLog[]>(this.baseUrl, options);
  }

  public getTransfersImportLogsCount(): Observable<number> {
    return this.requestService.get<number>(this.baseUrl + 'count');
  }

  public getTransferImportLog(id: number): Observable<TransfersImportLog> {
    return this.requestService.get<TransfersImportLog>(this.baseUrl + id);
  }
}
