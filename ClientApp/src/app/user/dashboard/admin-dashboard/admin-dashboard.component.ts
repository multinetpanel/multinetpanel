import { Component, OnInit } from '@angular/core';
import { DateService } from 'src/app/shared/services/date.service';
import { KeyValue } from 'src/app/shared/models/keyValue';
import { InvoiceService } from 'src/app/finance/invoices/services/invoice.service';
import { GetInvoicesOptions } from 'src/app/finance/invoices/models/get-invoices-options';
import { InvoicesSum } from 'src/app/finance/invoices/models/invoices-sum';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.css']
})
export class AdminDashboardComponent implements OnInit {

  public years: KeyValue<number>[];
  public months: KeyValue<number>[];
  public currentYear: number;
  public currentMonth: number;
  public invoicesSum: InvoicesSum = new InvoicesSum({ paidCount: 0, noPaidCount: 0, paidGross: 0, noPaidGross: 0 });

  constructor(private dateService: DateService, private invoiceService: InvoiceService) { }

  ngOnInit() {
    this.years = this.dateService.getYearsDictionary();
    this.months = this.dateService.getMonthsDictionary();
    this.currentMonth = new Date().getMonth() + 1;
    this.currentYear = new Date().getFullYear();

    this.loadInvoicesSummary();
  }

  public loadInvoicesSummary(): void {
    this.invoiceService.getInvoicesSum(new GetInvoicesOptions({
      invoiceDateFrom: new Date(this.currentYear, this.currentMonth, 1),
      invoiceDateTo: new Date(this.currentYear, this.currentMonth, this.dateService.getLastDayOfMonth(this.currentYear, this.currentMonth))
    })).subscribe(n => this.invoicesSum = n);
  }
}
