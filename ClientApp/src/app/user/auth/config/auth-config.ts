import { AuthConfig } from 'angular-oauth2-oidc';

export const authConfig: AuthConfig = {
  issuer: window.location.origin,
  redirectUri: window.location.origin,
  clientId: 'client-app',
  scope: 'openid profile email client-app',
  requireHttps: false
};
