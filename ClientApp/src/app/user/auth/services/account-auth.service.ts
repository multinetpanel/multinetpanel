import { Injectable } from '@angular/core';
import { ReplaySubject } from 'rxjs';
import {JwksValidationHandler, OAuthService} from 'angular-oauth2-oidc';
import { AccountClaims } from '../models/account-claims';
import { authConfig } from '../config/auth-config';

@Injectable({
  providedIn: 'root'
})
export class AccountAuthService {

  public isLogged: ReplaySubject<boolean> = new ReplaySubject();
  public claims: AccountClaims = new AccountClaims();

  constructor(private oAuthService: OAuthService) {
    this.onLogged();
  }

  public configureWithNewConfigApi(): void {
    this.oAuthService.configure(authConfig);

    this.oAuthService.loadDiscoveryDocument().then((doc) => {
      this.oAuthService.tryLogin({onTokenReceived: n => this.onLogged()});

      this.oAuthService.tokenValidationHandler = new JwksValidationHandler();
    });
  }

  private onLogged(): void {
    const isLogged = this.oAuthService.hasValidAccessToken();

    this.isLogged.next(isLogged);

    if (isLogged) {
      this.loadClaims();
    }
  }

  private loadClaims(): void {
    this.claims =  this.oAuthService.getIdentityClaims() as AccountClaims;
  }
}
