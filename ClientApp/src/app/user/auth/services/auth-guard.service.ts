import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AccountAuthService } from './account-auth.service';
import { Observable, Subscriber } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
@Injectable()
export class AuthGuard implements CanActivate {
  private isActivated: boolean;

  constructor(public accountAuthService: AccountAuthService, public router: Router) {
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return Observable.create(n => {
      this.accountAuthService.isLogged.subscribe(isLogged => {
        if (isLogged) {
          this.isActivated = true;
          n.next(true);
        } else {
          this.waitingForUserTimeout(n, 500);
        }
      });
    });
  }

  private waitingForUserTimeout(subsriber: Subscriber<boolean>, timeout: number): void {
    setTimeout(() => {
      if (!this.isActivated) {
        subsriber.next(false);

        this.router.navigate(['signin']);
      }
    }, timeout);
  }

}
