import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SigninComponent } from './signin/components/signin/signin.component';
import { RouterModule } from '@angular/router';
import { userRoutes } from './user.routes';
import { SharedModule } from '../shared/shared.module';
import { AdminDashboardComponent } from './dashboard/admin-dashboard/admin-dashboard.component';
import { UserDashboardComponent } from './dashboard/user-dashboard/user-dashboard.component';
import { DashboardComponent } from './dashboard/dashboard/dashboard.component';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(userRoutes),
    SharedModule
  ],
  declarations: [
    SigninComponent,
    AdminDashboardComponent,
    UserDashboardComponent,
    DashboardComponent
  ]
})
export class UserModule { }
