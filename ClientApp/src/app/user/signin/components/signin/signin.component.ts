import { Component, OnInit } from '@angular/core';
import { NotifyService } from 'src/app/shared/services/notify.service';
import { OAuthService } from 'angular-oauth2-oidc';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

  public username: string;
  public password: string;
  public error: string;

  constructor(private http: HttpClient,
    private oauthService: OAuthService,
    private notifyService: NotifyService) { }

  ngOnInit() {
  }

  public signIn(): void {
    this.http.post('account/signin', { username: this.username, password: this.password }).subscribe(n => {
      this.notifyService.success('Zalogowałeś się');

      this.oauthService.initImplicitFlow();
    }, n => {
      this.error = n.error;
    });
  }
}
