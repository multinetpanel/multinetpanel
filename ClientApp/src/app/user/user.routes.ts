import { Routes } from '@angular/router';
import { SigninComponent } from './signin/components/signin/signin.component';
import { DashboardComponent } from './dashboard/dashboard/dashboard.component';
import { AuthGuard } from './auth/services/auth-guard.service';
import { Breadcrumb } from '../shared/breadcrumbs/models/breadcrumb';
import { MainLayoutComponent } from '../shared/layout/components/main-layout/main-layout.component';

export const userRoutes: Routes = [
    { path: 'signin', component: SigninComponent },
    {
        path: 'account/logout',
        redirectTo: '/',
        pathMatch: 'full'
    },
    {
        path: '',
        component: MainLayoutComponent,
        canActivate: [AuthGuard],
        children: [
            {
                path: '',
                component: DashboardComponent
            }
        ]
    },
];
