export class User {
    id: number;
    username: string;
    email: string;

    constructor(user?: Partial<User>) {
        Object.assign(this, user);
    }
}
