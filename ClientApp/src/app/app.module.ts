import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { UserModule } from './user/user.module';
import { FinanceModule } from './finance/finance.module';
import { RouterModule } from '@angular/router';
import { appRoutes } from './app.routes';
import { OAuthModule } from 'angular-oauth2-oidc';
import { ClientModule } from './client/client.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    NoopAnimationsModule,
    RouterModule.forRoot(appRoutes),
    SharedModule,
    UserModule,
    FinanceModule,
    ClientModule,
    OAuthModule.forRoot({
      resourceServer: {
        allowedUrls: [window.location.origin],
        sendAccessToken: true
      }
    })
  ],
  providers: [

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
