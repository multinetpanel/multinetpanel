import { PagedModel } from 'src/app/shared/models/paged-model';

export class SearchPersonOptions extends PagedModel {
    search: string;

    constructor(options?: Partial<SearchPersonOptions>) {
        super(options);

        Object.assign(this, options);
    }
}
