import { PersonType } from './person-type';
import { PersonSex } from './person-sex';

export class Person {
    id: number;
    type: PersonType;
    pesel: string;
    nip: string;
    regon: string;
    identityCard: string;
    name: string;
    surname: string;
    companyName: string;
    companyShortName: string;
    birthday: Date;
    sex: PersonSex;
    province: string;
    disctrict: string;
    community: string;
    city: string;
    postCity: string;
    postCode: string;
    street: string;
    house: string;
    flat: string;
    email: string;
    phone: string;
    mobilePhone: string;
    servicePhone: string;
    fax: string;
    krs: string;

    nameDisplay: string;
    addressDisplay: string;

    constructor(person?: Partial<Person>) {
        Object.assign(this, person);
    }
}
