import { Injectable } from '@angular/core';
import { RequestService } from 'src/app/shared/services/request.service';
import { Person } from '../models/person';
import { Observable } from 'rxjs';
import { SearchPersonOptions } from '../models/search-person-options';

@Injectable({
  providedIn: 'root'
})
export class PersonService {
  private baseUrl = 'api/v1/people/';

  constructor(private requestService: RequestService) { }

  public search(phrase: string): Observable<Person[]> {
    return this.requestService.get<Person[]>(this.baseUrl + 'search', new SearchPersonOptions({ search: phrase, page: 1, pageSize: 50 }));
  }
}
