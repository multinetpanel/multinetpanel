import { Routes } from '@angular/router';
import { MainLayoutComponent } from '../shared/layout/components/main-layout/main-layout.component';
import { Breadcrumb } from '../shared/breadcrumbs/models/breadcrumb';
import { AuthGuard } from '../user/auth/services/auth-guard.service';
import { CustomersListComponent } from './clients/components/customers-list/customers-list.component';
import { AddCustomerComponent } from './clients/components/add-customer/add-customer.component';

export const clientRoutes: Routes = [
    {
        path: 'client',
        component: MainLayoutComponent,
        canActivate: [AuthGuard],
        children: [
        {
            path: '',
            component: CustomersListComponent,
            data: {
                breadcrumbs: [
                    new Breadcrumb('Lista klientów', '/customer/')
                ]
            }
        },
        {
            path: 'add',
            component: AddCustomerComponent,
            data: {
                breadcrumbs: [
                    new Breadcrumb('Lista klientów', '/customer/'),
                    new Breadcrumb('Dodaj nowego klienta', '/customer/add')
                ]
            }
        }
    ]}
];
