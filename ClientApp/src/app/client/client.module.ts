import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';
import { clientRoutes } from './client.routes';
import { TableModule } from 'primeng/table';
import { CustomersListComponent } from './clients/components/customers-list/customers-list.component';
import { AddCustomerComponent } from './clients/components/add-customer/add-customer.component';
import { UpdateCustomerComponent } from './clients/components/update-customer/update-customer.component';
import { CustomerFormComponent } from './clients/components/customer-form/customer-form.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(clientRoutes),
    TableModule
  ],
  declarations: [
    CustomersListComponent,
    AddCustomerComponent,
    UpdateCustomerComponent,
    CustomerFormComponent]
})
export class ClientModule { }
