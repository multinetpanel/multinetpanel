import { PagedModel } from 'src/app/shared/models/paged-model';

export class SearchClientOptions extends PagedModel {
    search: string;

    constructor(options?: Partial<SearchClientOptions>) {
        super(options);

        Object.assign(this, options);
    }
}
