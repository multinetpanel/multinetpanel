import { Person } from 'src/app/client/people/models/person';

export class Client {
    id: number;
    personId: number;
    person: Person;
}
