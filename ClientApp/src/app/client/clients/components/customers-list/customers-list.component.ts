import { Component, OnInit, ChangeDetectorRef, AfterViewInit } from '@angular/core';

import { Router } from '@angular/router';
import { LazyLoadEvent } from 'primeng/components/common/lazyloadevent';
import { ClientService } from '../../services/client.service';

@Component({
    selector: 'app-customers-list',
    templateUrl: './customers-list.component.html',
    styleUrls: ['./customers-list.component.css']
})
export class CustomersListComponent implements OnInit, AfterViewInit {
    // public selectedCustomer: Customer;
    public cols: any[];
    // public customers: Customer[] = [];
    public totalRecords: number;
    public loading: boolean;

    constructor(
        private router: Router,
        private customerService: ClientService,
        private changeDetector: ChangeDetectorRef
    ) { }

    ngOnInit() {
        this.loading = true;
        this.getTableColumns();
    }

    ngAfterViewInit(): void {
      this.changeDetector.detectChanges();
    }

    public onRowSelect(event): void {
        const id = event.data.id;

        this.router.navigate(['finance/invoice', id]);
    }

    public loadData(event: LazyLoadEvent): void {
        this.loading = true;

        // this.customerService.getCustomers().subscribe(n => {
        //     this.customers = n;

        //     this.loading = false;
        // });
    }

    private getTableColumns(): void {
        this.cols = [
            { field: 'id', header: 'Id', width: 100 },
            { field: 'name', header: 'Klient' },
            { field: 'street', header: 'Adres' },
            { field: 'phone', header: 'Telefon' },
            { field: 'pesel', header: 'PESEL/REGON' },
            { field: 'nip', header: 'NIP' },
            { field: 'id', header: 'Węzeł' },
        ];
    }
}
