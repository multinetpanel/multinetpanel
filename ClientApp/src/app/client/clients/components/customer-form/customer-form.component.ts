import { Component, OnInit } from '@angular/core';
import { CardTab } from '../../../../shared/layout/models/card-tab';

@Component({
  selector: 'app-customer-form',
  templateUrl: './customer-form.component.html',
  styleUrls: ['./customer-form.component.css']
})
export class CustomerFormComponent implements OnInit {

  public coreTab: CardTab = new CardTab({name: 'Dane podstawowe', isActive: true });
  public additionalTab: CardTab = new CardTab({name: 'Dane dodatkowe'});
  public phoneTab: CardTab = new CardTab({name: 'Telefony'});
  public addressTab: CardTab = new CardTab({name: 'Dane adresowe'});

  public tabs: CardTab[] = [this.coreTab, this.additionalTab, this.phoneTab, this.addressTab];

  constructor() { }

  ngOnInit() {

  }

  public setTab(tab: CardTab): void {
    this.tabs.forEach(n => n.isActive = false);
    tab.isActive = true;
  }
}
