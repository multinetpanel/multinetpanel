import { Injectable } from '@angular/core';
import { RequestService } from 'src/app/shared/services/request.service';
import { Client } from '../models/client';
import { Observable } from 'rxjs';
import { SearchClientOptions } from '../models/search-client-options';

@Injectable({
  providedIn: 'root'
})
export class ClientService {
  private baseUrl = 'api/v1/clients/';

  constructor(private requestService: RequestService) { }

  public search(phrase: string): Observable<Client[]> {
    return this.requestService.get<Client[]>(this.baseUrl + 'search', new SearchClientOptions({ search: phrase, page: 1, pageSize: 50 }));
  }
}
