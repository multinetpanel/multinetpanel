import { Component } from '@angular/core';
import { AccountAuthService } from './user/auth/services/account-auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  constructor(private accountAuthService: AccountAuthService) {
    this.accountAuthService.configureWithNewConfigApi();
  }
}
