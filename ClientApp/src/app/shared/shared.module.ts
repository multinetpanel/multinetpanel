import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './layout/components/header/header.component';
import { NavbarComponent } from './layout/components/navbar/navbar.component';
import { FooterComponent } from './layout/components/footer/footer.component';
import { MainLayoutComponent } from './layout/components/main-layout/main-layout.component';
import { RouterModule } from '@angular/router';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { BreadcrumbComponent } from './breadcrumbs/components/breadcrumb/breadcrumb.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { ToastaModule } from 'ngx-toasta';
import { DialogModule } from 'primeng/dialog';
import {AutoCompleteModule} from 'primeng/autocomplete';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    NgbModule,
    HttpClientModule,
    FormsModule,
    DialogModule,
    ToastaModule.forRoot(),
    AutoCompleteModule
  ],
  declarations: [
    HeaderComponent,
    NavbarComponent,
    FooterComponent,
    MainLayoutComponent,
    BreadcrumbComponent
  ],
  exports: [
    HeaderComponent,
    NavbarComponent,
    FooterComponent,
    MainLayoutComponent,
    NgbModule,
    HttpClientModule,
    FormsModule,
    ToastaModule,
    DialogModule,
    AutoCompleteModule
  ]
})
export class SharedModule { }
