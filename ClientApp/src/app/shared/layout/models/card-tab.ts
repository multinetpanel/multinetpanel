export class CardTab {
    name: string;
    isActive: boolean;
    IsInvalid: boolean;

    constructor(tab?: Partial<CardTab>) {
        Object.assign(this, tab);
    }
}
