import { Injectable } from '@angular/core';
import {ToastaConfig, ToastaService, ToastOptions} from 'ngx-toasta';

@Injectable({
  providedIn: 'root'
})
export class NotifyService {

  constructor(private toastaService: ToastaService, private toastaConfig: ToastaConfig) {
    this.toastaConfig.theme = 'default';
  }

  public info(message: string, title: string = 'Informacja'): void {
    this.toastaService.info({
      title: title,
      msg: message,
      showClose: true,
      timeout: 10000,
      theme: 'default'});
  }

  public success(message: string, title: string = 'Sukces'): void {
    this.toastaService.success({
      title: title,
      msg: message,
      showClose: true,
      timeout: 10000,
      theme: 'default'});
  }

  public error(message: string, title: string = 'Błąd'): void {
    this.toastaService.error({
      title: title,
      msg: message,
      showClose: true,
      timeout: 0,
      theme: 'default'});
  }

  public warning(message: string, title: string = 'Uwaga'): void {
    this.toastaService.warning({
      title: title,
      msg: message,
      showClose: true,
      timeout: 10000,
      theme: 'default'});
  }
}
