import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { NotifyService } from './notify.service';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class RequestService {

  constructor(private httpClient: HttpClient, private notifyService: NotifyService) { }

  public get<TResponse>(url: string, params?: object): Observable<TResponse> {
    const httpParams = this.mapToHttpParams(params);

    return this.handleError(this.httpClient.get<TResponse>(url, {params: httpParams}));
  }

  public post<TResponse>(url: string, body: any): Observable<TResponse> {
    return this.handleError(this.httpClient.post<TResponse>(url, body));
  }

  public put<TResponse>(url: string, body: any): Observable<TResponse> {
    return this.handleError(this.httpClient.put<TResponse>(url, body));
  }

  public delete<TResponse>(url: string): Observable<TResponse> {
    return this.handleError(this.httpClient.delete<TResponse>(url));
  }

  private handleError<TResponse>(requestAction: Observable<TResponse>): Observable<TResponse> {
    return new Observable<TResponse>(subscriber => {
      requestAction.subscribe(n => {
        subscriber.next(n);

        subscriber.complete();
      }, n => {
        if (this.isArrayOfString(n.error)) {
          (n.error as string[]).forEach(error => this.notifyService.error(error));
        } else {
          subscriber.error(n.error);
        }
      });
    });
  }

  private isArrayOfString(value: string[]): value is string[] {
    if (Array.isArray(value)) {
        return value.every(item => typeof item === 'string');
    }

    return false;
}

  private mapToHttpParams(params: object): HttpParams {
    let httpParams: HttpParams = null;

    if (params != null) {
      httpParams = new HttpParams();

      for (const key in params) {
        if (key === 'constructor') {
          continue;
        }

        if (params[key] != null) {

          if (!Array.isArray(params[key])) {
            httpParams = httpParams.append(key, this.getParamValue(params[key]));

            continue;
          }

          (params[key] as Array<any>).forEach(n => {
            httpParams = httpParams.append(key, n);
          });
        }
      }
    }

    return httpParams;
  }

  private getParamValue(obj: any): any {
    if (obj instanceof Date) {
      return moment(obj).format('YYYY-MM-DD HH:mm:ss');
    }

    return obj;
  }
}
