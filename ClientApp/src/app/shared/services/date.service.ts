import { Injectable } from '@angular/core';
import { KeyValue } from '../models/keyValue';

@Injectable({
  providedIn: 'root'
})
export class DateService {

  constructor() { }

  public getLastDayOfMonth(year: number, month: number): number {
    return new Date(year, month + 1, 0).getDate();
  }

  public getYearsDictionary(): KeyValue<number>[] {
    const startYear = 2010;
    const currentYear = new Date().getFullYear();
    const yearsAhead = currentYear - startYear + 2;

    const years: KeyValue<number>[] = [];

    for (let i = 0; i < yearsAhead; i++) {
      const year = startYear + i;

      years.push({ key: year.toString(), value: year });
    }

    return years;
  }

  public getMonthsDictionary(): KeyValue<number>[] {
    return [
      { key: 'Styczeń', value: 1 },
      { key: 'Luty', value: 2 },
      { key: 'Marzec', value: 3 },
      { key: 'Kwiecień', value: 4 },
      { key: 'Maj', value: 5 },
      { key: 'Czerwiec', value: 6 },
      { key: 'Lipiec', value: 7 },
      { key: 'Sierpień', value: 8 },
      { key: 'Wrzesień', value: 9 },
      { key: 'Październik', value: 10 },
      { key: 'Listopad', value: 11 },
      { key: 'Grudzień', value: 12 },
    ];
  }
}
