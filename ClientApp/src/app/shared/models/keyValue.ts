export class KeyValue<TModel> {
    public key: string;
    public value: TModel;

    constructor(keyValue?: Partial<KeyValue<TModel>>) {
        Object.assign(this, keyValue);
    }
}
