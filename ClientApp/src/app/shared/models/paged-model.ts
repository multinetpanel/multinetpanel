export class PagedModel {
    page: number;
    pageSize: number;

    constructor(model?: Partial<PagedModel>) {
        Object.assign(this, model);
    }
}
