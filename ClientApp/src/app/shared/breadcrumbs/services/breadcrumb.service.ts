import { Injectable } from '@angular/core';
import { Breadcrumb } from '../models/breadcrumb';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class BreadcrumbService {

  public breadcrumb = new Subject<Breadcrumb>();

  constructor(private router: Router) {

  }

  public addItem(label: string, href: string = this.router.url): void {
    this.breadcrumb.next(new Breadcrumb(label, href));
  }
}
