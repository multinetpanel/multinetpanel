import { NgbDate } from '@ng-bootstrap/ng-bootstrap';

export class NgbDateExtensions {
    public static toDate(ngbDate: NgbDate): Date {
        return ngbDate != null
            ? new Date(ngbDate.year, ngbDate.month - 1, ngbDate.day)
            : null;
    }

    public static getStartOfMonth(): NgbDate {
        const year = new Date().getFullYear();
        const month = new Date().getMonth() + 1;

        return new NgbDate(year, month, 1);
    }

    public static getCurrentDay(addDays?: number, addMonths?: number, addYears?: number): NgbDate {
        const day = new Date().getDate() + (addDays != null ? addDays : 0);
        const month = new Date().getMonth() + 1 + (addMonths != null ? addMonths : 0);
        const year = new Date().getFullYear() + (addYears != null ? addYears : 0);

        return new NgbDate(year, month, day);
    }

    public static getEndOfMonth(): NgbDate {
        const year = new Date().getFullYear();
        const month = new Date().getMonth() + 1;
        const day = new Date(year, month + 1, 0).getDate();

        return new NgbDate(year, month, day);
    }
}
